#!/bin/bash

cd ../

docker-compose run client npm install
docker-compose run puppeteer npm install
docker-compose run api composer install
