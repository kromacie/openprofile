#!/bin/bash

declare -A shells=(
  ["client"]="/bin/ash"
  ["puppeteer"]="/bin/bash"
  ["api"]="/bin/bash"
  ["web"]="/bin/bash"
)

cd "$(dirname "$(dirname "$(realpath "$0")")")" || exit

docker-compose run "$1" "${shells[$1]}"