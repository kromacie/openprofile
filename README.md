## OpenProfile

### Assumptions

- Create simple and quick cv creator
- Mix as many newest technologies as possible
- Create easy deploymentable service based on docker (compose)

### Functionalities

- MVP of cv composing functionality
- Create unlimited amount of cv documents
- Preview of currently selected cv

### Todo

- Translations
- PDF preview simulation as it's in real
- Many different cv schemas
- Section customization

> Check live preview of the project here http://server365214.nazwa.pl:8080/login

