<?php

namespace App\Repository;

use App\Entity\Experience;
use App\Repository\Interfaces\UserSecuredRepositoryInterface;
use App\Repository\Traits\UserSecuredRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Experience|null find($id, $lockMode = null, $lockVersion = null)
 * @method Experience|null findOneBy(array $criteria, array $orderBy = null)
 * @method Experience[]    findAll()
 * @method Experience[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExperienceRepository extends ServiceEntityRepository implements UserSecuredRepositoryInterface
{
    use UserSecuredRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Experience::class);
    }

    public function findByUser(int $id, int $user_id): ?Experience
    {
        return $this->findByUserOnSheetQuery(
            $this->createQueryBuilder('e'), $user_id, $id, 'e'
        );
    }

}
