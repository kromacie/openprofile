<?php

namespace App\Repository\Traits;

use Doctrine\ORM\QueryBuilder;

trait UserSecuredRepository
{
    public function findByUserOnSheetQuery(QueryBuilder $builder, int $user_id, int $entity_id, string $alias = 'e')
    {
        return $builder->andWhere("{$alias}.id = :id")
                       ->setParameter('id', $entity_id)
                       ->leftJoin("{$alias}.sheet", 's')
                       ->andWhere('s.user = :user')
                       ->setParameter('user', $user_id)
                       ->getQuery()
                       ->getSingleResult();
    }

}