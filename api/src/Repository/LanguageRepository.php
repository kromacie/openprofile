<?php

namespace App\Repository;

use App\Entity\Language;
use App\Entity\Skill;
use App\Repository\Interfaces\UserSecuredRepositoryInterface;
use App\Repository\Traits\UserSecuredRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Language|null find($id, $lockMode = null, $lockVersion = null)
 * @method Language|null findOneBy(array $criteria, array $orderBy = null)
 * @method Language[]    findAll()
 * @method Language[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LanguageRepository extends ServiceEntityRepository implements UserSecuredRepositoryInterface
{
    use UserSecuredRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Language::class);
    }

    public function findByUser(int $id, int $user_id): ?Language
    {
        return $this->findByUserOnSheetQuery(
            $this->createQueryBuilder('e'), $user_id, $id, 'e'
        );
    }

}
