<?php

namespace App\Repository;

use App\Entity\SheetConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SheetConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method SheetConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method SheetConfig[]    findAll()
 * @method SheetConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SheetConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SheetConfig::class);
    }

    // /**
    //  * @return SheetConfig[] Returns an array of SheetConfig objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SheetConfig
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
