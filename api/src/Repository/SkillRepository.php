<?php

namespace App\Repository;

use App\Entity\Skill;
use App\Repository\Interfaces\UserSecuredRepositoryInterface;
use App\Repository\Traits\UserSecuredRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Skill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skill[]    findAll()
 * @method Skill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillRepository extends ServiceEntityRepository implements UserSecuredRepositoryInterface
{
    use UserSecuredRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Skill::class);
    }

    public function findByUser(int $id, int $user_id): ?Skill
    {
        return $this->findByUserOnSheetQuery(
            $this->createQueryBuilder('e'), $user_id, $id, 'e'
        );
    }

}
