<?php

namespace App\Repository\Interfaces;

interface UserSecuredRepositoryInterface
{
    public function findByUser(int $id, int $user_id);

}