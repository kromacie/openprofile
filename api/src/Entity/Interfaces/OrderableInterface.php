<?php

namespace App\Entity\Interfaces;

interface OrderableInterface
{
    public function setOrder(int $order);

    public function getOrder(): int;

    public function getGroupIdentifier(): string;

    public function getGroup();

}