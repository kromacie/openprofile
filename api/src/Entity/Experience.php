<?php

namespace App\Entity;

use App\Entity\Extra\DateRange;
use App\Entity\Extra\EditorDescription;
use App\Entity\Interfaces\OrderableInterface;
use App\Entity\Traits\OrderableTrait;
use App\Repository\ExperienceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExperienceRepository::class)
 */
class Experience implements OrderableInterface
{
    use OrderableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @ORM\Embedded(class=EditorDescription::class)
     */
    private EditorDescription $description;

    /**
     * @ORM\Embedded(class=DateRange::class)
     */
    private DateRange $date;

    /**
     * @ORM\ManyToOne(targetEntity=Sheet::class, inversedBy="experiences")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $sheet;

    public function __construct()
    {
        $this->date = new DateRange();
        $this->description = new EditorDescription();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getDescription(): EditorDescription
    {
        return $this->description;
    }

    public function setDescription(EditorDescription $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): DateRange
    {
        return $this->date;
    }

    public function setDate(DateRange $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSheet(): ?Sheet
    {
        return $this->sheet;
    }

    public function getActive()
    {
        return false;
    }

    public function setSheet(?Sheet $sheet): self
    {
        $this->sheet = $sheet;

        return $this;
    }

    public function getGroupIdentifier(): string
    {
        return 'sheet';
    }

}
