<?php

namespace App\Entity;

use App\Repository\SheetConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SheetConfigRepository::class)
 */
class SheetConfig
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isExperienceVisible = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEducationVisible = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInterestsVisible = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSkillsVisible = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isLanguageVisible = true;

    /**
     * @ORM\OneToOne(targetEntity=Sheet::class, inversedBy="config", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $sheet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsExperienceVisible(): ?bool
    {
        return $this->isExperienceVisible;
    }

    public function setIsExperienceVisible(bool $isExperienceVisible): self
    {
        $this->isExperienceVisible = $isExperienceVisible;

        return $this;
    }

    public function getIsEducationVisible(): ?bool
    {
        return $this->isEducationVisible;
    }

    public function setIsEducationVisible(bool $isEducationVisible): self
    {
        $this->isEducationVisible = $isEducationVisible;

        return $this;
    }

    public function getIsInterestsVisible(): ?bool
    {
        return $this->isInterestsVisible;
    }

    public function setIsInterestsVisible(bool $isInterestsVisible): self
    {
        $this->isInterestsVisible = $isInterestsVisible;

        return $this;
    }

    public function getIsSkillsVisible(): ?bool
    {
        return $this->isSkillsVisible;
    }

    public function setIsSkillsVisible(bool $isSkillsVisible): self
    {
        $this->isSkillsVisible = $isSkillsVisible;

        return $this;
    }

    public function getIsLanguageVisible(): ?bool
    {
        return $this->isLanguageVisible;
    }

    public function setIsLanguageVisible(bool $isLanguageVisible): self
    {
        $this->isLanguageVisible = $isLanguageVisible;

        return $this;
    }

    public function getSheet(): ?Sheet
    {
        return $this->sheet;
    }

    public function setSheet(Sheet $sheet): self
    {
        $this->sheet = $sheet;

        return $this;
    }
}
