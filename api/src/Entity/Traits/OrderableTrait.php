<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait OrderableTrait
 *
 * @package App\Entity\Traits
 */
trait OrderableTrait
{
    /**
     * @ORM\Column(type="integer", nullable=false, name="`order`")
     */
    protected int $order;

    public function setOrder(int $order)
    {
        $this->order = $order;

        return $this;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function getGroup()
    {
        return $this->{$this->getGroupIdentifier()};
    }

}