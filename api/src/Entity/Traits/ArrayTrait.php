<?php

namespace App\Entity\Traits;

trait ArrayTrait
{

    protected function getOffset($options, string $key, $default = false)
    {
        $partials = explode('.', $key);

        if (count($partials) <= 1) {
            if (is_array($options) && array_key_exists($partials[0], $options)) {
                return $options[$partials[0]];
            }

            return $default;
        }

        if (is_array($options) && array_key_exists($partials[0], $options)) {
            return $this->getOffset(
                $options[$partials[0]], preg_replace("/^$partials[0]\./", '', $key), $default
            );
        }

        return $default;
    }

}