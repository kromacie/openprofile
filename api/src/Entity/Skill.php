<?php

namespace App\Entity;

use App\Entity\Interfaces\OrderableInterface;
use App\Entity\Traits\OrderableTrait;
use App\Repository\SkillRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SkillRepository::class)
 */
class Skill implements OrderableInterface
{
    use OrderableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name = '';

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating = 3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $opinion = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasRating = true;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description = '';

    /**
     * @ORM\ManyToOne(targetEntity=Sheet::class, inversedBy="skills")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sheet;

    public function __construct()
    {
        $this->languages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getOpinion(): ?string
    {
        return $this->opinion;
    }

    public function setOpinion(?string $opinion): self
    {
        $this->opinion = $opinion;

        return $this;
    }

    public function getHasRating(): ?bool
    {
        return $this->hasRating;
    }

    public function setHasRating(bool $hasRating): self
    {
        $this->hasRating = $hasRating;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSheet(): ?Sheet
    {
        return $this->sheet;
    }

    public function setSheet(?Sheet $sheet): self
    {
        $this->sheet = $sheet;

        return $this;
    }

    public function getGroupIdentifier(): string
    {
        return 'sheet';
    }

}
