<?php

namespace App\Entity\Extra;

use App\Entity\Traits\ArrayTrait;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Stringable;

/**
 * Class EditorDescription
 *
 * @package App\Entity\Extra
 *
 * @ORM\Embeddable()
 */
class EditorDescription implements JsonSerializable, Stringable
{
    use ArrayTrait;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $description;

    public function __construct(array $defaults = [])
    {
        $this->description = [
            'html' => $this->getOffset($defaults, 'html', ''),
            'raw'  => $this->getOffset($defaults, 'raw', [
                'blocks'    => $this->getOffset($defaults, 'raw.blocks', []),
                'entityMap' => $this->getOffset($defaults, 'raw.entityMap', new class {
                }),
            ]),
        ];
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function setHtml(string $html)
    {
        $this->description['html'] = $html;

        return $this;
    }

    public function setRaw(array $raw)
    {
        $this->description['raw'] = $raw;

        return $this;
    }

    public function setBlocks(array $blocks)
    {
        $this->description['raw']['blocks'] = $blocks;

        return $this;
    }

    public function setEntityMap(array $entityMap)
    {
        $this->description['raw']['entityMap'] = $entityMap;

        return $this;
    }

    public function jsonSerialize()
    {
        return json_encode($this->description);
    }

    public function __toString()
    {
        return $this->jsonSerialize();
    }

}