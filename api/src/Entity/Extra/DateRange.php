<?php

namespace App\Entity\Extra;

use App\Entity\Traits\ArrayTrait;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Stringable;

/**
 * Class DateRange
 *
 * @package App\Entity\Extra
 *
 * @ORM\Embeddable()
 */
class DateRange implements JsonSerializable, Stringable
{
    use ArrayTrait;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $date;

    public function __construct(array $defaults = [])
    {
        $this->date = [
            'from'    => $this->getOffset($defaults, 'from', ''),
            'to'      => $this->getOffset($defaults, 'to', ''),
            'current' => $this->getOffset($defaults, 'current', false),
        ];
    }

    public function setFrom(string $from)
    {
        $this->date['from'] = $from;

        return $this;
    }

    public function setTo(string $to)
    {
        $this->date['to'] = $to;

        return $this;
    }

    public function setCurrent(bool $current)
    {
        $this->date['current'] = $current;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function jsonSerialize()
    {
        return json_encode($this->date);
    }

    public function __toString()
    {
        return $this->jsonSerialize();
    }

}