<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCreateCommand extends Command
{
    protected static $defaultName = 'user:create';

    protected EntityManagerInterface $em;

    protected UserPasswordEncoderInterface $encoder;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();

        $this->em = $entityManager;
        $this->encoder = $encoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create user using command line');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $email = $io->ask("Give your user an email");

        password:
        $password = $io->ask("Give your user a password");
        $repeat = $io->ask("Repeat password");

        if ($password !== $repeat) {
            $io->warning("Passwords doesn't match");

            goto password;
        }

        $user = new User();

        $user->setEmail($email)
             ->setPassword(
                 $hashed = $this->encoder->encodePassword($user, $password)
             );

        $this->em->persist($user);
        $this->em->flush();

        $io->success(sprintf('User has been created successfully. %s', $hashed));

        return Command::SUCCESS;
    }

}
