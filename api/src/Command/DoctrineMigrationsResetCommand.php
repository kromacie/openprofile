<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

class DoctrineMigrationsResetCommand extends Command
{
    protected static $defaultName = 'doctrine:migrations:reset';

    protected function configure()
    {
        $this->setDescription('Reset migrations');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output = new ConsoleOutput();

        $this->getApplication()->find('doctrine:database:drop')->run(new ArrayInput([
            '--force' => true,
        ]), $output);

        $this->getApplication()->find('doctrine:database:create')->run($input, $output);
        $this->getApplication()->find('doctrine:migrations:migrate')->run($input, $output);

        return 0;
    }

}
