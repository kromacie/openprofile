<?php

namespace App\GraphQL\Mutation;

use App\Entity\Language;
use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\Repository\LanguageRepository;
use App\Repository\SheetRepository;
use App\Repository\SkillRepository;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Security\Core\Security;

class Languages implements AliasedInterface, MutationInterface
{
    use RepositoryHelper;

    private EntityManagerInterface $em;

    private Security $security;

    private SheetRepository $sheets;

    private LanguageRepository $languages;

    private array $fields = [
        'name',
        'opinion',
        'rating',
        'hasRating',
        'description',
    ];

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        SheetRepository $repository,
        LanguageRepository $languages
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->sheets = $repository;
        $this->languages = $languages;
    }

    public function update(int $id, string $key, $value)
    {
        $language = $this->getUserEntity($this->security, $this->languages, $id);

        if ($this->canSetEntityValue($this->fields, $key, $language)) {
            $this->setEntityValue($language, $key, [$value]);
        }

        $this->em->flush();

        return true;
    }

    public function create(int $id)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $sheet->setUpdatedAt(new \DateTime());

        $sheet->addLanguage($language = new Language());

        $this->em->persist($language);
        $this->em->flush();

        return $language;
    }

    public function delete(int $id)
    {
        /** @var Language $language */
        $language = $this->getUserEntity($this->security, $this->languages, $id);

        $language->getSheet()->setUpdatedAt(new \DateTime());

        $this->em->remove($language);
        $this->em->flush();

        return $language;
    }

    public static function getAliases(): array
    {
        return [
            'create' => 'createLanguage',
            'delete' => 'deleteLanguage',
            'update' => 'updateLanguage',
        ];
    }

}