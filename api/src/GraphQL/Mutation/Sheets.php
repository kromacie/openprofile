<?php

namespace App\GraphQL\Mutation;

use App\Entity\Profile;
use App\Entity\Sheet;
use App\Entity\SheetConfig;
use App\Entity\User;
use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\Repository\SheetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Security\Core\Security;

class Sheets implements MutationInterface, AliasedInterface
{
    use RepositoryHelper;

    private EntityManagerInterface $em;

    private Security $security;

    private SheetRepository $repository;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        SheetRepository $repository
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->repository = $repository;
    }

    public function create(string $name)
    {
        $user = $this->getUser();

        $sheet = new Sheet();

        $sheet->setProfile(new Profile());
        $sheet->setConfig(new SheetConfig());

        $sheet->setName($name);
        $sheet->setUser($user);

        $this->em->persist($sheet);
        $this->em->flush();

        return $sheet;
    }

    public function delete(int $id)
    {
        $sheet = $this->getUserSheet($this->security, $this->repository, $id);

        if ($sheet) {
            $this->em->remove($sheet);
            $this->em->flush();
        }

        return true;
    }

    public function update(int $id, string $name)
    {
        $sheet = $this->getUserSheet($this->security, $this->repository, $id);

        $sheet->setName($name);

        $this->em->flush();

        return $sheet;
    }

    public static function getAliases(): array
    {
        return [
            'create' => 'createSheet',
            'delete' => 'deleteSheet',
            'update' => 'updateSheet',
        ];
    }

    private function getUser(): User
    {
        /** @var User $user */
        $user = $this->security->getUser();

        return $user;
    }

}