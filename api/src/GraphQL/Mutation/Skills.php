<?php

namespace App\GraphQL\Mutation;

use App\Entity\Skill;
use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\Repository\SheetRepository;
use App\Repository\SkillRepository;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Security\Core\Security;

class Skills implements AliasedInterface, MutationInterface
{
    use RepositoryHelper;

    private EntityManagerInterface $em;

    private Security $security;

    private SheetRepository $sheets;

    private SkillRepository $skills;

    private array $fields = [
        'name',
        'opinion',
        'rating',
        'hasRating',
        'description'
    ];

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        SheetRepository $repository,
        SkillRepository $skills
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->sheets = $repository;
        $this->skills = $skills;
    }

    public function update(int $id, string $key, $value)
    {
        $skill = $this->getUserEntity($this->security, $this->skills, $id);

        if($this->canSetEntityValue($this->fields, $key, $skill)) {
            $this->setEntityValue($skill, $key, [$value]);
        }

        $this->em->flush();

        return true;
    }

    public function create(int $id)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $sheet->setUpdatedAt(new \DateTime());

        $sheet->addSkill($skill = new Skill());

        $this->em->persist($skill);
        $this->em->flush();

        return $skill;
    }

    public function delete(int $id)
    {
        /** @var Skill $skill */
        $skill = $this->getUserEntity($this->security, $this->skills, $id);

        $skill->getSheet()->setUpdatedAt(new \DateTime());

        $this->em->remove($skill);
        $this->em->flush();

        return $skill;
    }

    public static function getAliases(): array
    {
        return [
            'update' => 'updateSkill',
            'create' => 'createSkill',
            'delete' => 'deleteSkill',
        ];
    }

}