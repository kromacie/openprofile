<?php

namespace App\GraphQL\Mutation;

use App\Entity\Image;
use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\GraphQL\Validator\ImageInput;
use App\Repository\SheetRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Error\UserErrors;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Profile implements AliasedInterface, MutationInterface
{
    use RepositoryHelper;

    private EntityManagerInterface $em;

    private SheetRepository $sheets;

    private Security $security;

    private Filesystem $filesystem;

    private ValidatorInterface $validator;

    private string $profilesDirectory;

    private array $fields = [
        'name',
        'surname',
        'address',
        'email',
        'telephone',
        'title',
        'clause',
    ];

    private array $birthday = [
        'year',
        'month',
        'day',
    ];

    public function __construct(
        EntityManagerInterface $em,
        SheetRepository $repository,
        Security $security,
        Filesystem $filesystem,
        ValidatorInterface $validator,
        string $profilesDirectory
    ) {
        $this->em = $em;
        $this->sheets = $repository;
        $this->security = $security;
        $this->filesystem = $filesystem;
        $this->profilesDirectory = $profilesDirectory;
        $this->validator = $validator;
    }

    /**
     * @param int    $id
     * @param string $key
     * @param        $var
     */
    public function update(int $id, string $key, $var)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $sheet->setUpdatedAt(new DateTime());

        $profile = $sheet->getProfile();

        if ($this->canSetEntityValue($this->fields, $key, $profile)) {
            $this->setEntityValue($profile, $key, [$var]);
        }

        $this->em->flush();

        return $profile;
    }

    public function updateBirthday(int $id, string $key, $var)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $sheet->setUpdatedAt(new DateTime());

        $profile = $sheet->getProfile();

        if ($this->canSetEntityValue($this->birthday, $key, $profile))
            $profile->setBirthday(
                array_merge($profile->getBirthday(), [
                    $key => $var,
                ])
            );

        $this->em->flush();

        return $profile;
    }

    public function updateImage(int $id, UploadedFile $file)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $sheet->setUpdatedAt(new DateTime());

        $profile = $sheet->getProfile();

        $errors = $this->validator->validate(new ImageInput($file));

        if ($errors->count() > 0) {
            throw new UserErrors(
                array_map(
                    fn(ConstraintViolationInterface $throwable
                    ) => new UserError($throwable->getMessage()), iterator_to_array($errors)
                )
            );
        }

        if ($image = $profile->getImage()) {
            if ($this->filesystem->exists($path = $this->profilesDirectory . '/' . $image->getSrc())) {
                $this->filesystem->remove($path);
            }
        }

        try {
            $file->move(
                $this->profilesDirectory,
                $name = md5(random_bytes(20))
                    . '.'
                    . $file->getClientOriginalExtension()
            );

            $image = new Image();
            $image->setSrc($name);
            $image->setUrl($name);

            $profile->setImage($image);

            $this->em->flush();
        } catch (Exception $e) {
            throw new UserErrors([
                new UserError(sprintf($e->getMessage())),
            ]);
        }

        return $image;
    }

    public static function getAliases(): array
    {
        return [
            'update'         => 'updateSheetProfile',
            'updateBirthday' => 'updateSheetProfileBirthday',
            'updateImage'    => 'updateSheetProfileImage',
        ];
    }

}