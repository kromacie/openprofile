<?php

namespace App\GraphQL\Mutation;

use App\Entity\Education;
use App\Entity\Experience;
use App\Entity\Extra\DateRange;
use App\Entity\Extra\EditorDescription;
use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\Repository\EducationRepository;
use App\Repository\SheetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Security\Core\Security;

class Educations implements AliasedInterface, MutationInterface
{
    use RepositoryHelper;

    private Security $security;

    private EducationRepository $educations;

    private SheetRepository $sheets;

    private EntityManagerInterface $em;

    private array $fields = [
        'title',
    ];

    /**
     * Educations constructor.
     *
     * @param Security               $security
     * @param EducationRepository    $educations
     * @param SheetRepository        $sheets
     * @param EntityManagerInterface $em
     */
    public function __construct(
        Security $security,
        EducationRepository $educations,
        SheetRepository $sheets,
        EntityManagerInterface $em
    ) {
        $this->security = $security;
        $this->educations = $educations;
        $this->sheets = $sheets;
        $this->em = $em;
    }

    public function create(int $id)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $sheet->addEducation($education = new Education());

        $this->em->persist($education);
        $this->em->flush();

        return $education;
    }

    public function delete(int $id)
    {
        $education = $this->getUserEntity($this->security, $this->educations, $id);

        $this->em->remove($education);
        $this->em->flush();

        return true;
    }

    public function update(int $id, string $key, $value)
    {
        /** @var Education $experience */
        $experience = $this->getUserEntity($this->security, $this->educations, $id);

        $experience->getSheet()->setUpdatedAt(new \DateTime());

        if ($this->canSetEntityValue($this->fields, $key, $experience)) {
            $this->setEntityValue($experience, $key, [$value]);
        }

        $this->em->flush();

        return true;
    }

    public function updateDescription(int $id, EditorDescription $description)
    {
        /** @var Education $education */
        $education = $this->getUserEntity($this->security, $this->educations, $id);

        $education->getSheet()->setUpdatedAt(new \DateTime());

        $education->setDescription($description);

        $this->em->flush();

        return true;
    }

    public function updateDate(int $id, DateRange $range)
    {
        /** @var Education $education */
        $education = $this->getUserEntity($this->security, $this->educations, $id);

        $education->getSheet()->setUpdatedAt(new \DateTime());

        $education->setDate($range);

        $this->em->flush();

        return true;
    }

    public static function getAliases(): array
    {
        return [
            'create'            => 'createEducation',
            'delete'            => 'deleteEducation',
            'update'            => 'updateEducation',
            'updateDescription' => 'updateEducationDescription',
            'updateDate'        => 'updateEducationDate',
        ];
    }

}