<?php

namespace App\GraphQL\Mutation\Helpers;

use App\Entity\Sheet;
use App\Entity\User;
use App\Repository\Interfaces\UserSecuredRepositoryInterface;
use App\Repository\SheetRepository;
use Overblog\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Error\UserErrors;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\Security\Core\Security;

trait RepositoryHelper
{
    public function getUserSheet(Security $security, SheetRepository $sheets, int $id): Sheet
    {
        $user = $this->getUser($security);

        return $sheets->findOneBy([
            'user' => $user->getId(),
            'id'   => $id,
        ]);
    }

    public function getUserEntity(Security $security, UserSecuredRepositoryInterface $repository, int $id)
    {
        $user = $this->getUser($security);

        return $repository->findByUser($id, $user->getId());
    }

    public function setEntityValue(object $entity, string $key, array $args)
    {
        return call_user_func([
            $entity,
            Str::asLowerCamelCase("set" . $key),
        ], ...$args);
    }

    public function canSetEntityValue(array $fields, string $key, object $entity)
    {
        if (!key_exists($key, array_flip($fields))) {
            throw new UserErrors([
                new UserError(
                    sprintf(
                        "Given key \"%s\" not exists on \"%s\" type, available: \"%s\"", ...[
                            $key,
                            get_class($entity),
                            json_encode($fields),
                        ]
                    )
                ),
            ]);
        }

        return true;
    }

    public function getUser(Security $security): User
    {
        /** @var User $user */
        $user = $security->getUser();

        return $user;
    }

}