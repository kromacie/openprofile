<?php

namespace App\GraphQL\Mutation;

use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\Repository\SheetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Security\Core\Security;

class SheetConfig implements AliasedInterface, MutationInterface
{
    use RepositoryHelper;

    private Security $security;

    private SheetRepository $sheets;

    private EntityManagerInterface $em;

    private array $visibilityFields = [
        'isExperienceVisible',
        'isEducationVisible',
        'isSkillsVisible',
        'isLanguageVisible',
        'isInterestsVisible',
    ];

    /**
     * Educations constructor.
     *
     * @param Security               $security
     * @param SheetRepository        $sheets
     * @param EntityManagerInterface $em
     */
    public function __construct(
        Security $security,
        SheetRepository $sheets,
        EntityManagerInterface $em
    ) {
        $this->security = $security;
        $this->sheets = $sheets;
        $this->em = $em;
    }

    public function updateVisibility(int $id, string $name, bool $value)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $config = $sheet->getConfig();

        if($this->canSetEntityValue($this->visibilityFields, $name, $config)) {
            $this->setEntityValue($config, $name, [$value]);
        }

        $this->em->flush();

        return true;
    }

    public static function getAliases(): array
    {
        return [
            'updateVisibility' => 'updateSheetConfigVisibility',
        ];
    }

}