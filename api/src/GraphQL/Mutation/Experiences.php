<?php

namespace App\GraphQL\Mutation;

use App\Entity\Experience;
use App\Entity\Extra\DateRange;
use App\Entity\Extra\EditorDescription;
use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\Repository\ExperienceRepository;
use App\Repository\SheetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\Security\Core\Security;

class Experiences implements AliasedInterface, MutationInterface
{
    use RepositoryHelper;

    private Security $security;

    private SheetRepository $sheets;

    private ExperienceRepository $experiences;

    private EntityManagerInterface $em;

    private LoggerInterface $logger;

    private array $fields = [
        'title',
        'position',
    ];

    /**
     * Experiences constructor.
     *
     * @param Security               $security
     * @param SheetRepository        $sheets
     * @param ExperienceRepository   $experiences
     * @param EntityManagerInterface $em
     * @param LoggerInterface        $logger
     */
    public function __construct(
        Security $security,
        SheetRepository $sheets,
        ExperienceRepository $experiences,
        EntityManagerInterface $em,
        LoggerInterface $logger
    ) {
        $this->security = $security;
        $this->sheets = $sheets;
        $this->experiences = $experiences;
        $this->em = $em;
        $this->logger = $logger;
    }

    public function create(int $id)
    {
        $sheet = $this->getUserSheet($this->security, $this->sheets, $id);

        $sheet->addExperience($experience = new Experience());

        $this->em->persist($experience);
        $this->em->flush();

        return $experience;
    }

    public function delete(int $id)
    {
        $experience = $this->getUserEntity($this->security, $this->experiences, $id);

        $this->em->remove($experience);
        $this->em->flush();

        return true;
    }

    public function update(int $id, string $key, string $value)
    {
        /** @var Experience $experience */
        $experience = $this->getUserEntity($this->security, $this->experiences, $id);

        $experience->getSheet()->setUpdatedAt(new \DateTime());

        if ($this->canSetEntityValue($this->fields, $key, $experience)) {
            $this->setEntityValue($experience, $key, [$value]);
        }

        $this->em->flush();

        return true;
    }

    public function updateDescription(int $id, EditorDescription $description)
    {
        /** @var Experience $experience */
        $experience = $this->getUserEntity($this->security, $this->experiences, $id);

        $experience->getSheet()->setUpdatedAt(new \DateTime());

        $experience->setDescription($description);

        $this->em->flush();

        return true;
    }

    public function updateDate(int $id, DateRange $range)
    {
        /** @var Experience $experience */
        $experience = $this->getUserEntity($this->security, $this->experiences, $id);

        $experience->getSheet()->setUpdatedAt(new \DateTime());

        $experience->setDate($range);

        $this->em->flush();

        return true;
    }

    public static function getAliases(): array
    {
        return [
            'create'            => 'createExperience',
            'delete'            => 'deleteExperience',
            'update'            => 'updateExperience',
            'updateDescription' => 'updateExperienceDescription',
            'updateDate'        => 'updateExperienceDate',
        ];
    }

}