<?php

namespace App\GraphQL\Mutation;

use App\Entity\Interfaces\OrderableInterface;
use App\GraphQL\Mutation\Helpers\RepositoryHelper;
use App\Repository\Interfaces\UserSecuredRepositoryInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Error\UserErrors;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class Orderables implements MutationInterface, AliasedInterface
{
    use RepositoryHelper;

    private EntityManagerInterface $em;

    private Security $security;

    private LoggerInterface $logger;

    /**
     * Orderables constructor.
     *
     * @param EntityManagerInterface $em
     * @param Security               $security
     * @param LoggerInterface        $logger
     */
    public function __construct(EntityManagerInterface $em, Security $security, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->security = $security;
        $this->logger = $logger;
    }

    public function reorder(int $id, int $to, string $entity)
    {
        /** @var UserSecuredRepositoryInterface|EntityRepository $repository */
        $repository = $this->em->getRepository($entity);

        $entity = $this->getUserEntity($this->security, $repository, $id);

        if ($entity->getOrder() === $to) {
            throw new UserErrors([
                new UserError("Order can't be the same."),
            ]);
        }

        [$bigger, $smaller] = $this->sortItems(
            $entity->getOrder(),
            $to
        );

        $this->reorderCollection(
            $entity,
            $this->getItemsBetween(
                $entity,
                $repository,
                $bigger,
                $smaller,
                $id
            ),
            $to
        );

        $entity->setOrder($to);

        $this->em->flush();

        return true;
    }

    private function sortItems(int $from, int $to)
    {
        if ($from > $to) {
            return [
                $to,
                $from,
            ];
        }

        return [
            $from,
            $to,
        ];
    }

    private function getItemsBetween(
        OrderableInterface $from,
        EntityRepository $repository,
        int $smaller,
        int $bigger,
        int $id
    ) {
        return new ArrayCollection(
            $repository
                ->createQueryBuilder('e')
                ->andWhere('e.' . $from->getGroupIdentifier() . '= :group')
                ->andWhere('e.order BETWEEN :from AND :to')
                ->andWhere('e.id <> :id')
                ->setParameter('group', $from->getGroup())
                ->setParameter('to', $bigger)
                ->setParameter('from', $smaller)
                ->setParameter('id', $id)
                ->getQuery()
                ->getResult()
        );
    }

    private function reorderCollection(OrderableInterface $from, Collection $collection, int $to)
    {
        if ($from->getOrder() > $to) {
            $collection->map(fn(OrderableInterface $orderable) => $orderable->setOrder($orderable->getOrder() + 1));
        }
        else {
            $collection->map(fn(OrderableInterface $orderable) => $orderable->setOrder($orderable->getOrder() - 1));
        }
    }

    public static function getAliases(): array
    {
        return [
            'reorder' => 'reorderItems',
        ];
    }

}