<?php

namespace App\GraphQL\Type;

use App\Entity\Extra\DateRange;
use Exception;
use GraphQL\Language\AST\Node;

class AnyType
{
    /**
     * @param DateRange $value
     *
     * @return mixed
     */
    public static function serialize($value)
    {
        return $value;
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws Exception
     */
    public static function parseValue($value)
    {
        if(is_bool($value) || is_numeric($value) || is_string($value)) {
            return $value;
        }

        throw new Exception("Unexpected \"Any\" value type, allowed: int, bool, string");
    }

    /**
     * @param Node $valueNode
     *
     * @return string
     * @throws Exception
     */
    public static function parseLiteral($valueNode)
    {
        return $valueNode->value;
    }

}