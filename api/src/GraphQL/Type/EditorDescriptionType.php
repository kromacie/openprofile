<?php

namespace App\GraphQL\Type;

use App\Entity\Extra\EditorDescription;
use Exception;
use GraphQL\Language\AST\Node;

class EditorDescriptionType
{
    /**
     * @param EditorDescription $value
     *
     * @return array
     */
    public static function serialize(EditorDescription $value)
    {
        return $value->getDescription();
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws Exception
     */
    public static function parseValue($value)
    {
        return new EditorDescription($value);
    }

    /**
     * @param Node $valueNode
     *
     * @return string
     * @throws Exception
     */
    public static function parseLiteral($valueNode)
    {
        return json_decode($valueNode->value);
    }

}