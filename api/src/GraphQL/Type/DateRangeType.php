<?php

namespace App\GraphQL\Type;

use App\Entity\Extra\DateRange;
use Exception;
use GraphQL\Language\AST\Node;

class DateRangeType
{
    /**
     * @param DateRange $value
     *
     * @return array
     */
    public static function serialize(DateRange $value)
    {
        return $value->getDate();
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws Exception
     */
    public static function parseValue($value)
    {
        return new DateRange($value);
    }

    /**
     * @param Node $valueNode
     *
     * @return string
     * @throws Exception
     */
    public static function parseLiteral($valueNode)
    {
        return json_decode($valueNode->value);
    }

}