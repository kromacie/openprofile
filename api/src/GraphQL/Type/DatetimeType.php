<?php

namespace App\GraphQL\Type;

use DateTime;
use Exception;
use GraphQL\Language\AST\Node;

class DatetimeType
{
    /**
     * @param DateTime $value
     *
     * @return string
     */
    public static function serialize(DateTime $value)
    {
        return $value->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws Exception
     */
    public static function parseValue($value)
    {
        return new DateTime($value);
    }

    /**
     * @param Node $valueNode
     *
     * @return string
     * @throws Exception
     */
    public static function parseLiteral($valueNode)
    {
        return new DateTime($valueNode->value);
    }

}