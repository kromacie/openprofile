<?php

namespace App\GraphQL\Resolver;

use App\Entity\Image;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class ImageResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, Image $image)
    {
        return call_user_func([
            $image,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }

}