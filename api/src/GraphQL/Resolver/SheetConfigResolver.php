<?php

namespace App\GraphQL\Resolver;

use App\Entity\Profile;
use App\Entity\SheetConfig;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class SheetConfigResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, SheetConfig $config)
    {
        return call_user_func([
            $config,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }

}