<?php

namespace App\GraphQL\Resolver;

use App\Entity\Education;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class EducationResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, Education $profile)
    {
        return call_user_func([
            $profile,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }

}