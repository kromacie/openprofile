<?php

namespace App\GraphQL\Resolver;

use App\Entity\Interfaces\OrderableInterface;
use App\Entity\Sheet;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class SheetResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, Sheet $sheet)
    {
        return call_user_func([
            $sheet,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }

    public function getExperiences(ResolveInfo $info, Sheet $sheet, Argument $argument)
    {
        return $this->filter($this->__invoke($info, $sheet)->toArray(), $argument);
    }

    public function getEducations(ResolveInfo $info, Sheet $sheet, Argument $argument)
    {
        return $this->filter($this->__invoke($info, $sheet)->toArray(), $argument);
    }

    public function getLanguages(ResolveInfo $info, Sheet $sheet, Argument $argument)
    {
        return $this->filter($this->__invoke($info, $sheet)->toArray(), $argument);
    }

    public function getSkills(ResolveInfo $info, Sheet $sheet, Argument $argument)
    {
        return $this->filter($this->__invoke($info, $sheet)->toArray(), $argument);
    }

    private function filter(array $filterable, Argument $argument)
    {
        if (isset($argument['input']['filter'])) {
            $filter = $argument['input']['filter'];

            usort($filterable, function(OrderableInterface $previous, OrderableInterface $next) use ($filter) {
                return $filter === 'desc' ?
                    $previous->getOrder() - $next->getOrder() :
                    $next->getOrder() - $previous->getOrder();
            });
        }

        return $filterable;
    }

}