<?php

namespace App\GraphQL\Resolver;

use App\Entity\Language;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class LanguageResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, Language $language)
    {
        return call_user_func([
            $language,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }

}