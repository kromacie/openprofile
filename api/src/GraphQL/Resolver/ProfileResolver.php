<?php

namespace App\GraphQL\Resolver;

use App\Entity\Profile;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class ProfileResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, Profile $profile)
    {
        return call_user_func([
            $profile,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }

}