<?php

namespace App\GraphQL\Resolver;

use App\Entity\Experience;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class ExperienceResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, Experience $experience)
    {
        return call_user_func([
            $experience,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }
}