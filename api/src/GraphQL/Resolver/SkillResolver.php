<?php

namespace App\GraphQL\Resolver;

use App\Entity\Skill;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Bundle\MakerBundle\Str;

class SkillResolver implements ResolverInterface
{
    public function __invoke(ResolveInfo $info, Skill $skill)
    {
        return call_user_func([
            $skill,
            Str::asLowerCamelCase("get_" . $info->fieldName),
        ]);
    }

}