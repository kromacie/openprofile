<?php

namespace App\GraphQL\Resolver;

use App\Entity\User;
use App\Repository\SheetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserResolver implements ResolverInterface
{
    protected EntityManagerInterface $em;

    protected Security $security;

    protected SheetRepository $sheets;

    public function __construct(EntityManagerInterface $em, Security $security, SheetRepository $sheets)
    {
        $this->em = $em;
        $this->security = $security;
        $this->sheets = $sheets;
    }

    /**
     * @return UserInterface|User
     */
    public function getUser()
    {
        return $this->security->getUser();
    }

    public function getId(User $user)
    {
        return $user->getId();
    }

    public function getSheets(User $user)
    {
        return $user->getSheets();
    }

    public function getSheet(User $user, int $id)
    {
        return $this->sheets->findOneBy([
            'user' => $user->getId(),
            'id'   => $id,
        ]);
    }

}