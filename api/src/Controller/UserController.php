<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\SheetRepository;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private SheetRepository $sheets;

    public function __construct(SheetRepository $sheets)
    {
        $this->sheets = $sheets;
    }

    /**
     * @Route("/user/check", name="user.check", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function check()
    {
        return new Response("OK", 200);
    }

    public function getUser(): User
    {
        $user = parent::getUser();

        if ($user instanceof User) {
            return $user;
        }

        throw new InvalidArgumentException("User is incorrectly retrieved from the database.");
    }

}