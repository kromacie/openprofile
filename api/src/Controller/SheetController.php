<?php

namespace App\Controller;

use App\Entity\Sheet;
use App\Entity\User;
use App\Repository\SheetRepository;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SheetController extends AbstractController
{
    private SheetRepository $sheets;

    public function __construct(SheetRepository $sheets)
    {
        $this->sheets = $sheets;
    }

    /**
     * @Route("/user/sheet", name="user.sheet.all", methods={"GET"})
     *
     * @return Response
     */
    public function all()
    {
        return new JsonResponse([
            'all' => $this->getUser()->getSheets()->map(fn(Sheet $sheet) => [
                'name' => $sheet->getName(),
            ]),
        ], 200);
    }

    /**
     * @Route("/user/sheet/{id}/image", name="user.sheet.all", methods={"GET"})
     *
     * @param int    $id
     * @param string $profilesDirectory
     *
     * @return Response
     */
    public function image(int $id, string $profilesDirectory)
    {
        $sheet = $this->sheets->findOneBy([
            'user' => $this->getUser()->getId(),
            'id'   => $id,
        ]);

        if ($sheet && $image = $sheet->getProfile()->getImage()) {
            return new BinaryFileResponse($profilesDirectory . '/' . $image->getSrc(), 200);
        }

        throw new NotFoundHttpException("Resource not found.");
    }

    /**
     * @Route("/user/sheet", name="user.sheet.create", methods={"POST"})
     */
    public function create()
    {
        return new JsonResponse();
    }

    public function getUser(): User
    {
        $user = parent::getUser();

        if ($user instanceof User) {
            return $user;
        }

        throw new InvalidArgumentException("User is incorrectly retrieved from the database.");
    }

}