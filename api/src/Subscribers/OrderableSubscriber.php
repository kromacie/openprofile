<?php

namespace App\Subscribers;

use App\Entity\Interfaces\OrderableInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\NoResultException;

class OrderableSubscriber
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof OrderableInterface) {
            $this->updateOrderableEntity($entity);
        }
    }

    public function updateOrderableEntity(OrderableInterface $orderable)
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository(get_class($orderable));

        $orderable->setOrder(
            $this->getMaxOrder($repository, $orderable) + 1
        );
    }

    public function getMaxOrder(EntityRepository $repository, OrderableInterface $orderable): int
    {
        try {
            return $repository
                ->createQueryBuilder('e')
                ->select('MAX(e.order) as order')
                ->groupBy('e.' . $orderable->getGroupIdentifier())
                ->andWhere('e.' . $orderable->getGroupIdentifier() . ' = :group')
                ->setParameter('group', $orderable->getGroup()->getId())
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }

}