<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200828180406 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sheet_config (id INT AUTO_INCREMENT NOT NULL, sheet_id INT NOT NULL, is_experience_visible TINYINT(1) NOT NULL, is_education_visible TINYINT(1) NOT NULL, is_interests_visible TINYINT(1) NOT NULL, is_skills_visible TINYINT(1) NOT NULL, is_language_visible TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_7BE361228B1206A5 (sheet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sheet_config ADD CONSTRAINT FK_7BE361228B1206A5 FOREIGN KEY (sheet_id) REFERENCES sheet (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE sheet_config');
    }
}
