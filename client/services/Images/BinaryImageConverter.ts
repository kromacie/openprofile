export default new class BinaryImageConverter {
    public toBlob(data: ArrayBuffer, type: string) {
        return new Blob([new Uint8Array(data)], {type: type});
    }
}