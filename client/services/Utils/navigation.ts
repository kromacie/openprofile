import NavigationConsumer from "../../components/Composition/Navigation/Consumer";
import State from "../../components/Composition/Navigation/State";
import {cloneDeep} from "lodash";

export function onNavigationItemClick(this: NavigationConsumer, index: number): void {
    this.setState((prevState: State) => {
        return {
            navigation: Object.assign(prevState.navigation, {currentElement: index})
        };
    })
}

export function onNavigationToggle(this: NavigationConsumer): void {
    this.setState((prevState) => {
        return {navigation: Object.assign(prevState.navigation, {open: !prevState.navigation.open})}
    })
}