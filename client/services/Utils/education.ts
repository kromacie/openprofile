import Generator from "../../components/Generator/Generator";
import {findElementIndex} from "./updates";
import Education from "../Repositories/Sheets/Education/Education";
import {UpdateCodes} from "../../components/Builder/Creator/Indicator/Indicator";
import {cloneDeep} from "lodash";

export function onEducationCreate(this: Generator) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater()
        .once('education.create', () => {
            return Education.create(this.props.id).then((data) => {
                this.setState((prevState) => {
                    const details = cloneDeep(prevState.details);

                    details.educations.push({
                        id: data.id,
                        title: "",
                        description: {
                            html: "",
                            raw: {
                                blocks: [],
                                entityMap: {}
                            },
                        },
                        date: {
                            from: "",
                            to: "",
                            current: false
                        },
                        active: true,
                    });

                    return {
                        details: details,
                        updating: UpdateCodes.READY
                    };
                });
            }).catch(() => {
                this.setState({
                    updating: UpdateCodes.ERROR
                })
            })
        })
}

export function onEducationDelete(this: Generator, id: number) {
    this.setState({
        updating: UpdateCodes.PENDING
    });


    this.getUpdater().once('education.delete.' + id, () => {
        return Education.delete(id).then(() => {
            this.setState((prevState) => {
                const details = cloneDeep(prevState.details);

                details.educations.splice(
                    findElementIndex(id, details.educations), 1
                )

                return {details: details, updating: UpdateCodes.READY};
            });
        }).catch((e) => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        })
    })
}