import Generator from "../../components/Generator/Generator";
import {findElementIndex} from "./updates";
import Language from "../Repositories/Sheets/Language/Language";
import {UpdateCodes} from "../../components/Builder/Creator/Indicator/Indicator";
import {cloneDeep} from "lodash";

export function onLanguageCreate(this: Generator) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().once('language.create', () => {
        return Language
            .create(this.props.id)
            .then((e) => {
                this.setState((prevState) => {
                    const details = cloneDeep(prevState.details);

                    details.languages.push({
                        id: e.id,
                        name: "",
                        description: "",
                        opinion: "",
                        active: true,
                        rating: 3,
                    });

                    return {details: details, updating: UpdateCodes.READY};
                });
            })
            .catch((r) => {
                console.warn(r);

                this.setState({
                    updating: UpdateCodes.ERROR
                })
            })
    })
}

export function onLanguageDelete(this: Generator, id: number) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().once('language.delete', () => {
        return Language
            .delete(id)
            .then((e) => {
                this.setState((prevState) => {
                    const details = cloneDeep(prevState.details);

                    details.languages.splice(
                        findElementIndex(id, details.languages), 1
                    )

                    return {details: details, updating: UpdateCodes.READY};
                });
            })
            .catch((r) => {
                this.setState({
                    updating: UpdateCodes.ERROR
                })
            })
    })
}