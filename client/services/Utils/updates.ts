import Generator from "../../components/Generator/Generator";
import {Content} from "../../components/Builder/Creator/Editor/Content";
import IndexableDetails from "../../components/Generator/Interfaces/Details/IndexableDetails";
import Profile from "../Repositories/Sheets/Profile/Profile";
import telephone from "../../components/Curriculum/Basic/Sidebar/Section/Item/Telephone/Telephone";
import Birthday from "../Repositories/Sheets/Profile/Birthday/Birthday";
import Image from "../Repositories/Sheets/Profile/Image/Image";
import {UpdateCodes} from "../../components/Builder/Creator/Indicator/Indicator";
import Details from "../../components/Curriculum/Details";
import {cloneDeep, merge} from "lodash";
import Orderable from "../Repositories/Sheets/Orderable/Orderable";
import Education from "../Repositories/Sheets/Education/Education";
import EducationDescription from "../Repositories/Sheets/Education/Description/Description";
import EducationDate from "../Repositories/Sheets/Education/Date/Date";
import ExperienceDescription from "../Repositories/Sheets/Experience/Description/Description";
import ExperienceDate from "../Repositories/Sheets/Experience/Date/Date";
import Experience from "../Repositories/Sheets/Experience/Experience";
import Language from "../Repositories/Sheets/Language/Language";
import {UpdateRepositoryInterface} from "../Repositories/UpdateRepository";
import Skill from "../Repositories/Sheets/Skill/Skill";
import Config from "../Repositories/Sheets/Config/Config";

export function updateName(this: Generator, name: string) {
    updateRepositorySingleVariable.bind(this)(Profile, this.props.id, 'name', name);

    this.setState({details: Object.assign(this.state.details, {name: name}), updating: UpdateCodes.PENDING});
}

export function updateSurname(this: Generator, surname: string) {
    updateRepositorySingleVariable.bind(this)(Profile, this.props.id, 'surname', surname);

    this.setState({details: Object.assign(this.state.details, {surname: surname})});
}

export function updateEmail(this: Generator, email: string) {
    updateRepositorySingleVariable.bind(this)(Profile, this.props.id, 'email', email);

    this.setState({details: Object.assign(this.state.details, {email: email})})
}

export function updateAddress(this: Generator, address: string) {
    updateRepositorySingleVariable.bind(this)(Profile, this.props.id, 'address', address);

    this.setState({details: Object.assign(this.state.details, {address: address})});
}

export function updateBirthdayDay(this: Generator, day: string) {
    updateRepositorySingleVariable.bind(this)(Birthday, this.props.id, 'day', day);

    this.setState((prevState) => {
        return {
            details: Object.assign(prevState.details, {birthday: Object.assign({...prevState.details.birthday}, {day: day})})
        };
    });
}

export function updateBirthdayMonth(this: Generator, month: string) {
    updateRepositorySingleVariable.bind(this)(Birthday, this.props.id, 'month', month);

    this.setState((prevState) => {
        return {
            details: Object.assign(prevState.details, {birthday: Object.assign({...prevState.details.birthday}, {month: month})})
        };
    });
}

export function updateBirthdayYear(this: Generator, year: string) {
    updateRepositorySingleVariable.bind(this)(Birthday, this.props.id, 'year', year);

    this.setState((prevState) => {
        return {
            details: Object.assign(prevState.details, {birthday: Object.assign({...prevState.details.birthday}, {year: year})})
        };
    });
}

export function updateProfileImage(this: Generator, validity: ValidityState, files: FileList | null) {
    if (files && validity.valid) {
        const file = files[0];

        Image.update(this.props.id, file).then((res) => {
            this.setState((prevState) => {
                URL.revokeObjectURL(prevState.details.image.url);

                return {
                    details: Object.assign(prevState.details, {image: res}),
                    updating: UpdateCodes.READY
                };
            })
        }).catch(() => {
            this.setState(() => {
                return {
                    updating: UpdateCodes.ERROR
                };
            })
        })

        this.setState({
            updating: UpdateCodes.PENDING
        });
    }
}

export function updateTitle(this: Generator, title: string) {
    updateRepositorySingleVariable.bind(this)(Profile, this.props.id, 'title', title);

    this.setState({details: Object.assign(this.state.details, {title: title})});
}

export function updateTelephone(this: Generator, phone: string) {
    updateRepositorySingleVariable.bind(this)(Profile, this.props.id, 'telephone', phone);

    this.setState({details: Object.assign(this.state.details, {telephone: phone})});
}

export function findElementIndex(id: number, educations: Array<IndexableDetails>): number {
    return educations.findIndex((element: IndexableDetails) => {
        return element.id === id;
    })
}

export function updateArrayItemsOrder(elements: Array<any>, from: number, to: number) {
    const items = [...elements];


    const element = items.splice(findElementIndex(from, elements), 1);

    items.splice(findElementIndex(to, elements), 0, ...element);

    return items;
}

enum CollectionKeys {
    EDUCATIONS = 'educations',
    EXPERIENCES = 'experiences',
    LANGUAGES = 'languages',
    SKILLS = 'skills',
}

export function updateVariableInCollection(_details: Details, collectionKey: CollectionKeys, id: number, key: string, variable: any) {
    const details: Details = {..._details};
    const collection = details[collectionKey];

    const index = findElementIndex(id, collection);
    const elements = [...collection];
    elements[index] = merge(cloneDeep(elements[index]), {[key]: variable});

    return Object.assign({}, details, {[collectionKey]: elements});
}

export function updateRepositorySingleVariable(this: Generator, repository: UpdateRepositoryInterface, id: number, key: string, variable: any) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().update(`${repository.constructor.name}.${key}.${id}`, () => {
        return repository.update(id, key, variable).then(() => {
            this.setState({
                updating: UpdateCodes.READY
            })
        }).catch((r) => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });
}

export function updateSheetConfigVisibility(this: Generator, id: number, key: string, variable: boolean) {
    this.setState((prev) => {
        const config = Object.assign({}, prev.details.config, {[key]: variable});
        const details = cloneDeep(prev.details);
        details.config = config;

        return {
            details: details,
            updating: UpdateCodes.PENDING
        }
    });

    this.getUpdater().update(`sheet.config.visibility.${key}.${id}`, () => {
        return Config.updateVisibility(id, key, variable).then(() => {
            this.setState({
                    updating: UpdateCodes.READY
            })
        }).catch((r) => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });
}

export function updateRepositoryOrderableOrder(this: Generator, collection: CollectionKeys, from: number, to: number, entity: string) {
    if(from === to) {
        return;
    }

    this.setState({
        updating: UpdateCodes.PENDING
    });

    Orderable
        .reorder(from, findElementIndex(to, this.state.details[collection]), entity)
        .then(() => {
            this.setState({
                updating: UpdateCodes.READY
            });
        })
        .catch((e) => {
            console.log(e);
            this.setState({
                updating: UpdateCodes.ERROR
            });
        });

    this.setState((prevState) => {
        const {details} = prevState;

        details[collection] = updateArrayItemsOrder(details[collection], from, to);

        return {details: details};
    });
}

export function updateEducationTitle(this: Generator, id: number, title: string) {
    updateRepositorySingleVariable.bind(this)(Education, id, 'title', title);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EDUCATIONS, id, 'title', title)};
    });
}

export function updateEducationDescription(this: Generator, id: number, description: Content) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().update('education.description.' + id, () => {
        return EducationDescription.update(id, description).then(() => {
            this.setState({
                updating: UpdateCodes.READY
            })
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });
    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EDUCATIONS, id, 'description', description)};
    });
}

export function updateEducationDateTo(this: Generator, id: number, to: string) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().update('education.date.to.' + id, () => {
        return EducationDate.update(id, Object.assign(
            {}, this.state.details.educations[findElementIndex(id, this.state.details.educations)].date, {to: to})
        ).then(() => {
            this.setState({
                updating: UpdateCodes.READY
            })
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });
    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EDUCATIONS, id, 'date', {to: to})};
    });
}

export function updateEducationDateFrom(this: Generator, id: number, from: string) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().update('education.date.from.' + id, () => {
        return EducationDate.update(id, Object.assign(
            {}, this.state.details.educations[findElementIndex(id, this.state.details.educations)].date, {from: from})
        ).then(() => {
            this.setState({
                updating: UpdateCodes.READY
            })
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EDUCATIONS, id, 'date', {from: from})};
    });
}

export function updateEducationActive(this: Generator, id: number, active: boolean) {
    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EDUCATIONS, id, 'active', !active)};
    });
}

export function updateEducationItemsOrder(this: Generator, from: number, to: number) {
    updateRepositoryOrderableOrder.bind(this)(CollectionKeys.EDUCATIONS, from, to, 'education');
}

export function updateExperienceTitle(this: Generator, id: number, title: string) {
    updateRepositorySingleVariable.bind(this)(Experience, id, 'title', title);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EXPERIENCES, id, 'title', title)};
    });
}

export function updateExperiencePosition(this: Generator, id: number, position: string) {
    updateRepositorySingleVariable.bind(this)(Experience, id, 'position', position);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EXPERIENCES, id, 'position', position)};
    });
}

export function updateExperienceDescription(this: Generator, id: number, description: Content) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().update('experience.description.' + id, () => {
        return ExperienceDescription.update(id, description).then(() => {
            this.setState({
                updating: UpdateCodes.READY
            })
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EXPERIENCES, id, 'description', description)};
    });
}

export function updateExperienceDateTo(this: Generator, id: number, to: string) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().update('experience.date.to.' + id, () => {
        return ExperienceDate.update(id, Object.assign(
            {}, this.state.details.experiences[findElementIndex(id, this.state.details.experiences)].date, {to: to})
        ).then(() => {
            this.setState({
                updating: UpdateCodes.READY
            })
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EXPERIENCES, id, 'date', {to: to})};
    });
}

export function updateExperienceDateFrom(this: Generator, id: number, from: string) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().update('experience.date.from.' + id, () => {
        return ExperienceDate.update(id, Object.assign(
            {}, this.state.details.experiences[findElementIndex(id, this.state.details.experiences)].date, {from: from})
        ).then(() => {
            this.setState({
                updating: UpdateCodes.READY
            })
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        });
    });

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EXPERIENCES, id, 'date', {from: from})};
    });
}

export function updateExperienceActive(this: Generator, id: number, active: boolean) {
    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.EXPERIENCES, id, 'active', !active)};
    });
}

export function updateExperienceItemsOrder(this: Generator, from: number, to: number) {
    updateRepositoryOrderableOrder.bind(this)(CollectionKeys.EXPERIENCES, from, to, 'experience');
}

export function updateSkillName(this: Generator, id: number, name: string) {
    updateRepositorySingleVariable.bind(this)(Skill, id, 'name', name);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.SKILLS, id, 'name', name)};
    });
}

export function updateSkillDescription(this: Generator, id: number, description: string) {
    updateRepositorySingleVariable.bind(this)(Skill, id, 'description', description);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.SKILLS, id, 'description', description)};
    });
}

export function updateSkillOpinion(this: Generator, id: number, opinion: string) {
    updateRepositorySingleVariable.bind(this)(Skill, id, 'opinion', opinion);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.SKILLS, id, 'opinion', opinion)};
    });
}

export function updateSkillRating(this: Generator, id: number, rating: number | string) {
    console.log(rating);

    updateRepositorySingleVariable.bind(this)(Skill, id, 'rating', rating);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.SKILLS, id, 'rating', rating)};
    });
}

export function updateSkillHasRating(this: Generator, id: number, hasRating: boolean) {
    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.SKILLS, id, 'hasRating', hasRating)};
    });
}

export function updateSkillActive(this: Generator, id: number, active: boolean) {
    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.SKILLS, id, 'active', !active)};
    });
}

export function updateSkillItemsOrder(this: Generator, from: number, to: number) {
    updateRepositoryOrderableOrder.bind(this)(CollectionKeys.SKILLS, from, to, 'skill');
}

export function updateLanguageName(this: Generator, id: number, name: string) {
    updateRepositorySingleVariable.bind(this)(Language, id, 'name', name);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.LANGUAGES, id, 'name', name)};
    });
}

export function updateLanguageActive(this: Generator, id: number, active: boolean) {
    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.LANGUAGES, id, 'active', !active)};
    });
}

export function updateLanguageRating(this: Generator, id: number, rating: number | string) {
    updateRepositorySingleVariable.bind(this)(Language, id, 'rating', rating);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.LANGUAGES, id, 'rating', rating)};
    });
}

export function updateLanguageDescription(this: Generator, id: number, description: string) {
    updateRepositorySingleVariable.bind(this)(Language, id, 'description', description);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.LANGUAGES, id, 'description', description)};
    });
}

export function updateLanguageOpinion(this: Generator, id: number, opinion: string) {
    updateRepositorySingleVariable.bind(this)(Language, id, 'opinion', opinion);

    this.setState((prevState) => {
        return {details: updateVariableInCollection(prevState.details, CollectionKeys.LANGUAGES, id, 'opinion', opinion)};
    });
}

export function updateLanguageItemsOrder(this: Generator, from: number, to: number) {
    updateRepositoryOrderableOrder.bind(this)(CollectionKeys.LANGUAGES, from, to, 'language');
}

export function updateClause(this: Generator, clause: string) {
    updateRepositorySingleVariable.bind(this)(Profile, this.props.id, 'clause', clause);

    this.setState({details: Object.assign(this.state.details, {clause: clause})})

}

export function updateInterests(this: Generator, interests: Content) {
    this.setState((prevState) => {
        const {details} = prevState;

        details.interests = interests;

        return {details: details};
    });
}



