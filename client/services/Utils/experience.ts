import Generator from "../../components/Generator/Generator";
import {findElementIndex} from "./updates";
import Experience from "../Repositories/Sheets/Experience/Experience";
import {UpdateCodes} from "../../components/Builder/Creator/Indicator/Indicator";
import {cloneDeep} from "lodash";

export function onExperienceCreate(this: Generator) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().once('experience.create', () => {
        return Experience.create(this.props.id).then((data) => {
            this.setState((prevState) => {
                const details = cloneDeep(prevState.details);

                details.experiences.push({
                    id: data.id,
                    title: "",
                    position: "",
                    description: {
                        html: "",
                        raw: {
                            blocks: [],
                            entityMap: {}
                        },
                    },
                    date: {
                        from: "",
                        to: "",
                        current: false
                    },
                    active: true,
                });

                return {details: details, updating: UpdateCodes.READY};
            });
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        })
    })
}

export function onExperienceDelete(this: Generator, id: number) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().once('experience.delete.' + id, () => {
        return Experience.delete(id).then(() => {
            this.setState((prevState) => {
                const details = cloneDeep(prevState.details);

                details.experiences.splice(
                    findElementIndex(id, details.experiences), 1
                )

                return {details: details, updating: UpdateCodes.READY};
            });
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        })
    })
}