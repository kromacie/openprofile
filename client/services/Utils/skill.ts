import Generator from "../../components/Generator/Generator";
import {findElementIndex} from "./updates";
import Skill from "../Repositories/Sheets/Skill/Skill";
import {UpdateCodes} from "../../components/Builder/Creator/Indicator/Indicator";
import {cloneDeep} from "lodash";

export function onSkillCreate(this: Generator) {
    this.getUpdater().once('skill.create', () => {
        this.setState({
            updating: UpdateCodes.PENDING
        });

        return Skill.create(this.props.id).then((r) => {
            this.setState((prevState) => {
                const details = cloneDeep(prevState.details);

                details.skills.push({
                    id: r.id,
                    name: "",
                    opinion: "",
                    description: "",
                    rating: 3,
                    hasRating: true,
                    active: true
                });

                return {details: details, updating: UpdateCodes.READY};
            });
        }).catch(() => {
            this.setState({
                updating: UpdateCodes.ERROR
            })
        })
    })
}

export function onSkillDelete(this: Generator, id: number) {
    this.setState({
        updating: UpdateCodes.PENDING
    });

    this.getUpdater().once('skill.delete.' + id, () => {
        return Skill
            .delete(id)
            .then((e) => {
                this.setState((prevState) => {
                    const details = cloneDeep(prevState.details);

                    details.skills.splice(
                        findElementIndex(id, details.skills), 1
                    )

                    return {details: details, updating: UpdateCodes.READY};
                });
            })
            .catch((r) => {
                this.setState({
                    updating: UpdateCodes.ERROR
                })
            })
    })
}