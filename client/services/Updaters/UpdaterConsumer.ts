import UpdaterState from "./UpdaterState";
import Updater from "./Updater";

interface Callback {
    (prevState: UpdaterState): UpdaterState|null;
}

export default interface UpdaterConsumer {
    setState(callback: Callback): void,
    getUpdater(): Updater;
    state: UpdaterState
}