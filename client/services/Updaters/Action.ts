export default class Action {
    private readonly callback: () => Promise<any>;

    constructor(callback: () => Promise<any>) {
        this.callback = callback;
    }
    start() {
        return this.callback();
    }
}