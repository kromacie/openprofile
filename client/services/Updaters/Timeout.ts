export default class Timeout {
    private id?: number;
    private readonly time: number;
    protected callback: () => any;
    private started: boolean = false;
    private finished: boolean = false;
    private canceled: boolean = false;

    private onFinish?: () => void;
    private onStart?: () => void;

    constructor(callback: () => any, time: number) {
        this.time = time;
        this.callback = callback;
    }

    public start() {
        this.started = true;

        this.id = window.setTimeout(() => {
            this.finished = true;
            this.callback();

            if (this.onFinish) {
                this.onFinish();
            }
        }, this.time)

        if (this.onStart) {
            this.onStart();
        }
    }

    public cancel() {
        if (!this.finished && this.started) {
            this.canceled = true;

            window.clearTimeout(this.id);
        }
    }

    public reset(callback: () => any) {
        this.cancel();

        this.callback = callback;

        this.started = false;
        this.finished = false;
        this.canceled = false;

        return this;
    }

    public setOnStart(callback: () => void) {
        this.onStart = callback;
    }

    public setOnFinish(callback: () => void) {
        this.onFinish = callback;
    }

    public active() {
        return this.started && !this.finished;
    }
}