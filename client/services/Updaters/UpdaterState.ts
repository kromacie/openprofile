import State from "../../components/Composition/Navigation/State";
import Timeout from "./Timeout";
import Action from "./Action";

export default interface UpdaterState {
    updaters: Record<string, Timeout>,
    watchers: Record<string, boolean>
}