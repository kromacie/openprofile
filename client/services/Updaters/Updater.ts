import UpdaterConsumer from "./UpdaterConsumer";
import Timeout from "./Timeout";
import Action from "./Action";

class Updater {
    private consumer: UpdaterConsumer;
    private readonly timeout: number;

    constructor(consumer: UpdaterConsumer, timeout: number = 500) {
        this.consumer = consumer;
        this.timeout = timeout;
    }

    public update(key: string, callback: () => any) {
        this.consumer.setState((prev) => {
            const updaters = {
                ...prev.updaters
            };

            if (updaters[key]) {
                updaters[key].reset(callback).start();
            } else {
                const timeout = new Timeout(callback, this.timeout);

                updaters[key] = timeout;

                timeout.start();
            }

            return {
                updaters: updaters,
                watchers: prev.watchers
            };
        })
    }

    public once(key: string, callback: () => any) {
        console.log(key);

        if (!this.consumer.state.watchers[key]) {
            const action = new Action(async () => {
                return await callback();
            });

            this.consumer.setState((prev) => {
                const {watchers} = prev;

                watchers[key] = true;

                return {
                    updaters: prev.updaters,
                    watchers: watchers
                }
            })

            action
                .start()
                .then(() => {
                    this.consumer.setState((prev) => {
                        const {watchers} = prev;

                        watchers[key] = false;

                        return {
                            updaters: prev.updaters,
                            watchers: watchers
                        }
                    })
                })
                .catch(() => {
                    this.consumer.setState((prev) => {
                        const {watchers} = prev;

                        watchers[key] = false;

                        return {
                            updaters: prev.updaters,
                            watchers: watchers
                        }
                    })
                })
        }
    }

}

export default Updater;