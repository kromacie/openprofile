import api from "../../requests/api/connection";
import Router from "next/router";
import Unauthenticated from "./Exceptions/Unauthenticated";
import TokenNotFound from "./Exceptions/TokenNotFound";
import {AxiosInstance, AxiosResponse} from "axios";

class Authenticator {
    private token?: string | null;
    private refresh?: string | null;
    private connection: AxiosInstance;

    constructor(connection?: AxiosInstance) {
        this.connection = connection ? connection : api;

        this._init();
        this._configureConnection(this.connection);
    }

    private _init(): void {
        if (typeof window !== "undefined") {
            this.token = sessionStorage.getItem("jwt")
            this.refresh = sessionStorage.getItem("refresh")
        }
    }

    public setConnection(connection: AxiosInstance) {
        this.connection = connection;

        return new Authenticator(connection);
    }

    public loadCookies(cookies: {
        [key: string]: string;
    }) {
        if (cookies.open_profile_token) {
            this.setToken(cookies.open_profile_token);
        } else {
            this.setToken("null");
        }

        return this;
    }

    private _configureConnection(connection: AxiosInstance): void {
        if (this.hasToken()) {
            connection.defaults.headers.common['Authorization'] =
                'Bearer ' + this.getToken()
        }
    }

    private _parseTokenFromResponse(res: AxiosResponse): void {
        this.setToken(res.data.token)
            .setRefresh(res.data.refresh_token)
    }

    private static _catchNetworkError(err: any, retry?: () => any): any {
        let status: number;

        if (err.response) {
            status = err.response.status;
        } else if (err.networkError) {
            status = err.networkError.statusCode;
        } else {
            throw err;
        }

        // Unauthenticated error
        if (status === 401) {
            throw new Unauthenticated();
        }
        // Slow network error
        if (status === 110) {
            if (retry) {
                return retry();
            }
        }

        // Throw default error if any of these occurred
        throw err;
    }

    public hasToken(): boolean {
        return this.token != null && this.token.length > 0;
    }

    public getToken(): string | null | undefined {
        return this.token;
    }

    public hasRefresh(): boolean {
        return this.refresh != null && this.refresh.length > 0;
    }

    public setToken(token: string): Authenticator {
        if (typeof window !== "undefined")
            sessionStorage.setItem('jwt', token);

        this.token = token;

        this._configureConnection(this.connection);

        return this;
    }

    public setRefresh(refresh: string): Authenticator {
        if (typeof window !== "undefined")
            sessionStorage.setItem('refresh', refresh);

        this.refresh = refresh;

        return this;
    }

    public authenticate(tries: number = 3): Promise<void> {
        return new Promise<void>((accept, reject) => {
            if (this.hasRefresh()) {
                this.connection
                    .post("/auth/token/refresh", {
                        refresh_token: this.refresh
                    })
                    .then((res) => {
                        this._parseTokenFromResponse(res)

                        accept();
                    })
                    .catch((err) => {
                        try {
                            Authenticator._catchNetworkError(err, () => {
                                return this.authenticate(tries - 1);
                            })
                        } catch (e) {
                            reject(e);
                        }
                    })
            } else {
                throw new TokenNotFound();
            }
        });
    }

    public check(): Promise<void> {
        return this.connection.post("/user/check");
    }

    public perform(action: () => any, redirect = true): Promise<any> {
        return new Promise<any>((accept, reject) => {
            Promise
                .resolve(action())
                .then((res) => {
                    accept(res);
                })
                .catch((e) => {
                    try {
                        Authenticator._catchNetworkError(e, action);
                    } catch (e) {
                        if (e instanceof Unauthenticated) {
                            return this.authenticate()
                                .then(() => {
                                    action();
                                })
                                .catch((e) => {
                                    if (e instanceof TokenNotFound) {
                                        if (redirect && typeof window !== "undefined") {
                                            return Router.push("/login")
                                        }

                                        reject(e);
                                    }

                                })
                        }

                        reject(e);
                    }
                });
        })
    }

    public handle(err: any, retry?: () => any): Promise<any> {
        return new Promise<void>((accept, reject) => {
            try {
                accept(Authenticator._catchNetworkError(err, retry));
            } catch (e) {
                if (e instanceof Unauthenticated) {
                    return this.authenticate().then(() => {
                        if (retry) {
                            retry()
                        }
                    }).catch((e) => {
                        reject(e);
                    })
                }

                reject(e);
            }
        })
    }

    public login(username: string, password: string, tries: number = 3): Promise<{ token: string, refresh_token: string }> {
        return new Promise<{ token: string, refresh_token: string }>((accept, reject) => {
            this.connection
                .post('/auth/token', {
                    username: username,
                    password: password
                })
                .then((res) => {
                    this._parseTokenFromResponse(res);

                    accept(res.data);
                })
                .catch((err) => {
                    try {
                        Authenticator._catchNetworkError(err, () => {
                            return this.login(username, password, tries - 1);
                        })
                    } catch (e) {
                        reject(e);
                    }
                })
        });
    }
}

export default new Authenticator();