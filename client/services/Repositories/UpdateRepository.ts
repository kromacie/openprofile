import {DocumentNode} from "graphql";
import Authenticator from "../Authenticator/Authenticator";
import {ApolloClient} from "@apollo/client";
import Builder from "../../requests/Builder";

export default class UpdateRepository {
    private client: ApolloClient<any>;

    constructor(client?: ApolloClient<any>) {
        this.client = client ? client : new Builder().graphql();
    }

    public multipart() {
        return new UpdateRepository(new Builder().graphql(true));
    }

    public update(query: DocumentNode, variables: object) {
        return Authenticator
            .perform(() => {
                return this.client.mutate({
                    mutation: query,
                    variables: variables
                })
            });
    }
}

export interface UpdateRepositoryInterface {
    update: (id: number, key: string, value: any) => Promise<boolean>,
}