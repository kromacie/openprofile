import UpdateRepository, {UpdateRepositoryInterface} from "../../../UpdateRepository";
import update from "../../../../../requests/graphql/schemas/sheets/profile/birthday/update";

export default new class Birthday implements UpdateRepositoryInterface {
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    public update(id: number, key: string, value: string) {
        return this.repository
            .update(update, {
                id: id,
                key: key,
                value: value
            })
            .catch((r) => {
                console.warn("Birthday not updated");

                throw r;
            });

    }
}