import update from "../../../../requests/graphql/schemas/sheets/profile/update";
import UpdateRepository, {UpdateRepositoryInterface} from "../../UpdateRepository";

export default new class Profile implements UpdateRepositoryInterface {
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    update(id: number, key: string, value: string) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(update, {
                    id: id,
                    key: key,
                    value: value
                })
                .then((res) => {
                    accept(true);
                })
                .catch((res) => {
                    reject("Unable to update sheet profile");
                })
        })

    }
}