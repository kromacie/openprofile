import UpdateRepository from "../../../UpdateRepository";
import update from "../../../../../requests/graphql/schemas/sheets/profile/image/update";
import BinaryImageConverter from "../../../../Images/BinaryImageConverter";
import Builder, {AxiosConnection} from "../../../../../requests/Builder";
import {AxiosInstance} from "axios";
import Authenticator from "../../../../Authenticator/Authenticator";

export class ImageNotFound {

}

export default new class Image {
    private repository: UpdateRepository;
    private readonly api: AxiosInstance;

    constructor() {
        this.repository = new UpdateRepository();
        this.api = new Builder().axios(AxiosConnection.API);
    }

    public update(id: number, value: File) {
        return new Promise<{ url: string }>((accept, reject) => {
            return this.repository
                .multipart()
                .update(update, {
                    id: id,
                    picture: value
                })
                .then((e) => {
                    accept(this.get(id, new Date().getTime()))
                })
                .catch((r) => {
                    reject("Cannot update profile image!")
                });
        })

    }

    public get(id: number, hash: number = 0) {
        return new Promise<{ url: string }>((accept, reject) => {
            Authenticator.setConnection(this.api)
                .perform(() => {
                    return this.api.get(`/user/sheet/${id}/image?dummy=${hash}`, {responseType: 'arraybuffer'})
                })
                .then((res) => {
                    accept({
                        url: URL.createObjectURL(BinaryImageConverter.toBlob(res.data, res.headers['content-type']))
                    });
                })
                .catch((res) => {
                    if(typeof res.response !== "undefined" && res.response.status === 404) {
                        reject(new ImageNotFound())
                    }

                    reject("Unable to load an image!");
                })
        })
    }
}