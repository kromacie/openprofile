import UpdateRepository, {UpdateRepositoryInterface} from "../../UpdateRepository";
import create from "../../../../requests/graphql/schemas/sheets/skill/create";
import remove from "../../../../requests/graphql/schemas/sheets/skill/remove";
import update from "../../../../requests/graphql/schemas/sheets/skill/update";

export default new class Skill implements UpdateRepositoryInterface {
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    create(id: number) {
        return new Promise<{ id: number }>((accept, reject) => {
            return this.repository
                .update(create, {
                    id: id
                })
                .then((res) => {
                    accept(res.data.createSkill);
                })
                .catch(() => {
                    reject("Unable to create skill")
                })
        });
    }

    delete(id: number) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(remove, {
                    id: id
                })
                .then(() => {
                    accept(true);
                })
                .catch(() => {
                    reject("Unable to delete skill")
                })
        });
    }

    update(id: number, key: string, value: any) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(update, {
                    id: id, key: key, value: value
                })
                .then((res) => {
                    accept(true);
                })
                .catch((res) => {
                    reject("Unable to update sheet skill");
                })
        });
    }
}