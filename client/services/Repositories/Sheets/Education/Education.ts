import create from "../../../../requests/graphql/schemas/sheets/education/create";
import remove from "../../../../requests/graphql/schemas/sheets/education/remove";
import UpdateRepository, {UpdateRepositoryInterface} from "../../UpdateRepository";
import update from "../../../../requests/graphql/schemas/sheets/education/update";

export default new class Education implements UpdateRepositoryInterface {
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    create(id: number) {
        return new Promise<{ id: number }>((accept, reject) => {
            return this.repository
                .update(create, {
                    id: id
                })
                .then((res) => {
                    accept(res.data.createEducation)
                })
                .catch(() => {
                    reject("Unable to create education.")
                })
        })
    }

    delete(id: number) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(remove, {
                    id: id
                })
                .then(() => {
                    accept(true)
                })
                .catch(() => {
                    reject("Unable to create education.")
                })
        })
    }

    update(id: number, key: string, value: any) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(update, {
                    id: id,
                    key: key,
                    value: value
                })
                .then(() => {
                    accept(true)
                })
                .catch(() => {
                    reject("Unable to update education.")
                })
        })
    }
}