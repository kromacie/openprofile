import UpdateRepository from "../../../UpdateRepository";
import update from "../../../../../requests/graphql/schemas/sheets/education/description/update";
import {Content} from "../../../../../components/Builder/Creator/Editor/Content";

export default new class Description {
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    update(id: number, description: Content) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(update, {
                    id: id,
                    value: description
                })
                .then(() => accept(true))
                .catch((res) => console.log(res))
        });
    }
}