import UpdateRepository from "../../../UpdateRepository";
import update from "../../../../../requests/graphql/schemas/sheets/experience/date/update";

export default new class Date {
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    update(id: number, date: { from: string, to: string, current: boolean }) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(update, {
                    id: id,
                    value: date
                })
                .then(() => accept(true))
                .catch(() => reject("Can't update experience date."))
        });
    }
}