import create from "../../../../requests/graphql/schemas/sheets/experience/create";
import remove from "../../../../requests/graphql/schemas/sheets/experience/remove";
import UpdateRepository, {UpdateRepositoryInterface} from "../../UpdateRepository";
import update from "../../../../requests/graphql/schemas/sheets/experience/update";

export default new class Experience implements UpdateRepositoryInterface{
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    create(id: number) {
        return new Promise<{ id: number }>((accept, reject) => {
            return this.repository
                .update(create, {
                    id: id
                })
                .then((res) => {
                    accept(res.data.createExperience);
                })
                .catch(() => {
                    reject("Unable to create experience")
                })
        });
    }

    delete(id: number) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(remove, {
                    id: id
                })
                .then(() => {
                    accept(true);
                })
                .catch(() => {
                    reject("Unable to delete experience")
                })
        });
    }

    update(id: number, key: string, value: any) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(update, {
                    id: id, key: key, value: value
                })
                .then((res) => {
                    accept(true);
                })
                .catch((res) => {
                    reject("Unable to update sheet profile");
                })
        });
    }
}