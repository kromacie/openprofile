import Authenticator from "../../../Authenticator/Authenticator";
import connection from "../../../../requests/graphql/connection";
import {gql} from "@apollo/client";
import reorder from "../../../../requests/graphql/schemas/sheets/orderable/reorder";
import UpdateRepository from "../../UpdateRepository";

export default new class Orderable {
    private repository: UpdateRepository;

    constructor() {
        this.repository = new UpdateRepository();
    }

    reorder(id: number, to: number, entity: string) {
        return new Promise<boolean>((accept, reject) => {
            return this.repository
                .update(reorder, {
                    id: id,
                    to: to,
                    entity: entity
                })
                .then(() => {
                    accept(true);
                })
                .catch(() => {
                    reject(`Cannot update "${entity}" order`);
                });
        });
    }
}