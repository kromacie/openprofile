import Authenticator from "../../../Authenticator/Authenticator";
import Builder from "../../../../requests/Builder";
import update from "../../../../requests/graphql/schemas/sheets/config/update";

export default new class Config {
    updateVisibility(id: number, name: string, value: boolean): Promise<{
        id: number,
        updated_at: string,
    }> {
        return new Promise<{
            id: number,
            updated_at: string,
        }>((accept, reject) => {
            Authenticator
                .perform(() => {
                    return new Builder().graphql().mutate({
                        mutation: update,
                        variables: {
                            id: id,
                            key: name,
                            value: value
                        }
                    })
                })
                .then((res) => {
                    accept(res.data.updateSheetConfig);
                })
                .catch((r) => {
                    console.log(r)
                    reject("Unable to update config");
                })
        })
    }

}