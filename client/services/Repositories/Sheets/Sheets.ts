import Authenticator from "../../Authenticator/Authenticator";

import create from "../../../requests/graphql/schemas/sheets/create";
import remove from "../../../requests/graphql/schemas/sheets/remove";
import update from "../../../requests/graphql/schemas/sheets/update";
import all from "../../../requests/graphql/schemas/sheets/all";
import Details from "../../../components/Generator/Interfaces/Details";
import find from "../../../requests/graphql/schemas/sheets/find";
import Builder from "../../../requests/Builder";

export default new class Sheets {
    create(name: string): Promise<{
        name: string,
        id: number
    }> {
        return new Promise<{
            name: string,
            id: number
        }>((accept, reject) => {
            Authenticator
                .perform(() => {
                    return new Builder().graphql().mutate({
                        mutation: create,
                        variables: {
                            name: name
                        }
                    })
                })
                .then((res) => {
                    accept(res.data.createSheet);
                })
                .catch(() => {
                    reject("Unable to create sheet");
                })
        })
    }

    delete(id: number): Promise<void> {
        return new Promise<void>((accept, reject) => {
            Authenticator
                .perform(() => {
                    return new Builder().graphql().mutate({
                        mutation: remove,
                        variables: {
                            id: id
                        }
                    })
                })
                .then(() => {
                    accept();
                })
                .catch(() => {
                    reject("Unable to delete sheet");
                })
        })
    }

    update(id: number, name: string): Promise<{
        id: number,
        updated_at: string,
    }> {
        return new Promise<{
            id: number,
            updated_at: string,
        }>((accept, reject) => {
            Authenticator
                .perform(() => {
                    return new Builder().graphql().mutate({
                        mutation: update,
                        variables: {
                            id: id,
                            name: name
                        }
                    })
                })
                .then((res) => {
                    accept(res.data.updateSheet);
                })
                .catch(() => {
                    reject("Unable to update sheet");
                })
        })
    }

    get(): Promise<Array<{
        id: number,
        name: string,
        updated_at: string,
    }>> {
        return new Promise<Array<{
            id: number,
            name: string,
            updated_at: string,
        }>>((accept, reject) => {
            Authenticator
                .perform(() => {
                    return new Builder().graphql().query({
                        query: all
                    })
                })
                .then((res) => {
                    accept(res.data.user.sheets);
                })
                .catch((rej) => {
                    reject("Unable to load sheets");
                })
        })
    }

    find(id: number, filter: string = 'desc'): Promise<Details> {
        return Authenticator
            .perform(() => {
                return new Builder().graphql().query({
                    query: find,
                    variables: {
                        id: id,
                        filter: filter
                    },
                });
            })
            .then((res) => {
                return res.data.user.sheet;
            })
    }

    print(): Promise<Details> {
        return new Promise<Details>(() => {

        })
    }
}