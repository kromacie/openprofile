import {ReactNode} from "react";

export default interface Element {
    render: () => ReactNode,
    value: string|number,
    handler: (value: string|number) => void
}