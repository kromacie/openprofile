import React, {ReactNode, useEffect, useRef, useState} from "react";
import Element from "./Element/Element";

import styles from './radio.module.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStarHalfAlt} from "@fortawesome/free-solid-svg-icons";

const getStyles = (offsetX: number) => {
    return {
        transform: `translateX(${offsetX - 1}px)`
    }
}

type Props = {
    elements: Array<Element>,
    first?: number,
    title: ReactNode,
}

const radio = ({first = 0, elements, title}: Props) => {
    const indicator = useRef<HTMLDivElement>(null);
    const initializer = useRef<HTMLDivElement>(null);
    const [offsetX, setOffsetX] = useState(0);
    const [initialized, setInitialized] = useState(false);

    const onClick = (element: React.MouseEvent<HTMLDivElement>, value: number | string, handler: (value: number | string) => void) => {
        if (!indicator.current) {
            return;
        }

        setOffsetX(
            element.currentTarget.offsetLeft + (element.currentTarget.clientWidth - indicator.current.clientWidth) / 2
        )

        indicator.current.innerHTML = element.currentTarget.innerHTML

        if(initialized) {
            handler(value);
        }
    }

    useEffect(() => {
        if (window && initializer.current) {
            initializer.current.click();
            setInitialized(true);
        }
    }, []);

    return (
        <div className={styles.wrapper}>
            <div className={styles.description}>
                {title}
            </div>
            <div className={styles.radio}>
                <div className={styles.icon}>
                    <FontAwesomeIcon icon={faStarHalfAlt} /> <span className={styles.value}>{first}</span>
                </div>
                <div className={styles.elements}>
                    {elements.map((e) => {
                        return (
                            <div className={styles.element}
                                 key={e.value}
                                 ref={e.value == first ? initializer : null}
                                 onClick={(event) => onClick(event, e.value, e.handler)}>{e.render()}</div>
                        );
                    })}
                </div>
                <div className={styles.indicator} style={getStyles(offsetX)} ref={indicator}>
                </div>
            </div>
        </div>
    )
};

export default radio;