import React, {ReactNode, useEffect, useRef} from "react";

import styles from './textarea.module.scss';

import cn from "classnames";

type Props = {
    title: ReactNode,
    onChange: (value: string) => void,
    placeholder?: string,
    bordered?: boolean,
    value: string
}

function onInput(element: HTMLTextAreaElement, padding: number = 2) {
    element.style.height = `auto`;

    element.style.height = `calc(${element.scrollHeight}px - ${padding}rem)`;
}

const textarea = (props: Props) => {
    const ref = useRef<HTMLTextAreaElement>(null);

    return (
        <label className={cn(styles.wrapper, {[styles.bordered]: props.bordered})}>
            <div className={styles.description}>
                {props.title}
            </div>
            <div className={styles.input}>
                <textarea ref={ref}
                          onLoad={() => ref.current ? onInput(ref.current, props.bordered ? 1 : 2) : null}
                          onInput={() => ref.current ? onInput(ref.current, props.bordered ? 1 : 2) : null}
                          className={styles.textarea} placeholder={props.placeholder}
                          onChange={(e) => props.onChange(e.target.value)} value={props.value}>
                 </textarea>
            </div>
        </label>
    )
};

export default textarea;