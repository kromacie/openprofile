import React, {ChangeEvent, ReactNode} from "react";

import styles from './file.module.scss';
import {IconDefinition} from "@fortawesome/fontawesome-common-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

type FileProps = {
    children: ReactNode,
    icon: IconDefinition,
    onChange: OnImageChange
}

export interface OnImageChange {
    (validity: ValidityState, files: FileList | null): boolean | void,
}

const File = ({children, icon, onChange}: FileProps) => {
    return (
        <label className={styles.container}>
            <div className={styles.icon}>
                <FontAwesomeIcon icon={icon}/>
            </div>
            <div className={styles.description}>
                {children}
            </div>
            <input className={styles.input} type="file" onChange={(e: ChangeEvent<HTMLInputElement>) => {
                return onChange(e.target.validity, e.target.files)
            }}/>
        </label>
    );
};

export default File;