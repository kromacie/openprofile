import React, {Fragment, ReactNode} from 'react';

import styles from './group.module.scss';
import cn from "classnames";

interface GroupProps {
    title: string,
    children: ReactNode,
    bold?: boolean
}

const Group = ({title, children, bold = true}: GroupProps) => {
    return (
        <Fragment>
            <h3 className={cn(styles.title, {[styles.bold]: bold})}>
                {title}
            </h3>
            <div>
                {children}
            </div>
        </Fragment>
    )
};

export default Group;