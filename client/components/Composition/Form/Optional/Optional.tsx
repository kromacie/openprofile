import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import styles from './optional.module.scss';
import {IconProp} from "@fortawesome/fontawesome-svg-core";
import OnChange from "./OnChange";
import cn from "classnames";
import {faCheck} from "@fortawesome/free-solid-svg-icons";

interface InputProps {
    title: string,
    placeholder: string,
    icon: IconProp,
    value?: string,
    onChange?: OnChange,
    animateTitle?: boolean,
    hidden?: boolean
}

const optional = ({title, placeholder, icon, value, onChange, animateTitle = true, hidden = false}: InputProps) => {
    return (
        <div className={styles.container}>
            <div className={cn(styles.wrapper, {[styles.animateTitle]: animateTitle})}>
                <div className={styles.description}>
                    {title}
                </div>
                <div className={styles.inputWrapper}>
                <span className={styles.icon}>
                    <FontAwesomeIcon icon={icon}/>
                </span>
                    <input type={hidden ? 'password' : 'text'} className={styles.input} placeholder={placeholder}
                           value={value}
                           onChange={onChange}/>
                </div>
            </div>
            <label className={styles.optional}>
                Obecnie
                <input type="checkbox"/>
                <span className={styles.checkbox}>
                            <span className={styles.checkmark}>
                                <FontAwesomeIcon icon={faCheck}/>
                            </span>
                        </span>
            </label>
        </div>
    );
};

export default optional;