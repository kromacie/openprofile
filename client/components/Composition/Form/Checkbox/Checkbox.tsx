import React from "react";

import styles from './checkbox.module.scss';

const Checkbox = () => {
    return (
        <div className={styles.wrapper}>
            Włącz <input type="checkbox"/>
        </div>
    );
};

export default Checkbox;