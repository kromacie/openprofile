import {ChangeEvent} from "react";

type OnChange = (event: ChangeEvent<HTMLInputElement>) => any;

export default OnChange;