import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import styles from './input.module.scss';
import {IconProp} from "@fortawesome/fontawesome-svg-core";
import OnChange from "./OnChange";
import cn from "classnames";

interface InputProps {
    title: string,
    placeholder: string,
    icon: IconProp,
    value?: string,
    onChange?: OnChange,
    animateTitle?: boolean,
    hidden?: boolean
}

const input = ({title, placeholder, icon, value, onChange, animateTitle = true, hidden = false}: InputProps) => {
    return (
        <div className={styles.container}>
            <div className={cn(styles.wrapper, {[styles.animateTitle]: animateTitle})}>
                <div className={styles.description}>
                    {title}
                </div>
                <div className={styles.inputWrapper}>
                <span className={styles.icon}>
                    <FontAwesomeIcon icon={icon}/>
                </span>
                    <input type={hidden ? 'password' : 'text'} className={styles.input} placeholder={placeholder}
                           value={value}
                           onChange={onChange}/>
                </div>
            </div>
        </div>
    );
};

export default input;