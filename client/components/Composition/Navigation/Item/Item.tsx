import React from 'react';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronRight} from "@fortawesome/free-solid-svg-icons";
import cx from 'classnames';

import styles from './item.module.scss';

interface ItemProps {
    title: string,
    icon: JSX.Element,
    bottom?: boolean,
    clicked?: boolean,
    open?: boolean,
    onClick?: () => void,
    spin?: boolean
}

const item = ({title, icon, bottom, clicked, open, onClick, spin}: ItemProps) => {
    return (
        <div className={cx(styles.item, {
            [styles.bottom]: bottom,
            [styles.clicked]: clicked,
            [styles.open]: open,
            [styles.spin]: spin
        })} onClick={onClick}>
            <div className={styles.box}>
                {icon}
            </div>

            <span className={styles.title}>
                {title}
                <span className={styles.arrow}>
                <FontAwesomeIcon icon={faChevronRight}/>
                </span>
            </span>
        </div>
    );
};

export default item;