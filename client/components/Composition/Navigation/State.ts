export default interface State {
    navigation: {
        currentElement: number,
        open: boolean
    }
}