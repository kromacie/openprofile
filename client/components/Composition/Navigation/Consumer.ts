import State from "./State";

interface Callback {
    (prevState: State): State;
}

interface NavigationConsumer {
    setState(callback: Callback): void
}

export default NavigationConsumer;