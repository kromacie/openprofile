import React from 'react';

import styles from './logo.module.scss';

const logo = () => {
    return (
        <div className={styles.logo}>
            Open
            <div className={styles.subtext}>
                profile
            </div>
        </div>
    );
};

export default logo;