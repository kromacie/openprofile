import React, {ReactNode} from 'react';
import Logo from './Logo/Logo';

import cx from 'classnames';
import styles from './navigation.module.scss';

interface NavigationProps {
    topItems?: Array<ReactNode>,
    bottomItems?: Array<ReactNode>,
    navigationOpen: boolean,
}

const Navigation = ({navigationOpen, topItems, bottomItems}: NavigationProps) => {
    return (
        <div className={cx(styles.wrapper, {[styles.open]: navigationOpen})}>
            <div className={cx(styles.navigation, {[styles.open]: navigationOpen})}>
                <Logo/>
                <div className={styles.container}>
                    {topItems}
                    <div style={{marginTop: "auto"}}>
                        {bottomItems}
                    </div>
                </div>
            </div>
            <div className={styles.filling}>

            </div>
        </div>
    );
};

export default Navigation;