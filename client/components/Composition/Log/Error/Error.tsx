import React, {ReactNode} from "react";

import styles from './error.module.scss'

const Error = ({children}: { children: ReactNode }) => {
    return (
        <span className={styles.error}>
            {children}
        </span>
    )
};

export default Error;