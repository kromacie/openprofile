import DetailsInterface from "../Generator/Interfaces/Details";
import EducationDetails from "../Generator/Interfaces/Details/EducationDetails";
import ExperienceDetails from "../Generator/Interfaces/Details/ExperienceDetails";
import SkillDetails from "../Generator/Interfaces/Details/SkillDetails";
import LanguageDetails from "../Generator/Interfaces/Details/LanguageDetails";
import {Content} from "../Builder/Creator/Editor/Content";

class Details implements DetailsInterface {
    constructor() {
        this.name = "";
        this.surname = "";
        this.address = "";
        this.birthday = {
            year: "",
            month: "",
            day: ""
        };
        this.email = "";
        this.telephone = "";
        this.title = "";
        this.image = {
            url: ""
        }

        this.clause = "Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do realizacji procesu rekrutacji zgodnie z Rozporządzeniem Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (RODO).";
        this.interests = {
            raw: {
                blocks: [],
                entityMap: {}
            },
            html: ""
        }

        this.educations = [];
        this.experiences = [];
        this.skills = [];
        this.languages = [];
        this.config = {
            isEducationVisible: true,
            isExperienceVisible: true,
            isInterestsVisible: true,
            isLanguageVisible: true,
            isSkillsVisible: true
        }
    }

    public name: string;
    public surname: string;
    public birthday: {
        year: string,
        month: string,
        day: string
    };
    public telephone: string;
    public address: string;
    public title: string;
    public email: string;
    public image: {
        url: string
    }
    public config: {
        isExperienceVisible: boolean,
        isEducationVisible: boolean,
        isSkillsVisible: boolean,
        isLanguageVisible: boolean,
        isInterestsVisible: boolean,
    }

    public clause: string;
    public interests: Content;

    public educations: Array<EducationDetails>;
    public experiences: Array<ExperienceDetails>;
    public skills: Array<SkillDetails>;
    public languages: Array<LanguageDetails>;
}

export default Details;