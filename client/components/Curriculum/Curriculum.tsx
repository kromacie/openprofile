import React from "react";
import Layout from "./Layout";

import cn from "classnames";

import styles from './curriculum.module.scss';

class Curriculum extends React.Component<Layout, {}> {
    constructor(layout: Layout) {
        super(layout);
    }

    render() {
        const Schema = this.props.schema;

        return (
            <div id="sheet" className={cn(styles.container, {[styles.print]: this.props.print, "curriculum-ready": this.props.ready})}>
                <Schema details={this.props.details}/>
            </div>
        );
    }
}

export default Curriculum;