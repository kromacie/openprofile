import Details from "../Generator/Interfaces/Details";

interface SheetProps {
    print: boolean,
    details: Details,
}

export default SheetProps