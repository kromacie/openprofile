import Details from "./Details";

interface Layout {
    schema: any,
    print?: boolean,
    ready?: boolean,
    details: Details,
}

export default Layout;