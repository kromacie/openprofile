import React from 'react';
import Item from '../Item';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone } from "@fortawesome/free-solid-svg-icons";

const telephone = ({ children }) => {
    return (
        <Item
            icon={<FontAwesomeIcon icon={faPhone} />}
            title="Phone">
            { children }
        </Item>
    );
};

export default telephone;