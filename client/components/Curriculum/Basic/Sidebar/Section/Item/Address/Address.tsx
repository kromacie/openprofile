import React from 'react';
import Item from '../Item';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from "@fortawesome/free-solid-svg-icons";

type Props = {
    children: string,
}

const address = ({ children }: Props) => {
    return (
        <Item
            title="Address"
            icon={<FontAwesomeIcon icon={faHome} />}>
            { children }
        </Item>
    );
};

export default address;