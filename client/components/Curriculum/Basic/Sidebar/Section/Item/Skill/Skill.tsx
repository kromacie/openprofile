import React, {Fragment, ReactNode} from 'react';

import Item from "../Item";
import Description from "../Description/Description";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faTools} from "@fortawesome/free-solid-svg-icons";

import Rating from "../Rating/Rating";
import styles from './skill.module.scss';

const skill = ({ children, title, rating, opinion, icon }: SkillProps) => {
    return (
        <div className={styles.container}>
            <div className={styles.item}>
                <Item
                    icon={icon ? icon : <FontAwesomeIcon icon={ faTools } />}
                    transparent={ true }
                    title={ title }>
                    <div className={styles.rating}>
                        <Rating rating={rating} />
                        <span className={styles.opinion}>
                        {opinion}
                    </span>
                    </div>
                </Item>
            </div>
            <Description>
                { children }
            </Description>
        </div>
    );
};

export default skill;

export interface SkillProps {
    title: string,
    rating: number,
    opinion: string,
    icon?: ReactNode,
    children: ReactNode
}