import React from 'react';
import Item from '../Item';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";

type Props = {
    children: string
}

const email = ({children}: Props) => {
    return (
        <Item
            icon={<FontAwesomeIcon icon={faEnvelope}/>}
            title="Email">
            {children}
        </Item>
    );
}

export default email;