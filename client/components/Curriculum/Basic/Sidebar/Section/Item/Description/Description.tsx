import React, {ReactNode} from 'react';

import styles from './description.module.scss';

type Props = {
    children: ReactNode,
}

const description = ({children}: Props) => {
    return (
        <div className={styles.description}>
            <p className={styles.content}>
                {children}
            </p>
        </div>
    );
};

export default description;