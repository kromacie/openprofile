import React, {Fragment, ReactNode} from 'react';
import cx from 'classnames';

import styles from './item.module.scss';

const item = ({icon, title, children, transparent}: ItemProps): JSX.Element => {
    const core =
        <div className={styles.header}>
            {icon ? (
                <span className={cx(styles.icon, {[styles.transparent]: transparent})}>
                    {icon}
                </span>
            ) : null}
            <span className={styles.title}>
                {title}
            </span>
            <span className={styles.contents}>
                {children}
            </span>
        </div>;

    return (
        <li className={styles.container}>
            {core}
        </li>
    );
};

export default item;

export interface ItemProps {
    icon: ReactNode,
    title: string,
    children: ReactNode,
    transparent?: boolean
}