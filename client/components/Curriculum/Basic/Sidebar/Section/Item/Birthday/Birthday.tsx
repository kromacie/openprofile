import React from 'react';
import Item from '../Item';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";

type Props = {
    children: string,
}

const birthday = ({ children }: Props) => {
    return (
        <Item
            title="Birthday"
            icon={<FontAwesomeIcon icon={faCalendarAlt} />}>
            { children }
        </Item>
    );
};

export default birthday;