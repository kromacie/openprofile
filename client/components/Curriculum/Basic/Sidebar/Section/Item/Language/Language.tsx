import React from 'react';
import Skill from "../Skill/Skill";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-regular-svg-icons';

type Props = {
    title: string,
    rating: number,
    opinion: string
    description?: string
};

const language = (props: Props) => {
    return (
        <Skill
            {...props}
            icon={<FontAwesomeIcon icon={faCircle} />}
            children={props.description}
        />
    );
};

export default language;