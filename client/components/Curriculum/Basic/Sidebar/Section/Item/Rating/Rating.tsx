import React, {ReactNode, Fragment} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar as emptyStar} from "@fortawesome/free-regular-svg-icons";
import {faStar as filledStar} from "@fortawesome/free-solid-svg-icons";

interface RatingInterface {
    rating: number
}

const rating = ({rating}: RatingInterface): JSX.Element => {
    return (
        <Fragment>
            {Array.from({length: 5})
                .map((el, ind) => <FontAwesomeIcon key={ind} icon={emptyStar}/>)
                .reduce((accumulator: Array<ReactNode>, current: JSX.Element) => {
                    if (rating-- > 0) accumulator.push(<FontAwesomeIcon key={current.key ? current.key : 0} icon={filledStar}/>);
                    else accumulator.push(current);
                    return accumulator;
                }, [])}
        </Fragment>
    );

}

export default rating;