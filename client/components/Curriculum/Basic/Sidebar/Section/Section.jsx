import React, { Fragment } from 'react';
import styles from './section.module.scss';

const section = ({ title, children }) => {
    return (
        <Fragment>
            <h2 className={styles.title}>{ title }</h2>
            <ul className={styles.items}>
                { children }
            </ul>
        </Fragment>
    );
};

export default section;