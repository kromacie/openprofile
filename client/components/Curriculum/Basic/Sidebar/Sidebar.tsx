import React from 'react';
import Profile from '../Profile/Profile';
import Section from './Section/Section';

import Telephone from './Section/Item/Telephone/Telephone';
import Address from './Section/Item/Address/Address';
import Email from './Section/Item/Email/Email';
import Birthday from './Section/Item/Birthday/Birthday';

import Skill from "./Section/Item/Skill/Skill";

import styles from './sidebar.module.scss';
import Language from "./Section/Item/Language/Language";
import ProfileDetails from "../../../Generator/Interfaces/Details/ProfileDetails";
import SkillDetails from "../../../Generator/Interfaces/Details/SkillDetails";
import LanguageDetails from "../../../Generator/Interfaces/Details/LanguageDetails";
import ConfigDetails from "../../../Generator/Interfaces/Details/ConfigDetails";

interface SidebarProps {
    profile: ProfileDetails,
    skills: Array<SkillDetails>,
    languages: Array<LanguageDetails>,
    config: ConfigDetails
}

const sidebar = (details: SidebarProps) => {
    return (
        <div className={styles.sidebar}>
            <Profile url={details.profile.image.url}/>
            <Section title="Personal details">
                <Telephone>
                    {details.profile.telephone}
                </Telephone>
                <Address>
                    {details.profile.address}
                </Address>
                <Email>
                    {details.profile.email}
                </Email>
                <Birthday>
                    {details.profile.birthday.year + '-' + details.profile.birthday.month + '-' + details.profile.birthday.day}
                </Birthday>
            </Section>
            {details.config.isSkillsVisible ? (
                <Section title="Skills">
                    {details.skills.map((skill) => {
                        return (
                            <Skill key={skill.id} title={skill.name} rating={skill.rating} opinion={skill.opinion}>
                                {skill.description}
                            </Skill>
                        );
                    })}
                </Section>
            ) : null}

            {details.config.isLanguageVisible ? (
                <Section title="Languages">
                    {details.languages.map((language) => {
                        return (
                            <Language key={language.id} title={language.name} rating={language.rating} opinion={language.opinion}
                                      description={language.description}/>
                        );
                    })}
                </Section>
            ) : null}
        </div>
    );
};

export default sidebar;