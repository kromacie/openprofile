import React, {Fragment} from 'react';
import Header from './Header/Header';
import Sidebar from './Sidebar/Sidebar';
import Main from './Main/Main';
import Footer from "./Footer/Footer";

import styles from './styles.module.scss';
import SheetProps from "../SheetProps";

const basic = ({details}: SheetProps) => {
    return (
        <Fragment>
            <Header
                name={details.name}
                surname={details.surname}
                email={details.email}
                address={details.address}
                birthday={details.birthday}
                title={details.title}
                telephone={details.telephone}
                image={details.image}
            />
            <div className={styles.center}>
                <Sidebar config={details.config} profile={details} skills={details.skills} languages={details.languages}/>
                <Main details={details}/>
            </div>
            <Footer>
                {details.clause}
            </Footer>
        </Fragment>
    );
}

export default basic;