import React from 'react';
import styles from "./header.module.scss";
import ProfileDetails from "../../../Generator/Interfaces/Details/ProfileDetails";

const header = (details: ProfileDetails) => {
    return (
        <div className={styles.header}>
            <div className={styles.title}>
                {details.name + " " + details.surname}
            </div>
            <div className={styles.subtitle}>
                {details.title}
            </div>
        </div>
    );
}

export default header;