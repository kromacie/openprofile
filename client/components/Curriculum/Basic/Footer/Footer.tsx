import React, {ReactNode} from 'react';

import styles from './footer.module.scss'

type FooterProps = {
    children: ReactNode
}

const footer = (props: FooterProps) => {
    return (
        <div id="cv-footer" className={styles.footer}>
            <p className={styles.content}>
                {props.children}
            </p>
        </div>
    );
};

export default footer;