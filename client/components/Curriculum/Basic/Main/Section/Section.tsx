import React, {ReactNode} from 'react';
import styles from "./section.module.scss";
import cn from "classnames";

type Props = {
    title: string,
    children: ReactNode,
    icon: ReactNode,
    border?: boolean,
}

const section = ({title, children, icon, border = true}: Props) => {
    return (
        <div className={styles.section}>
            <div className={styles.title}>
                <span className={styles.icon}>
                    {icon}
                </span>
                {title}
            </div>
            <div className={cn(styles.content, {[styles.border]: border})}>
                {children}
            </div>
        </div>
    );
};

export default section;