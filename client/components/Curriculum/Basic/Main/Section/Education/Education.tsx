import React from 'react';
import Section from "../Section";
import Item from "../Item/Item";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faGraduationCap} from "@fortawesome/free-solid-svg-icons";
import EducationDetails from "../../../../../Generator/Interfaces/Details/EducationDetails";

type Props = {
    educations: Array<EducationDetails>,
    isVisible: boolean
}

const education = ({educations, isVisible}: Props) => {
    return (
        isVisible ?
        <Section
            icon={<FontAwesomeIcon icon={faGraduationCap}/>}
            title={"Education"}>
            {educations.map((education) => {
                return (
                    <Item key={education.id} title={education.title} date={education.date} description={education.description.html} />
                );
            })}
        </Section> : null
    );
};

export default education;