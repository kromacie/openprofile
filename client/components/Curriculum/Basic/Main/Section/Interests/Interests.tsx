import React from 'react';
import Section from "../Section";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBowlingBall, faGamepad} from "@fortawesome/free-solid-svg-icons";

import styles from './interests.module.scss'

type Props = {
    interests: string,
    isVisible: boolean
}

const interests = ({interests, isVisible}: Props) => {
    return (
        isVisible ?
        <Section
            icon={<FontAwesomeIcon icon={faGamepad}/>}
            title="Interests"
            border={false}>
            <div className={styles.interests} dangerouslySetInnerHTML={{__html: interests}}/>
        </Section> : null
    );
};

export default interests;