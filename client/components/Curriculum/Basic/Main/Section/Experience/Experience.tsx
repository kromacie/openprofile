import React from 'react';
import Section from "../Section";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBriefcase } from "@fortawesome/free-solid-svg-icons";
import Item from "../Item/Item";
import ExperienceDetails from "../../../../../Generator/Interfaces/Details/ExperienceDetails";

type Props = {
    experiences: Array<ExperienceDetails>,
    isVisible: boolean
}

const experience = ({experiences, isVisible}: Props) => {
    return (
        isVisible ?
        <Section
            icon={<FontAwesomeIcon icon={faBriefcase} />}
            title="Experience">
            <div>
                {experiences.map((experience) => {
                    return (
                        <Item key={experience.id} subtitle={experience.position} title={experience.title} date={experience.date} description={experience.description.html} />
                    );
                })}
            </div>
        </Section> : null
    );
};

export default experience;