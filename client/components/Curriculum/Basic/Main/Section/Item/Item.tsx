import React from 'react';

import styles from './item.module.scss';

type Props = {
    title: string,
    subtitle?: string,
    date: {
        from: string,
        to: string
    },
    description: string
}

const item = (props: Props) => {
    return (
        <div className={styles.item}>
            <div className={styles.header}>
                <div className={styles.title}>
                    {props.title}
                </div>
                <div className={styles.date}>
                    {props.date.from} - {props.date.to}
                </div>
                {props.subtitle ? (
                    <div className={styles.subtitle}>
                        {props.subtitle}
                    </div>
                ): null}
            </div>
            <div className={styles.content} dangerouslySetInnerHTML={{__html: props.description}} />
        </div>
    );
};

export default item;