import React from 'react';
import Experience from "./Section/Experience/Experience";
import Education from "./Section/Education/Education";
import Interests from "./Section/Interests/Interests";

import styles from './main.module.scss';
import Details from "../../Details";

type Props = {
    details: Details,
}

const main = ({details}: Props) => {
    return (
        <div className={styles.main}>
            <Education isVisible={details.config.isEducationVisible} educations={details.educations}/>
            <Experience isVisible={details.config.isExperienceVisible} experiences={details.experiences}/>
            <Interests isVisible={details.config.isInterestsVisible} interests={details.interests.html}/>
        </div>
    );
};

export default main;