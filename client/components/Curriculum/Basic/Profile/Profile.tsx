import React from 'react';
import styles from './profile.module.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUser} from "@fortawesome/free-solid-svg-icons";

const profile = ({url}: {url: string}) => {
    return (
        <div className={styles.profile}>
            {url ? (
                <img className={styles.image} src={url} alt="My Photo"/>
                ) : (
                <div className={styles.wrapper}>
                    <div className={styles.icon}>
                        <FontAwesomeIcon icon={faUser} />
                    </div>
                </div>
            ) }
        </div>
    );
};

export default profile;