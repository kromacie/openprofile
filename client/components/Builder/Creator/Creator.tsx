import React, {Fragment} from 'react';
import Main from './Main/Main';

import styles from './creator.module.scss';
import Topbar from "./Topbar/Topbar";
import Details from "../../Generator/Interfaces/Details";
import Handlers from "../../Generator/Interfaces/Handlers";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPrint} from "@fortawesome/free-solid-svg-icons";
import Indicator, {UpdateCodes} from "./Indicator/Indicator";

interface SidebarProps {
    information: {
        details: Details,
        handlers: Handlers,
    },
    pages: object
    navigation: {
        currentElement: number,
    },
    onNavigationItemClick: (index: number) => void,
    onReadyClick: () => void,
    updating: UpdateCodes
}

const creator = ({information, navigation, onNavigationItemClick, pages, updating, onReadyClick}: SidebarProps) => {
    return (
        <Fragment>
            <div className={styles.sidebar}>
                <div className={styles.topbar}>
                    <Topbar/>
                </div>
                <div className={styles.main}>
                    <Main
                        information={information}
                        navigation={navigation}
                        onNavigationItemClick={onNavigationItemClick}
                        pages={pages}/>
                </div>
                <Indicator status={updating} onReadyClick={onReadyClick} />
            </div>
        </Fragment>
    )
};

export default creator;