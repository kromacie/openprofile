import React, {useState} from 'react';

import styles from './main.module.scss';

import {faArrowDown, faArrowUp} from "@fortawesome/free-solid-svg-icons";

import Button, {Theme} from "../Button/Rounded/Rounded";
import Carousel from "../Carousel/Carousel";
import Details from "../../../Generator/Interfaces/Details";
import Handlers from "../../../Generator/Interfaces/Handlers";

interface MainProps {
    information: {
        details: Details,
        handlers: Handlers
    },
    navigation: {
        currentElement: number,
    },
    onNavigationItemClick: (index: number )=> void
    pages: object
}

const main = ({information, navigation, pages, onNavigationItemClick}: MainProps) => {
    const [pending, setPending] = useState(false);

    const changePageIndex = (value: number) => {
        if (!pending) {
            onNavigationItemClick(value);

            setPending(true);

            setTimeout(() => {
                setPending(false);
            }, 1000)
        }
    }

    const next = () => {
        return navigation.currentElement + 1;
    }

    const prev = () => {
        return navigation.currentElement - 1;
    }

    return (
        <div className={styles.main}>
            <div className={styles.pages}>
                <Carousel elements={pages} index={navigation.currentElement} watch={[JSON.stringify(information.details)]}/>
            </div>
            <div className={styles.buttons}>
                <div className={styles.button}>
                    <Button
                        theme={Theme.DANGER}
                        icon={faArrowUp}
                        onClick={() => changePageIndex(prev())}
                        disabled={0 > navigation.currentElement - 1}
                    >
                        Cofnij
                    </Button>
                </div>
                <div className={styles.button}>
                    <Button
                        theme={Theme.PRIMARY}
                        icon={faArrowDown}
                        onClick={() => changePageIndex(next())}
                        disabled={Object.keys(pages).length - 1 < navigation.currentElement + 1}
                    >
                        Przejdź dalej
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default main;