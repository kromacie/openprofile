import React, {MouseEvent} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import styles from './item.module.scss';
import {IconDefinition} from "@fortawesome/fontawesome-common-types";
import cn from "classnames";

type Props = {
    title: string,
    icon: IconDefinition,
    active: boolean,
    onToggle: (style: string) => any,
    style: string
};

const Item = ({title, icon, style, onToggle, active}: Props) => {
    const onItemToggle = (event: MouseEvent) => {
        event.preventDefault();

        onToggle(style);
    };

    return (
        <div className={cn(styles.item, {[styles.active]: active})} title={title} onMouseDown={onItemToggle}>
            <FontAwesomeIcon icon={icon} />
        </div>
    )
};

export default Item;