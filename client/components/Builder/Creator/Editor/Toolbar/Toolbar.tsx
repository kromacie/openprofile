import React from "react";
import {EditorState} from "draft-js";
import Item from "./Item/Item";
import {
    faBold,
    faItalic,
    faAlignLeft,
    faAlignCenter,
    faAlignRight,
    faListUl, faListOl, faUnderline
} from "@fortawesome/free-solid-svg-icons";

interface Props {
    editor: EditorState,
    onToggle: (style: string) => any,
}

const Toolbar = ({editor, onToggle}: Props) => {
    const isActive = (state: EditorState, style: string): boolean => {
        const selection = state.getSelection();
        const anchorKey = selection.getAnchorKey();
        const currentContent = state.getCurrentContent();
        const currentContentBlock = currentContent.getBlockForKey(anchorKey);
        const blockType = currentContentBlock.getType();
        const currentStyle = state.getCurrentInlineStyle();

        return currentStyle.has(style) || blockType === style;
    }

    const toolbarItems: any = [
        {style: "BOLD", icon: faBold},
        {style: "ITALIC", icon: faItalic},
        {style: "UNDERLINE", icon: faUnderline},
        {style: "ALIGN_LEFT", icon: faAlignLeft},
        {style: "ALIGN_CENTER", icon: faAlignCenter},
        {style: "ALIGN_RIGHT", icon: faAlignRight},
        {style: "unordered-list-item", icon: faListUl},
        {style: "ordered-list-item", icon: faListOl},
    ];

    return (
        toolbarItems.map((toolbarItem: any) => (
            <Item
                icon={toolbarItem.icon}
                key={toolbarItem.style}
                active={isActive(editor, toolbarItem.style)}
                title={toolbarItem.style}
                onToggle={onToggle}
                style={toolbarItem.style}
            />
        ))
    );
};

export default Toolbar;