import {Content} from "./Content";

type OnChange = (content: Content) => void;

export default OnChange;