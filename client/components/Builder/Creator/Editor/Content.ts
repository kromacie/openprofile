import {RawDraftContentState} from "draft-js";

export interface Content {
    raw: RawDraftContentState,
    html: string,
}