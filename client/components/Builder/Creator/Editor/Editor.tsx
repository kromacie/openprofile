import React, {Component as ReactComponent} from "react";

import Draft, {
    DraftHandleValue,
    Editor as DraftEditor,
    EditorState,
    RichUtils,
    DraftStyleMap,
    DraftEditorCommand,
    ContentBlock,
    RawDraftContentState,
    convertFromRaw,
    convertToRaw,
    getDefaultKeyBinding,
} from "draft-js";

import {RenderConfig, stateToHTML} from 'draft-js-export-html';

import styles from './editor.module.scss';
import Toolbar from "./Toolbar/Toolbar";
import {SyntheticKeyboardEvent} from "react-draft-wysiwyg";
import {Content} from "./Content";

type Props = {
    initialContent: RawDraftContentState,
    onContentChange?: ({raw, html}: Content) => any,
    placeholder?: string
}

class Editor extends ReactComponent<Props, any> {
    ref: any;
    editor?: DraftEditor;
    state = {
        editor: EditorState.createWithContent(convertFromRaw(this.props.initialContent))
    }

    constructor(props: any) {
        super(props);

        this.ref = React.createRef();
    }

    onEditorChange = (editorState: EditorState) => {
        this.setState({editor: editorState})

        let options = {
            blockStyleFn: (block: ContentBlock): RenderConfig => {
                return {
                    attributes: {
                        class: this.getBlockStyle(block)
                    }
                }
            }
        }

        this.props.onContentChange ?
            this.props.onContentChange(
                {
                    html: stateToHTML(editorState.getCurrentContent(), options),
                    raw: convertToRaw(editorState.getCurrentContent())
                }
            ) : null;
    }

    onEditorFocus = () => {
        if (typeof this.editor !== "undefined") {
            this.editor.focus();
        }
    }

    onToolbarToggle = (style: string) => {
        switch (style) {
            case "BOLD":
            case "ITALIC":
            case "UNDERLINE":
                this.onEditorChange(
                    RichUtils.toggleInlineStyle(this.state.editor, style)
                )
                break;
            default:
                this.onEditorChange(
                    RichUtils.toggleBlockType(this.state.editor, style)
                )
        }
    }

    handleKeyCommand = (command: DraftEditorCommand): DraftHandleValue => {
        const {editor} = this.state;
        const newState = RichUtils.handleKeyCommand(editor, command)
        if (newState) {
            this.onEditorChange(newState)
            return 'handled';
        }
        return 'not-handled';
    }

    getBlockStyle(block: ContentBlock): string {
        const type: string = block.getType();

        switch (type) {
            case "ALIGN_RIGHT":
                return 'alignRight';
            case "ALIGN_LEFT":
                return 'alignLeft';
            case "ALIGN_CENTER":
                return "alignCenter";
            default:
                return "";
        }
    }

    getKeyBindings = (e: SyntheticKeyboardEvent): string|null => {
        if (e.keyCode === 9) {
            e.preventDefault();
            return null;
        }
        return getDefaultKeyBinding(e);
    }

    render() {
        if (window === undefined) {
            return null;
        }

        return (
            <div className={styles.wrapper}>
                <div className={styles.toolbar}>
                    <Toolbar editor={this.state.editor} onToggle={this.onToolbarToggle}/>
                </div>
                <div className={styles.editor} onFocus={this.onEditorFocus} onClick={this.onEditorFocus}>
                    <DraftEditor
                        placeholder={this.props.placeholder}
                        customStyleMap={styleMap}
                        blockStyleFn={this.getBlockStyle}
                        onChange={this.onEditorChange}
                        keyBindingFn={this.getKeyBindings}
                        handleKeyCommand={this.handleKeyCommand}
                        editorKey="somekey"
                        ref={(editor: DraftEditor) => {
                            this.editor = editor
                        }}
                        editorState={this.state.editor}
                    />
                </div>
            </div>
        );
    }
}

const styleMap: DraftStyleMap = {
    CODE: {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
        fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
        fontSize: 16,
        padding: 4,
        border: '1px solid black'
    },
    BOLD: {
        fontWeight: 600,
    },
    ITALIC: {
        fontStyle: 'italic',
    },
    UNDERLINE: {
        textDecoration: 'underline',
    }
}

export default Editor;