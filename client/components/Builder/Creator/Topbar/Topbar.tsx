import React, { useState } from 'react';

import Item from './Item/Item';

import styles from './topbar.module.scss';

const topbar = () => {
    const [progress, setProgress] = useState(0);

    let clickEvent = () => {
        setProgress((prevState) => {
            return prevState + 25;
        });
    };

    return (
        <div className={styles.topbar} onClick={clickEvent}>
            <Item description="25%" progress={progress} />
            <Item description="50%" progress={progress - 100} />
            <Item description="75%" progress={progress - 200} />
            <Item description="100%" progress={progress - 300} />
        </div>
    );
};

export default topbar;