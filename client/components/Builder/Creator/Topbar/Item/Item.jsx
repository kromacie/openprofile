import React from 'react';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/free-solid-svg-icons";

import styles from './item.module.scss';
import cx from 'classnames';

const item = ({description, progress}) => {
    let leftComplete = false;
    let middleComplete = false;
    let rightComplete = false;

    if (progress > 0) {
        leftComplete = true;
    }

    if (progress >= 50) {
        middleComplete = true;
    }

    if (progress >= 100) {
        rightComplete = true;
    }

    return (
        <div className={styles.item}>
            <span className={cx(styles.circle, {[styles.complete]: middleComplete})}>
                <span className={styles.icon}>
                    <FontAwesomeIcon icon={faCheck}/>
                </span>
            </span>
            <span className={cx(styles.progressBarLeft, {[styles.complete]: leftComplete})}/>
            <span className={styles.barLeft}/>
            <span className={cx(styles.progressBarRight, {[styles.complete]: rightComplete})}/>
            <span className={styles.barRight}/>
            <span className={cx(styles.subcircle, {[styles.complete]: middleComplete})}/>
            <div className={styles.text}>
                {description}
            </div>
        </div>
    );
};

export default item;