import React, {useEffect, useState} from 'react';

import styles from './carousel.module.scss';

import propsAppend from "../../../../hoc/propsAppend";

const carousel = ({elements, index, watch}) => {
    const [localTarget, setLocalTarget] = useState(0);
    const [initialized, setInitialized] = useState(false);
    const [currentRef, setCurrentRef] = useState(null);
    const [indexes, setIndexes] = useState([index, index]);
    const [toRender, setToRender] = useState([elements[index]]);
    const [reversed, setReversed] = useState(false);
    const [pendingIndexDelete, setPendingIndexDelete] = useState(null);

    useEffect(() => {
        setIndexes((prev) => {
            let next = [...prev];
            next.shift();
            next.push(index);

            return next;
        })
    }, [index]);

    useEffect(() => {
        let appending = [];

        // We check in indexes history which index was bigger
        if (indexes[0] > indexes[1]) {
            setReversed(true);

            // We should only update if it's not currently deleting
            if (pendingIndexDelete !== indexes[0]) {
                appending[0] = propsAppend(
                    elements[indexes[0]],
                    {
                        active: true,
                        hide: true,
                        offsetY: currentRef ? currentRef.clientHeight : 0,
                        onRender: (element) => setCurrentRef(
                            element.current
                        ),
                    }
                );
            }
            appending[1] = propsAppend(
                elements[indexes[1]], {
                    active: true,
                    onRender: (element) => setCurrentRef(
                        element.current
                    ),
                }
            );

            // We set index of current slide to know, which is actually active
            setLocalTarget(1);

        // We check in indexes history which index was bigger
        } else if (indexes[1] > indexes[0]) {
            setReversed(false);

            appending[1] = propsAppend(
                elements[indexes[1]],
                {
                    active: true,
                    onRender: (element) => setCurrentRef(
                        element.current
                    ),
                }
            );

            setLocalTarget(1);

            if (pendingIndexDelete !== indexes[0]) {
                appending[0] = propsAppend(
                    elements[indexes[0]], {
                        active: true,
                        hide: true,
                        offsetY: currentRef ? -currentRef.clientHeight : 0,
                    }
                );
            }
        //If here is just one index in history
        } else {
            appending[0] = propsAppend(
                elements[index],
                {
                    active: true,
                    onRender: (element) => setCurrentRef(
                        element.current
                    ),
                }
            );

            setLocalTarget(0);
        }

        if(!initialized) {
            setInitialized(true);
        }

        setToRender(appending);

    }, [...indexes, pendingIndexDelete])

    useEffect(() => {
        const timer = setTimeout(() => {
            if (indexes[1] !== indexes[0]) {
                setPendingIndexDelete(indexes[0])
            }
        }, 1000);

        return () => clearTimeout(timer);
    }, [indexes]);

    useEffect(() => {
        if(initialized) {
            setToRender((prevState => {
                let newState = [...prevState];
                    newState[localTarget] = propsAppend(prevState[localTarget], {children: elements[index].props.children})
                return newState;
            }))
        }
    }, [...Object.values(watch)]);

    let extra = {};

    if (reversed) {
        extra.flexDirection = 'column-reverse';
    } else {
        extra.flexDirection = 'column';
    }

    return (
        <div style={extra} className={styles.carousel}>
            {toRender}
        </div>
    );
}

export default carousel;