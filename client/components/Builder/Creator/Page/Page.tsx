import React, {useEffect, useRef} from 'react';

import Title from "./Title/Title";

import styles from "./page.module.scss";
import cx from 'classnames';

interface PageProps {
    title: string,
    active?: boolean,
    hide?: boolean,
    children?: React.ReactNode,
    offsetY?: number,
    onRender?: (ref?: React.RefObject<HTMLDivElement>) => void
}

const page = ({active, hide, children, offsetY, onRender, title}: PageProps) => {

    let style;

    if (offsetY !== undefined && offsetY <= 0) {
        style = {
            marginTop: offsetY ? `${offsetY}px` : 0
        };
    } else {
        style = {
            marginBottom: offsetY ? `${-offsetY}px` : 0
        };
    }

    const ref = useRef<HTMLDivElement>(null);

    useEffect(() => {
        onRender ? onRender(ref) : null
    })

    return (
        <div ref={ref} style={style} className={cx(styles.page, {[styles.active]: active, [styles.hide]: hide})}>
            <div className={styles.title}>
                <Title title={title}/>
            </div>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    {children}
                </div>
            </div>
        </div>
    );
};

export default page;

export enum Pages {
    PROFILE = 0,
    EXPERIENCE = 2,
    EDUCATION = 1,
    SKILLS = 3,
    LANGUAGES = 4,
    OTHERS = 5,
    CONFIG = 6,
}