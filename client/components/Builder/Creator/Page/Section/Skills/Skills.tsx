import React from "react";

import Details from "../../../../../Generator/Interfaces/Details";
import Button from "../../../Button/Normal/Normal"
import Item from "./Item/Item";

import styles from '../section.module.scss';

import {Container, Draggable} from "react-smooth-dnd";
import SkillHandlers from "../../../../../Generator/Interfaces/Handlers/SkillHandlers";

interface SkillProps {
    information: {
        handlers: SkillHandlers,
        details: Details
    },
    onSkillItemCreate: () => void,
    onSkillItemDelete: (id: number) => void,
    onSkillItemOrderChange: (from: number, to: number) => void,
}

const skills = ({information, onSkillItemCreate, onSkillItemDelete, onSkillItemOrderChange}: SkillProps) => {
    return (
        <div className={styles.wrapper}>
            <Container lockAxis={"y"} dragHandleSelector=".drag-handler"
                       onDrop={(e) => onSkillItemOrderChange(
                           e.removedIndex !== null ? information.details.skills[e.removedIndex].id : 0,
                           e.addedIndex !== null ? information.details.skills[e.addedIndex].id : 0,
                       )}>
                {information.details.skills.map((element, index) => {
                    return (
                        <Draggable key={element.id} render={() => {
                            return (
                                <Item draggable={true} index={index} information={information} skill={element}
                                      onSkillItemDelete={onSkillItemDelete}/>
                            );
                        }}/>
                    );
                })}
            </Container>

            <div className={styles.buttons}>
                <Button onClick={onSkillItemCreate}>
                    Dodaj umiejętność
                </Button>
            </div>
        </div>
    );
};

export default skills;