import React from 'react';

import cn from "classnames";
import styles from "../../section.module.scss";

import Card from "../../../../Card/Card";
import SkillDetails from "../../../../../../Generator/Interfaces/Details/SkillDetails";
import Skill from "../../../../Card/Skill/Skill";
import SkillHandlers from "../../../../../../Generator/Interfaces/Handlers/SkillHandlers";

interface EducationProps {
    index: number,
    draggable: boolean,
    information: {
        handlers: SkillHandlers,
    },
    onSkillItemDelete?: (id: number) => void,
    skill: SkillDetails,
}

const item = ({skill, information, onSkillItemDelete, index, draggable}: EducationProps) => {
    return (
        <div
            className={cn(styles.card, {
                [styles.active]: skill.active,
                ['smooth-dnd-draggable-wrapper']: draggable,
                ['draggable-card']: draggable,
                ['card-active']: skill.active,
            })}
            data-id={skill.id}
        >
            <Card
                key={skill.id}
                title="Umiejętność"
                number={index + 1}
                onToggle={() => {
                    information.handlers?.onSkillActiveChange(skill.id, skill.active)
                }}
                onClose={() => {
                    onSkillItemDelete ? onSkillItemDelete(skill.id) : null;
                }}
                active={skill.active}
            >
                <Skill
                    handlers={information.handlers}
                    details={skill}
                />
            </Card>
        </div>
    );
}

export default item;