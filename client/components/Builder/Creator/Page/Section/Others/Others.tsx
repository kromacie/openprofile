import React from 'react';

import Textarea from "../../../../../Composition/Form/Textarea/Textarea";
import Group from "../../../../../Composition/Form/Group/Group";

import cx from "classnames";
import styles from "./others.module.scss";
import OthersDetails from "../../../../../Generator/Interfaces/Details/OthersDetails";
import OthersHandlers from "../../../../../Generator/Interfaces/Handlers/OthersHandlers";
import Editor from "../../../Editor/Editor";

interface OthersInformation {
    information: {
        details: OthersDetails,
        handlers: OthersHandlers
    }
}

const Others = ({information}: OthersInformation) => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.others}>
                <h3 className={styles.title}>
                    Dodatkowe informacje
                </h3>
                <div className={cx(styles.col100, styles.mt2)}>
                        <Editor placeholder="Podaj zainteresowania" initialContent={information.details.interests.raw}
                                onContentChange={information.handlers.onInterestsChange}/>
                </div>
                <div className={cx([styles.row, styles.spaced, styles.mt2])}>
                    <div className={cx(styles.col100)}>
                        <Textarea title="Klauzula prywatności" placeholder="Treść klauzuli..." bordered={true}
                                  onChange={information.handlers.onClauseChange} value={information.details.clause}/>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default Others;