import React from "react";

import Details from "../../../../../Generator/Interfaces/Details";
import Button from "../../../Button/Normal/Normal"
import Item from "./Item/Item";

import styles from '../section.module.scss';

import {Container, Draggable} from "react-smooth-dnd";
import LanguageHandlers from "../../../../../Generator/Interfaces/Handlers/LanguageHandlers";

interface LanguageProps {
    information: {
        handlers: LanguageHandlers,
        details: Details
    },
    onLanguageItemCreate: () => void,
    onLanguageItemDelete: (id: number) => void,
    onLanguageItemOrderChange: (from: number, to: number) => void,
}

const language = ({information, onLanguageItemCreate, onLanguageItemDelete, onLanguageItemOrderChange}: LanguageProps) => {
    return (
        <div className={styles.wrapper}>
            <Container lockAxis={"y"} dragHandleSelector=".drag-handler"
                       onDrop={(e) => onLanguageItemOrderChange(
                           e.removedIndex !== null ? information.details.languages[e.removedIndex].id : 0,
                           e.addedIndex !== null ? information.details.languages[e.addedIndex].id : 0,
                       )}>
                {information.details.languages.map((element, index) => {
                    return (
                        <Draggable key={element.id} render={() => {
                            return (
                                <Item draggable={true} index={index} information={information} language={element}
                                      onLanguageItemDelete={onLanguageItemDelete}/>
                            );
                        }}/>
                    );
                })}
            </Container>

            <div className={styles.buttons}>
                <Button onClick={onLanguageItemCreate}>
                    Dodaj język
                </Button>
            </div>
        </div>
    );
};

export default language;