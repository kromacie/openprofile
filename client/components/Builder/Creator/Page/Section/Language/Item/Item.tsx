import React from 'react';

import cn from "classnames";
import styles from "../../section.module.scss";

import Card from "../../../../Card/Card";
import LanguageDetails from "../../../../../../Generator/Interfaces/Details/LanguageDetails";
import Language from "../../../../Card/Language/Language";
import LanguageHandlers from "../../../../../../Generator/Interfaces/Handlers/LanguageHandlers";

interface EducationProps {
    index: number,
    draggable: boolean,
    information: {
        handlers: LanguageHandlers,
    },
    onLanguageItemDelete?: (id: number) => void,
    language: LanguageDetails,
}

const item = ({language, information, onLanguageItemDelete, index, draggable}: EducationProps) => {
    return (
        <div
            className={cn(styles.card, {
                [styles.active]: language.active,
                ['smooth-dnd-draggable-wrapper']: draggable,
                ['draggable-card']: draggable,
                ['card-active']: language.active,
            })}
            data-id={language.id}
        >
            <Card
                key={language.id}
                title="Język"
                number={index + 1}
                onToggle={() => {
                    information.handlers?.onLanguageActiveChange(language.id, language.active)
                }}
                onClose={() => {
                    onLanguageItemDelete ? onLanguageItemDelete(language.id) : null;
                }}
                active={language.active}
            >
                <Language
                    handlers={information.handlers}
                    details={language}
                />
            </Card>
        </div>
    );
}

export default item;