import React, {Fragment} from 'react';

import {
    faBriefcase,
    faCalendar,
    faCalendarDay,
    faCalendarWeek,
    faEnvelope,
    faHome,
    faPhone,
    faUser
} from "@fortawesome/free-solid-svg-icons";

import Group from "../../../../../Composition/Form/Group/Group";
import Input from "../../../../../Composition/Form/Input/Input";

import ProfileDetails from "../../../../../Generator/Interfaces/Details/ProfileDetails";
import ProfileHandlers from "../../../../../Generator/Interfaces/Handlers/ProfileHandlers";

import cx from "classnames";
import styles from "./profile.module.scss";
import File from "../../../../../Composition/Form/File/File";


interface ProfileInformation {
    information: {
        details: ProfileDetails,
        handlers: ProfileHandlers
    }
}

const profile = ({information}: ProfileInformation) => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.profile}>
                <h3 className={styles.title}>
                    Podaj dane osobowe
                </h3>
                <div className={cx([styles.row, styles.spaced, styles.mt1])}>
                    <div className={cx(styles.col23, styles.mt1)}>
                        <Input title="Imię" placeholder="Podaj imię" icon={faUser}
                               onChange={information.handlers.onNameChange} value={information.details.name}/>
                        <div className={styles.mt2}>
                            <Input title="Nazwisko" placeholder="Podaj nazwisko" icon={faUser}
                                   onChange={information.handlers.onSurnameChange} value={information.details.surname}/>
                        </div>
                        <div className={styles.mt2}>
                            <Input title="Stanowisko" placeholder="Podaj stanowisko na które aplikujesz"
                                   icon={faBriefcase}
                                   onChange={information.handlers.onTitleChange} value={information.details.title}/>
                        </div>
                    </div>
                    <div className={cx(styles.col13, styles.mt1)}>
                        <File icon={faUser} onChange={information.handlers.onImageChange}>
                            Dodaj zdjęcie
                        </File>
                    </div>
                </div>
                <div className={cx(styles.col100, styles.mt2)}>
                    <Input title="Email" placeholder="Podaj email" icon={faEnvelope}
                           onChange={information.handlers.onEmailChange} value={information.details.email}/>
                </div>
                <div className={cx(styles.col100, styles.mt2)}>
                    <Input title="Telefon" placeholder="Podaj numer telefonu" icon={faPhone}
                           onChange={information.handlers.onPhoneChange} value={information.details.telephone}/>
                </div>
                <div className={cx(styles.col100, styles.mt2)}>
                    <Input title="Adres" placeholder="Podaj adres zamieszkania" icon={faHome}
                           onChange={information.handlers.onAddressChange} value={information.details.address}/>
                </div>
                <Group title="Data urodzenia">
                    <div className={cx([styles.row, styles.spaced])}>
                        <div className={cx(styles.col100, styles.mt2)}>
                            <Input title="Rok" placeholder="Podaj rok" icon={faCalendar}
                                   onChange={information.handlers.onBirthdayYearChange}
                                   value={information.details.birthday.year}/>
                        </div>
                        <div className={cx(styles.col100, styles.mt2)}>
                            <Input title="Miesiąc" placeholder="Podaj miesiąc" icon={faCalendarWeek}
                                   onChange={information.handlers.onBirthdayMonthChange}
                                   value={information.details.birthday.month}/>
                        </div>
                        <div className={cx(styles.col100, styles.mt2)}>
                            <Input title="Dzień" placeholder="Podaj dzień" icon={faCalendarDay}
                                   onChange={information.handlers.onBirthdayDayChange}
                                   value={information.details.birthday.day}/>
                        </div>
                    </div>
                </Group>
            </div>
        </div>
    )
};

export default profile;