import React from 'react';
import cn from "classnames";
import styles from "../../section.module.scss";
import Card from "../../../../Card/Card";
import ExperienceDetails from "../../../../../../Generator/Interfaces/Details/ExperienceDetails";
import Experience from "../../../../Card/Experience/Experience";
import ExperienceHandlers from "../../../../../../Generator/Interfaces/Handlers/ExperienceHandlers";

interface EducationProps {
    index: number,
    draggable: boolean,
    information: {
        handlers: ExperienceHandlers,
    },
    onExperienceItemDelete?: (id: number) => void,
    experience: ExperienceDetails,
}

const item = ({experience, information, onExperienceItemDelete, index, draggable}: EducationProps) => {
    return (
        <div
            className={cn(styles.card, {
                [styles.active]: experience.active,
                ['smooth-dnd-draggable-wrapper']: draggable,
                ['draggable-card']: draggable,
                ['card-active']: experience.active,
            })}
            data-id={experience.id}
        >
            <Card
                key={experience.id}
                title="Doświadczenie"
                number={index + 1}
                onToggle={() => {
                    information.handlers?.onExperienceActiveChange(experience.id, experience.active)
                }}
                onClose={() => {
                    onExperienceItemDelete ? onExperienceItemDelete(experience.id) : null;
                }}
                active={experience.active}
            >
                <Experience
                    onContentChange={(content) => information.handlers?.onExperienceDescriptionChange(experience.id, content)}
                    handlers={information.handlers}
                    details={experience}
                />
            </Card>
        </div>
    );
}

export default item;