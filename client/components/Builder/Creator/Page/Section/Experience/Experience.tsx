import React from "react";

import Details from "../../../../../Generator/Interfaces/Details";
import Button from "../../../Button/Normal/Normal"
import Item from "./Item/Item";

import styles from '../section.module.scss';

import {Container, Draggable} from "react-smooth-dnd";
import ExperienceHandlers from "../../../../../Generator/Interfaces/Handlers/ExperienceHandlers";

interface ExperienceProps {
    information: {
        handlers: ExperienceHandlers,
        details: Details
    },
    onExperienceItemCreate: () => void,
    onExperienceItemDelete: (id: number) => void,
    onExperienceItemOrderChange: (from: number, to: number) => void,
}

const experience = ({information, onExperienceItemCreate, onExperienceItemDelete, onExperienceItemOrderChange}: ExperienceProps) => {
    return (
        <div className={styles.wrapper}>
            <Container lockAxis={"y"} dragHandleSelector=".drag-handler"
                       onDrop={(e) => onExperienceItemOrderChange(
                           e.removedIndex !== null ? information.details.experiences[e.removedIndex].id : 0,
                           e.addedIndex !== null ? information.details.experiences[e.addedIndex].id : 0,
                       )}>
                {information.details.experiences.map((element, index) => {
                    return (
                        <Draggable key={element.id} render={() => {
                            return (
                                <Item draggable={true} index={index} information={information} experience={element}
                                      onExperienceItemDelete={onExperienceItemDelete}/>
                            );
                        }}/>
                    );
                })}
            </Container>

            <div className={styles.buttons}>
                <Button onClick={onExperienceItemCreate}>
                    Dodaj doświadczenie
                </Button>
            </div>
        </div>
    );
};

export default experience;