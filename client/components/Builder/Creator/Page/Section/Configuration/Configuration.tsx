import React from 'react';

import styles from "./configuration.module.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import ConfigDetails from "../../../../../Generator/Interfaces/Details/ConfigDetails";
import cn from "classnames";
import ConfigHandlers from "../../../../../Generator/Interfaces/Handlers/ConfigHandlers";

interface ConfigInformation {
    information: {
        config: ConfigDetails,
        handlers: ConfigHandlers
    }
}

const configuration = ({information}: ConfigInformation) => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.profile}>
                <h3 className={styles.title}>
                    Konfiguracja
                </h3>
                <div className={styles.items}>
                    <div className={styles.desc}>
                        Włącz/Wyłącz sekcję
                    </div>

                    <div className={styles.item}>
                        Edukacja
                        <span className={styles.visibility} onClick={
                            () => information.handlers.onSheetConfigVisibilityChange('isEducationVisible', !information.config.isEducationVisible)
                        }>
                            <FontAwesomeIcon icon={information.config.isEducationVisible ? faEye : faEyeSlash}/>
                        </span>
                    </div>
                    <div className={styles.item}>
                        Doświadczenie
                        <span className={styles.visibility} onClick={
                            () => information.handlers.onSheetConfigVisibilityChange('isExperienceVisible', !information.config.isExperienceVisible)
                        }>
                            <FontAwesomeIcon icon={information.config.isExperienceVisible ? faEye : faEyeSlash}/>
                        </span>
                    </div>
                    <div className={styles.item}>
                        Umiejętności
                        <span className={styles.visibility} onClick={
                            () => information.handlers.onSheetConfigVisibilityChange('isSkillsVisible', !information.config.isSkillsVisible)
                        }>
                            <FontAwesomeIcon icon={information.config.isSkillsVisible ? faEye : faEyeSlash}/>
                        </span>
                    </div>
                    <div className={styles.item}>
                        Języki
                        <span className={styles.visibility} onClick={
                            () => information.handlers.onSheetConfigVisibilityChange('isLanguageVisible', !information.config.isLanguageVisible)
                        }>
                            <FontAwesomeIcon icon={information.config.isLanguageVisible ? faEye : faEyeSlash}/>
                        </span>
                    </div>
                    <div className={styles.item}>
                        Zainteresowania
                        <span className={styles.visibility} onClick={
                            () => information.handlers.onSheetConfigVisibilityChange('isInterestsVisible', !information.config.isInterestsVisible)
                        }>
                            <FontAwesomeIcon icon={information.config.isInterestsVisible ? faEye : faEyeSlash}/>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default configuration;