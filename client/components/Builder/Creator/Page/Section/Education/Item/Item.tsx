import React from 'react';
import cn from "classnames";
import styles from "../../section.module.scss";
import Card from "../../../../Card/Card";
import Education from "../../../../Card/Education/Education";
import EducationDetails from "../../../../../../Generator/Interfaces/Details/EducationDetails";
import EducationHandlers from "../../../../../../Generator/Interfaces/Handlers/EducationHandlers";


interface EducationProps {
    index: number,
    draggable: boolean,
    information: {
        handlers?: EducationHandlers,
    },
    onEducationItemDelete?: (id: number) => void,
    education: EducationDetails,
}

const item = ({education, information, onEducationItemDelete, index, draggable}: EducationProps) => {
    return (
        <div
            className={cn(styles.card, {
                [styles.active]: education.active,
                ['smooth-dnd-draggable-wrapper']: draggable,
                ['draggable-card']: draggable,
                ['card-active']: education.active,
            })}
            data-id={education.id}
        >
            <Card
                title="Uczelnia"
                number={index + 1}
                onToggle={() => {
                    information.handlers?.onEducationActiveChange(education.id, education.active)
                }}
                onClose={() => {
                    onEducationItemDelete ? onEducationItemDelete(education.id) : null;
                }}
                active={education.active}
            >
                <Education
                    onContentChange={(content) => information.handlers?.onEducationDescriptionChange(education.id, content)}
                    handlers={information.handlers}
                    details={education}
                />
            </Card>
        </div>
    );
}

export default item;