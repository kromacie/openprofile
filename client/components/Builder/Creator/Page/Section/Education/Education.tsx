import React from "react";

import Details from "../../../../../Generator/Interfaces/Details";
import Button from "../../../Button/Normal/Normal";
import Item from "./Item/Item";

import {Container, Draggable} from 'react-smooth-dnd';

import styles from '../section.module.scss';
import EducationHandlers from "../../../../../Generator/Interfaces/Handlers/EducationHandlers";

interface EducationProps {
    information: {
        handlers: EducationHandlers,
        details: Details
    },
    onEducationItemCreate: () => void,
    onEducationItemDelete: (id: number) => void,
    onEducationItemOrderChange: (from: number, to: number) => void,
}

const education = ({information, onEducationItemCreate, onEducationItemDelete, onEducationItemOrderChange}: EducationProps) => {
    return (
        <div className={styles.wrapper}>
            <Container lockAxis={"y"} dragHandleSelector=".drag-handler"
                       getChildPayload={(index) => {
                           return information.details.educations[index];
                       }}
                       onDrop={(e) => onEducationItemOrderChange(
                           e.removedIndex !== null ? information.details.educations[e.removedIndex].id : 0,
                           e.addedIndex !== null ? information.details.educations[e.addedIndex].id : 0,
                       )}>
                {information.details.educations.map((element, index) => {
                    return (
                        <Draggable key={element.id} render={() => {
                            return (
                                <Item draggable={true} index={index} education={element} information={information}
                                      onEducationItemDelete={onEducationItemDelete}/>
                            );
                        }}/>
                    );
                })}
            </Container>

            <div className={styles.buttons}>
                <Button onClick={onEducationItemCreate}>
                    Dodaj uczelnię
                </Button>
            </div>
        </div>
    );
};

export default education;