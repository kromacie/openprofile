import React, {Fragment} from "react";
import Input from "../../../../Composition/Form/Input/Input";
import Radio from "../../../../Composition/Form/Radio/Radio";
import Textarea from "../../../../Composition/Form/Textarea/Textarea";

import {faCalendarWeek, faPen, faLanguage} from "@fortawesome/free-solid-svg-icons";
import cx from "classnames";
import styles from "../card.module.scss";
import LanguageDetails from "../../../../Generator/Interfaces/Details/LanguageDetails";
import LanguageHandlers from "../../../../Generator/Interfaces/Handlers/LanguageHandlers";

type Props = {
    details: LanguageDetails,
    handlers?: LanguageHandlers,
}

const Language = ({details, handlers}: Props) => {
    const id = details.id;

    return (
        <Fragment>
            <div>
                <Input onChange={(e) => handlers?.onLanguageNameChange(id, e.target.value)} icon={faLanguage}
                       placeholder="Angielski" animateTitle={true}
                       value={details.name}
                       title="Nazwa języka"/>
            </div>
            <div className={cx(styles.row, styles.spaced)}>
                <div className={cx(styles.col100, styles.mt2)}>
                    <Input onChange={(e) => handlers?.onLanguageOpinionChange(id, e.target.value)} icon={faPen}
                           value={details.opinion}
                           placeholder="B2/B1" animateTitle={true} title="Opis oceny języka"/>
                </div>
            </div>
            <div className={cx(styles.row)}>
                <div className={cx(styles.col100, styles.mt2)}>
                    <Radio
                        title={"Ocena języka"}
                        first={details.rating}
                        elements={[
                            {
                                render: () => "1", value: 1, handler: (value) => {
                                    handlers?.onLanguageRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "2", value: 2, handler: (value) => {
                                    handlers?.onLanguageRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "3", value: 3, handler: (value) => {
                                    handlers?.onLanguageRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "4", value: 4, handler: (value) => {
                                    handlers?.onLanguageRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "5", value: 5, handler: (value) => {
                                    handlers?.onLanguageRatingChange(id, value)
                                }
                            },
                        ]}
                    />
                </div>
            </div>

            <div className={cx(styles.mt2)}>
                <Textarea title={"Opis języka"}
                          placeholder={"Opisz swój język"}
                          onChange={(value) => handlers?.onLanguageDescriptionChange(id, value)}
                          value={details.description}/>
            </div>
        </Fragment>
    );
}

export default Language;