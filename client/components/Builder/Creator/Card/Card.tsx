import React, {Fragment, ReactNode, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {faBars, faAngleDown} from "@fortawesome/free-solid-svg-icons";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

import styles from './card.module.scss';
import cx from "classnames";

type Props = {
    title: string,
    number: number,
    active: boolean,
    onToggle: () => void,
    onClose: () => void,
    onDragEnable?: () => void,
    onDragDisable?: () => void,
    children: ReactNode,
}

const card = ({active, onToggle, onClose, title, number, children, onDragEnable, onDragDisable}: Props) => {
    return (
        <Fragment>
            <div className={cx(styles.card, {[styles.active]: active})}>
                <div className={styles.title}>
                    <span className={styles.icon}>
                        {title} <span className={styles.number}>{number}</span>
                    </span>
                    <div className={cx(styles.drag, {[styles.active]: active}, 'drag-handler')}
                         onMouseEnter={onDragEnable} onMouseLeave={onDragDisable}
                    >
                        <FontAwesomeIcon icon={faBars}/>
                    </div>
                    <div className={cx(styles.toggle, {[styles.active]: active})} onClick={onToggle}>
                        <FontAwesomeIcon icon={faAngleDown}/>
                    </div>
                    <div className={cx(styles.close, {[styles.active]: active})} onClick={onClose}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </div>
                </div>
                <div className={styles.content}>
                    {active ? children : null}
                </div>
            </div>
        </Fragment>
    );
};

export default card;