import React, {Fragment} from "react";
import Input from "../../../../Composition/Form/Input/Input";
import Radio from "../../../../Composition/Form/Radio/Radio";
import Textarea from "../../../../Composition/Form/Textarea/Textarea";

import {faCalendarWeek, faPen, faTools} from "@fortawesome/free-solid-svg-icons";
import cx from "classnames";
import styles from "../card.module.scss";

import SkillDetails from "../../../../Generator/Interfaces/Details/SkillDetails";
import SkillHandlers from "../../../../Generator/Interfaces/Handlers/SkillHandlers";

type Props = {
    details: SkillDetails,
    handlers?: SkillHandlers,
}

const Skill = ({details, handlers}: Props) => {
    const id = details.id;

    return (
        <Fragment>
            <div>
                <Input onChange={(e) => handlers?.onSkillNameChange(id, e.target.value)} icon={faTools}
                       placeholder="Pakiet biurowy" animateTitle={true}
                       value={details.name}
                       title="Nazwa umiejętności"/>
            </div>
            <div className={cx(styles.row, styles.spaced)}>
                <div className={cx(styles.col100, styles.mt2)}>
                    <Input onChange={(e) => handlers?.onSkillOpinionChange(id, e.target.value)} icon={faPen}
                           value={details.opinion}
                           placeholder="Zaawansowany/Dobry" animateTitle={true} title="Opis oceny umiejętności"/>
                </div>
            </div>
            <div className={cx(styles.row)}>
                <div className={cx(styles.col100, styles.mt2)}>
                    <Radio
                        title={"Ocena umiejętności"}
                        first={details.rating}
                        elements={[
                            {
                                render: () => "1", value: 1, handler: (value) => {
                                    handlers?.onSkillRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "2", value: 2, handler: (value) => {
                                    handlers?.onSkillRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "3", value: 3, handler: (value) => {
                                    handlers?.onSkillRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "4", value: 4, handler: (value) => {
                                    handlers?.onSkillRatingChange(id, value)
                                }
                            },
                            {
                                render: () => "5", value: 5, handler: (value) => {
                                    handlers?.onSkillRatingChange(id, value)
                                }
                            },
                        ]}
                    />
                </div>
            </div>

            <div className={cx(styles.mt2)}>
                <Textarea title={"Opis umiejętności"}
                          placeholder={"Opisz swoją umiejętność"}
                          onChange={(value) => handlers?.onSkillDescriptionChange(id, value)}
                          value={details.description}/>
            </div>
        </Fragment>
    );
}

export default Skill;