import React, {Fragment} from "react";
import Input from "../../../../Composition/Form/Input/Input";
import Optional from "../../../../Composition/Form/Optional/Optional";
import {faCalendarWeek, faGraduationCap} from "@fortawesome/free-solid-svg-icons";
import cx from "classnames";
import styles from "../card.module.scss";
import Editor from "../../Editor/Editor";
import EducationDetails from "../../../../Generator/Interfaces/Details/EducationDetails";

import {Content} from "../../Editor/Content";
import EducationHandlers from "../../../../Generator/Interfaces/Handlers/EducationHandlers";

type Props = {
    onContentChange: (content: Content) => any,
    details: EducationDetails,
    handlers?: EducationHandlers,
}

const Education = ({onContentChange, details, handlers}: Props) => {
    const id = details.id;

    return (
        <Fragment>
            <div>
                <Input onChange={(e) => handlers?.onEducationTitleChange(id, e.target.value)} icon={faGraduationCap}
                       placeholder="Szkoła zawodowa nr. 7 im. Józefa Piłsudzkiego" animateTitle={true}
                       value={details.title}
                       title="Nazwa szkoły"/>
            </div>
            <div className={cx(styles.row, styles.spaced)}>
                <div className={cx(styles.col12, styles.mt2)}>
                    <Input onChange={(e) => handlers?.onEducationDateFromChange(id, e.target.value)}
                           icon={faCalendarWeek} placeholder="2017-05" animateTitle={true} value={details.date.from}
                           title="Czas trwania od"/>
                </div>
                <div className={cx(styles.col12, styles.mt2)}>
                    <Optional onChange={(e) => handlers?.onEducationDateToChange(id, e.target.value)}
                              icon={faCalendarWeek}
                              placeholder="2018-06" animateTitle={true} value={details.date.to}
                              title="Czas trwania do"/>
                </div>
            </div>
            <div className={cx(styles.row, styles.mt2)}>
                <Editor initialContent={details.description.raw} onContentChange={onContentChange}
                        placeholder="Opisz swoją edukację..."/>
            </div>
        </Fragment>
    );
}

export default Education;