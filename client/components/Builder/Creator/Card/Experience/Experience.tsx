import React, {Fragment} from "react";
import Input from "../../../../Composition/Form/Input/Input";
import {faCalendarWeek, faBriefcase, faBuilding} from "@fortawesome/free-solid-svg-icons";
import cx from "classnames";
import styles from "../card.module.scss";
import Editor from "../../Editor/Editor";

import {Content} from "../../Editor/Content";
import ExperienceDetails from "../../../../Generator/Interfaces/Details/ExperienceDetails";
import ExperienceHandlers from "../../../../Generator/Interfaces/Handlers/ExperienceHandlers";

type Props = {
    onContentChange?: (content: Content) => any,
    details: ExperienceDetails,
    handlers?: ExperienceHandlers,
}

const Experience = ({onContentChange, details, handlers}: Props) => {
    const id = details.id;

    return (
        <Fragment>
            <div>
                <Input onChange={(e) => handlers?.onExperienceTitleChange(id, e.target.value)} icon={faBriefcase}
                       placeholder="Junior PHP Developer" animateTitle={true}
                       value={details.title}
                       title="Nazwa stanowiska"/>
            </div>
            <div className={cx(styles.mt2)}>
                <Input onChange={(e) => handlers?.onExperiencePositionChange(id, e.target.value)} icon={faBuilding}
                       placeholder="Asplex we Wrocławiu" animateTitle={true}
                       value={details.position}
                       title="Nazwa firmy"/>
            </div>
            <div className={cx(styles.row, styles.spaced)}>
                <div className={cx(styles.col12, styles.mt2)}>
                    <Input onChange={(e) => handlers?.onExperienceDateFromChange(id, e.target.value)}
                           value={details.date.from}
                           icon={faCalendarWeek} placeholder="2017-05" animateTitle={true} title="Czas trwania od"/>
                </div>
                <div className={cx(styles.col12, styles.mt2)}>
                    <Input onChange={(e) => handlers?.onExperienceDateToChange(id, e.target.value)} icon={faCalendarWeek}
                           value={details.date.to}
                           placeholder="2018-06" animateTitle={true} title="Czas trwania do"/>
                </div>
            </div>
            <div className={cx(styles.row, styles.mt2)}>
                <Editor initialContent={details.description.raw} onContentChange={onContentChange} placeholder="Opisz swoje doświadczenie.." />
            </div>
        </Fragment>
    );
}

export default Experience;