import React, {ReactNode} from 'react';

import styles from './normal.module.scss';

import cn from "classnames";

type Props = {
    children: ReactNode,
    onClick?: () => void,
    outline?: boolean
}

const Normal = ({children, onClick, outline = true}: Props) => {
    return (
        <button onClick={onClick} className={cn(styles.button, {[styles.outline]: outline})}>
            {children}
        </button>
    );
};

export default Normal;