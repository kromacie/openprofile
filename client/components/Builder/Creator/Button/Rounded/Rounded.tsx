import React, {ReactNode} from 'react';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import cx from 'classnames';

import styles from './rounded.module.scss';
import {IconDefinition} from "@fortawesome/fontawesome-svg-core";

interface ButtonProps {
    direction?: Direction,
    onClick: () => void,
    theme: Theme,
    icon: IconDefinition,
    children: ReactNode,
    disabled?: boolean,
    slide?: boolean,
    style?: Style
}

const rounded = ({ onClick, theme, icon, children, disabled, direction = Direction.RIGHT, slide = true, style = Style.NORMAL }: ButtonProps) => {
    return (
        <div className={cx(styles.button, styles[direction], styles[style], {[styles[theme]]: theme, [styles.disabled]: disabled, [styles.slide]: slide})} onClick={onClick}>
            <div className={styles.icon}>
                <FontAwesomeIcon icon={icon} />
            </div>
            <div className={styles.content}>
                {children}
            </div>
        </div>
    );
};

export default rounded;

export enum Direction {
    LEFT = 'directionLeft',
    RIGHT = 'directionRight'
}

export enum Theme {
    PRIMARY = 'primary',
    DANGER = 'danger'
}

export enum Style {
    OUTLINE = 'styleOutline',
    NORMAL = 'styleNormal'
}
