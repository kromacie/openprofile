import styles from "./indicator.module.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faExclamation, faPrint, faSpinner} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import cn from "classnames";

export enum UpdateCodes {
    PENDING = 'pending',
    READY = 'ready',
    ERROR = 'error',
}

interface IndicatorProps {
    onReadyClick: () => void,
    status: UpdateCodes,
}

const getIcon = (status: UpdateCodes) => {
    switch (status) {
        case UpdateCodes.ERROR:
            return faExclamation;
        case UpdateCodes.PENDING:
            return faSpinner;
        case UpdateCodes.READY:
        default:
            return faPrint;
    }
}

const Indicator = (props: IndicatorProps) => {
    return (
        <div className={cn(styles.indicator, styles[props.status])} onClick={() => {
            if(props.status === UpdateCodes.READY) {
                props.onReadyClick();
            }
        }}>
            <div className={styles.icon}>
                <FontAwesomeIcon icon={getIcon(props.status)}/>
            </div>
        </div>
    );
}

export default Indicator;