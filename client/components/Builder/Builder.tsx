import React from 'react';

import Navigation from './Navigation/Navigation';
import Creator from "./Creator/Creator";

import styles from './builder.module.scss';
import Details from "../Generator/Interfaces/Details";
import Handlers from "../Generator/Interfaces/Handlers";
import {UpdateCodes} from "./Creator/Indicator/Indicator";

interface BuilderProps {
    information: {
        details: Details,
        handlers: Handlers
    },
    navigation: {
        open: boolean,
        currentElement: number,
    },
    pages: object,
    onNavigationItemClick: (index: number) => void,
    onNavigationToggle: () => void,
    onReadyClick: () => void,
    updating: UpdateCodes
}

const builder = ({navigation, information, onNavigationItemClick, onNavigationToggle, pages, updating, onReadyClick}: BuilderProps) => {
    return (
        <div className={styles.builder}>
            <Navigation navigationOpen={navigation.open} onNavigationToggle={onNavigationToggle}
                        onItemClick={onNavigationItemClick} currentItem={navigation.currentElement}/>
            <Creator
                information={information}
                navigation={navigation}
                onNavigationItemClick={onNavigationItemClick}
                onReadyClick={onReadyClick}
                pages={pages}
                updating={updating}
            />
        </div>
    );
};

export default builder;