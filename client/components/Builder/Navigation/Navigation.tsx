import React from 'react';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faArrowLeft,
    faBriefcase,
    faEllipsisH,
    faGraduationCap,
    faLanguage, faLayerGroup,
    faPrint, faTools,
    faUserAlt
} from "@fortawesome/free-solid-svg-icons";

import {Pages} from "../Creator/Page/Page";
import Item from "../../Composition/Navigation/Item/Item";
import BaseNavigation from "../../Composition/Navigation/Navigation";


interface NavigationProps {
    onNavigationToggle: () => void,
    onItemClick: (index: number) => void,
    navigationOpen: boolean,
    currentItem: number
}

const Navigation = ({onNavigationToggle, navigationOpen, onItemClick, currentItem}: NavigationProps) => {

    const open = navigationOpen;

    return (
        <BaseNavigation navigationOpen={navigationOpen} topItems={[
            <Item
                key={Pages.PROFILE}
                icon={<FontAwesomeIcon icon={faUserAlt}/>}
                title="Profil"
                open={open}
                onClick={() => onItemClick(Pages.PROFILE)}
                clicked={currentItem == Pages.PROFILE}/>,
            <Item
                key={Pages.EDUCATION}
                icon={<FontAwesomeIcon icon={faGraduationCap}/>}
                open={open}
                onClick={() => onItemClick(Pages.EDUCATION)}
                clicked={currentItem == Pages.EDUCATION}
                title="Edukacja"/>,
            <Item
                key={Pages.EXPERIENCE}
                icon={<FontAwesomeIcon icon={faBriefcase}/>}
                open={open}
                onClick={() => onItemClick(Pages.EXPERIENCE)}
                clicked={currentItem == Pages.EXPERIENCE}
                title="Doświadczenie"/>,
            <Item
                key={Pages.SKILLS}
                icon={<FontAwesomeIcon icon={faTools}/>}
                open={open}
                onClick={() => onItemClick(Pages.SKILLS)}
                clicked={currentItem == Pages.SKILLS}
                title="Umiejętności"/>,
            <Item
                key={Pages.LANGUAGES}
                icon={<FontAwesomeIcon icon={faLanguage}/>}
                open={open}
                onClick={() => onItemClick(Pages.LANGUAGES)}
                clicked={currentItem == Pages.LANGUAGES}
                title="Języki"/>,
            <Item
                key={Pages.OTHERS}
                icon={<FontAwesomeIcon icon={faEllipsisH}/>}
                open={open}
                onClick={() => onItemClick(Pages.OTHERS)}
                clicked={currentItem == Pages.OTHERS}
                title="Inne"/>,
        ]} bottomItems={[
            <Item
                key={Pages.CONFIG}
                icon={<FontAwesomeIcon icon={faLayerGroup}/>}
                open={open}
                onClick={() => onItemClick(Pages.CONFIG)}
                clicked={currentItem == Pages.CONFIG}
                title="Konfiguracja"/>,
            <Item
                key={"Zwiń"}
                icon={<FontAwesomeIcon icon={faArrowLeft}/>}
                title="Zwiń"
                open={open}
                onClick={onNavigationToggle}
                spin={true}
                bottom={true}/>
        ]}/>
    );
};

export default Navigation;