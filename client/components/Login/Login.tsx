import React, {FormEvent, Fragment, useState} from 'react';

import Logo from '../Composition/Logo/Logo';
import Normal from "../Builder/Creator/Button/Normal/Normal";
import styles from './login.module.scss'
import Input from '../Composition/Form/Input/Input'
import {faPen, faUser} from "@fortawesome/free-solid-svg-icons";
import cn from "classnames";
import Background from "../Composition/Background/Background";
import Router from "next/router";
import Error from "../Composition/Log/Error/Error";
import Authenticator from "../../services/Authenticator/Authenticator";
import Unauthenticated from "../../services/Authenticator/Exceptions/Unauthenticated";

const Login = () => {
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const [hasError, setHasError] = useState(false);

    const onSubmit = (e: FormEvent) => {
        e.preventDefault();

        Authenticator
            .login(login, password, 3)
            .then(async () => {
                await Router.push("/")
            })
            .catch((e) => {
                if (e instanceof Unauthenticated) {
                    setHasError(true)
                    setError("Błędny login lub hasło")
                } else {
                    console.warn(e);
                }

            })
    }

    return (
        <Fragment>
            <Background/>
            <div className={styles.wrapper}>
                <div className={styles.login}>
                    <div className={styles.navigation}>
                        <Logo/>
                    </div>
                    <form onSubmit={onSubmit} className={styles.main}>
                        <h1 className={styles.title}>
                            Logowanie
                        </h1>
                        {hasError ? (
                            <Error>
                                {error}
                            </Error>
                        ) : ""}
                        <div className={styles.mt2}>
                            <Input title="Login" value={login} onChange={(e) => setLogin(e.target.value)}
                                   placeholder="Nazwa użytkownika" icon={faUser}/>
                        </div>
                        <div className={styles.mt2}>
                            <Input title="Hasło" value={password} onChange={(e) => setPassword(e.target.value)}
                                   placeholder="Hasło użytkownika" hidden={true} icon={faPen}/>
                        </div>
                        <div className={cn(styles.mt2, styles.row)}>
                            <div className={cn(styles.col13, styles.button)}>
                                <Normal outline={false}>
                                    Zaloguj
                                </Normal>
                            </div>
                            <div className={cn(styles.col23, styles.forgotWrapper)}>
                                <a href="#" className={styles.forgot}>
                                    Nie pamiętasz hasła?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Fragment>
    );
}

export default Login;