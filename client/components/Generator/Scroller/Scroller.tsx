import React, {ReactNode} from "react";

import styles from './scroller.module.scss';

const scroller = ({children}: {children: ReactNode}) => {
    return (
        <div className={styles.scroller}>
            {children}
        </div>
    )
}

export default scroller;