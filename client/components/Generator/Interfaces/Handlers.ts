import ProfileHandlers from "./Handlers/ProfileHandlers";
import EducationHandlers from "./Handlers/EducationHandlers";
import ExperienceHandlers from "./Handlers/ExperienceHandlers";
import SkillHandlers from "./Handlers/SkillHandlers";
import LanguageHandlers from "./Handlers/LanguageHandlers";
import ConfigHandlers from "./Handlers/ConfigHandlers";

interface Handlers extends ProfileHandlers, EducationHandlers, ExperienceHandlers, SkillHandlers, LanguageHandlers, ConfigHandlers {
}

export default Handlers;