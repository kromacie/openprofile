interface ProfileDetails {
    name: string,
    surname: string,
    email: string,
    address: string,
    telephone: string,
    title: string,
    image: {
        url: string
    }
    birthday: {
        year: string,
        month: string,
        day: string
    }
}

export default ProfileDetails;