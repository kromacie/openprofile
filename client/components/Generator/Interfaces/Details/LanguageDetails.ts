import IndexableDetails from "./IndexableDetails";

interface LanguageDetails extends IndexableDetails {
    name: string,
    active: boolean,
    rating: number,
    opinion: string,
    description: string,
}

export default LanguageDetails;