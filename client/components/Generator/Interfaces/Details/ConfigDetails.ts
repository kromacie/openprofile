export default interface ConfigDetails {
    isExperienceVisible: boolean,
    isEducationVisible: boolean,
    isSkillsVisible: boolean,
    isLanguageVisible: boolean,
    isInterestsVisible: boolean,
}