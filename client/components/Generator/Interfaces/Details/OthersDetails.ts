import {Content} from "../../../Builder/Creator/Editor/Content";

export default interface OthersDetails {
    clause: string,
    interests: Content,
}