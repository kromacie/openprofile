import IndexableDetails from "./IndexableDetails";

export default interface SkillDetails extends IndexableDetails{
    name: string,
    rating: number,
    opinion: string,
    hasRating: boolean,
    description: string,
    active: boolean,
}