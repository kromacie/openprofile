import {Content} from "../../../Builder/Creator/Editor/Content";
import IndexableDetails from "./IndexableDetails";

interface EducationDetails extends IndexableDetails {
    title: string,
    active: boolean,
    description: Content,
    date: {
        from: string,
        to: string,
        current: boolean,
    }
}

export default EducationDetails;