import {Content} from "../../../Builder/Creator/Editor/Content";
import IndexableDetails from "./IndexableDetails";

interface ExperienceDetails extends IndexableDetails {
    title: string,
    position: string,
    active: boolean,
    description: Content,
    date: {
        from: string,
        to: string,
        current: boolean,
    }
}

export default ExperienceDetails;