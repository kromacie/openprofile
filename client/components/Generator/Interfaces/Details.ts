import ProfileDetails from "./Details/ProfileDetails";
import EducationDetails from "./Details/EducationDetails";
import ExperienceDetails from "./Details/ExperienceDetails";
import SkillDetails from "./Details/SkillDetails";
import LanguageDetails from "./Details/LanguageDetails";
import OthersDetails from "./Details/OthersDetails";
import ConfigDetails from "./Details/ConfigDetails";

interface Details extends ProfileDetails, OthersDetails {
    educations: Array<EducationDetails>,
    experiences: Array<ExperienceDetails>,
    skills: Array<SkillDetails>,
    languages: Array<LanguageDetails>,
    config: ConfigDetails
}

export default Details;