import OnChange from "../../../Composition/Form/Input/OnChange";
import {OnImageChange} from "../../../Composition/Form/File/File";

export default interface ProfileHandlers {
    onNameChange: OnChange,
    onSurnameChange: OnChange,
    onEmailChange: OnChange,
    onAddressChange: OnChange,
    onTitleChange: OnChange,
    onPhoneChange: OnChange,
    onImageChange: OnImageChange,
    onBirthdayYearChange: OnChange,
    onBirthdayMonthChange: OnChange,
    onBirthdayDayChange: OnChange,
}