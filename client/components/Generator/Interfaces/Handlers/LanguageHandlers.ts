import CollectionUpdater from "./CollectionUpdater";

export default interface ExperienceHandlers {
    onLanguageNameChange: CollectionUpdater<string>,
    onLanguageDescriptionChange: CollectionUpdater<string>,
    onLanguageRatingChange: CollectionUpdater<number | string>,
    onLanguageOpinionChange: CollectionUpdater<string>,
    onLanguageActiveChange: CollectionUpdater<boolean>,
}