import CollectionUpdater from "./CollectionUpdater";

export default interface SkillHandlers {
    onSkillNameChange: CollectionUpdater<string>,
    onSkillDescriptionChange: CollectionUpdater<string>,
    onSkillOpinionChange: CollectionUpdater<string>,
    onSkillRatingChange: CollectionUpdater<number | string>,
    onSkillHasRatingChange: CollectionUpdater<boolean>,
    onSkillActiveChange: CollectionUpdater<boolean>,
}