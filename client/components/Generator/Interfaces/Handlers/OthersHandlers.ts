import {Content} from "../../../Builder/Creator/Editor/Content";

export default interface ProfileHandlers {
    onClauseChange: (value: string) => void,
    onInterestsChange: (value: Content) => void,
}