type CollectionUpdater<T> = (id: number, value: T) => any;

export default CollectionUpdater;