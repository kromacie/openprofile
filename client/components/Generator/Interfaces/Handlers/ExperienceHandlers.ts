import {Content} from "../../../Builder/Creator/Editor/Content";
import CollectionUpdater from "./CollectionUpdater";

export default interface ExperienceHandlers {
    onExperienceTitleChange: CollectionUpdater<string>,
    onExperiencePositionChange: CollectionUpdater<string>,
    onExperienceDateFromChange: CollectionUpdater<string>,
    onExperienceDateToChange: CollectionUpdater<string>,
    onExperienceDescriptionChange: CollectionUpdater<Content>,
    onExperienceActiveChange: CollectionUpdater<boolean>,
}