import CollectionUpdater from "./CollectionUpdater";
import {Content} from "../../../Builder/Creator/Editor/Content";

export default interface EducationHandlers {
    onEducationTitleChange: CollectionUpdater<string>,
    onEducationDateFromChange: CollectionUpdater<string>,
    onEducationDateToChange: CollectionUpdater<string>,
    onEducationDescriptionChange: CollectionUpdater<Content>,
    onEducationActiveChange: CollectionUpdater<boolean>,
}