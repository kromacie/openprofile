export default interface ConfigHandlers {
    onSheetConfigVisibilityChange: (name: string, value: boolean) => any;
}