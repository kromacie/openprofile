import React, {ReactNode} from "react";
import {PositionerConsumer} from "../../../../context/PositionerContext/PositionerContext";

import styles from './positioned.module.scss';

const Positioned = ({children}: {children: ReactNode}) => {

    const getDimensions = (width: number): Object => {
        return {
            width: `${width}px`,
        }
    }

    const getScale = (width: number): Object => {
        return {
            transform: `scale(${width / 1000})`,
        }
    }

    return (
        <PositionerConsumer>
            {(props) => {
                return (
                    <div style={getDimensions(props.width - 100)}>
                        <div className={styles.positioned} style={getScale(props.width - 100)}>
                            {children}
                        </div>
                    </div>
                )
            }}
        </PositionerConsumer>
    );
};

export default Positioned;