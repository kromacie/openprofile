import React, {ReactNode, useRef, useState} from "react";
import ReactResizeDetector from 'react-resize-detector';

import {PositionerProvider} from "../../../context/PositionerContext/PositionerContext";

import styles from './positioner.module.scss';

const Positioner = ({children}: {children: ReactNode}) => {
    const ref = useRef<HTMLDivElement>(null);
    const [dimensions, setDimensions] = useState({width: 0, height: 0});

    const dimensionChangeHandler = () => {
        if(ref.current) {
            const XY = ref.current.getBoundingClientRect();

            setDimensions({width: XY.width, height: XY.height})
        }
    }

    return (
        <div className={styles.container}>
            <ReactResizeDetector onResize={dimensionChangeHandler}>
                <div className={styles.positioner} ref={ref}>
                    <PositionerProvider value={dimensions}>
                        {children}
                    </PositionerProvider>
                </div>
            </ReactResizeDetector>
        </div>
    )
};

export default Positioner;