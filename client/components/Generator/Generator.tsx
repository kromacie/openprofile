import React, {ChangeEvent, Component} from 'react';

import styles from './generator.module.scss';

import {
    updateAddress,
    updateBirthdayDay,
    updateBirthdayMonth,
    updateBirthdayYear,
    updateClause,
    updateEducationActive,
    updateEducationDateFrom,
    updateEducationDateTo,
    updateEducationDescription,
    updateEducationItemsOrder,
    updateEducationTitle,
    updateEmail,
    updateExperienceActive,
    updateExperienceDateFrom,
    updateExperienceDateTo,
    updateExperienceDescription,
    updateExperienceItemsOrder,
    updateExperiencePosition,
    updateExperienceTitle,
    updateInterests,
    updateLanguageActive,
    updateLanguageDescription,
    updateLanguageItemsOrder,
    updateLanguageName,
    updateLanguageOpinion,
    updateLanguageRating,
    updateName,
    updateProfileImage, updateSheetConfigVisibility,
    updateSkillActive,
    updateSkillDescription,
    updateSkillHasRating,
    updateSkillItemsOrder,
    updateSkillName,
    updateSkillOpinion,
    updateSkillRating,
    updateSurname,
    updateTelephone,
    updateTitle,
} from "../../services/Utils/updates";

import {onNavigationItemClick, onNavigationToggle} from "../../services/Utils/navigation";
import {onEducationCreate, onEducationDelete} from "../../services/Utils/education";
import {onExperienceCreate, onExperienceDelete} from "../../services/Utils/experience";
import {onLanguageCreate, onLanguageDelete} from "../../services/Utils/languages";
import {onSkillCreate, onSkillDelete} from "../../services/Utils/skill";

import Page, {Pages} from "../Builder/Creator/Page/Page";
import {Content} from "../Builder/Creator/Editor/Content";
import Curriculum from "../Curriculum/Curriculum";
import Basic from "../Curriculum/Basic/Basic";
import Details from "../Curriculum/Details";
import Builder from '../Builder/Builder';
import Profile from "../Builder/Creator/Page/Section/Profile/Profile";
import Education from "../Builder/Creator/Page/Section/Education/Education";
import Experience from "../Builder/Creator/Page/Section/Experience/Experience";
import Skills from "../Builder/Creator/Page/Section/Skills/Skills";
import Languages from "../Builder/Creator/Page/Section/Language/Language";
import Configuration from "../Builder/Creator/Page/Section/Configuration/Configuration";
import Positioner from "./Positioner/Positioner";
import Positioned from "./Positioner/Positioned/Positioned";
import Scroller from './Scroller/Scroller';
import NavigationConsumer from "../Composition/Navigation/Consumer";
import Others from "../Builder/Creator/Page/Section/Others/Others";
import Updater from "../../services/Updaters/Updater";
import UpdaterConsumer from "../../services/Updaters/UpdaterConsumer";
import UpdaterState from "../../services/Updaters/UpdaterState";
import Sheets from "../../services/Repositories/Sheets/Sheets";
import Image, {ImageNotFound} from "../../services/Repositories/Sheets/Profile/Image/Image";
import {UpdateCodes} from "../Builder/Creator/Indicator/Indicator";
import ConnectionBuilder, {AxiosConnection} from "../../requests/Builder";
import Authenticator from "../../services/Authenticator/Authenticator";

interface GeneratorState extends UpdaterState {
    navigation: {
        open: boolean,
        currentElement: number,
    },
    details: Details,
    updating: UpdateCodes
}

interface GeneratorProps {
    id: number,
    details?: Details
}

class Generator extends Component<GeneratorProps, GeneratorState> implements NavigationConsumer, UpdaterConsumer {
    __updater: Updater;
    updateName = updateName.bind(this);
    updateSurname = updateSurname;
    updateEmail = updateEmail;
    updateAddress = updateAddress;
    updateBirthdayDay = updateBirthdayDay;
    updateBirthdayMonth = updateBirthdayMonth;
    updateBirthdayYear = updateBirthdayYear;
    updateTitle = updateTitle;
    updatePhone = updateTelephone;
    updateInterests = updateInterests.bind(this);
    updateClause = updateClause.bind(this);
    updateImage = updateProfileImage.bind(this);

    updateEducationTitle = updateEducationTitle;
    updateEducationDescription = updateEducationDescription;
    updateEducationDateTo = updateEducationDateTo;
    updateEducationDateFrom = updateEducationDateFrom;
    updateEducationActive = updateEducationActive;

    updateExperienceTitle = updateExperienceTitle;
    updateExperiencePosition = updateExperiencePosition;
    updateExperienceDescription = updateExperienceDescription;
    updateExperienceDateFrom = updateExperienceDateFrom;
    updateExperienceDateTo = updateExperienceDateTo;
    updateExperienceActive = updateExperienceActive;

    updateSkillName = updateSkillName;
    updateSkillDescription = updateSkillDescription;
    updateSkillRating = updateSkillRating;
    updateSkillOpinion = updateSkillOpinion;
    updateSkillHasRating = updateSkillHasRating;
    updateSkillActive = updateSkillActive;

    updateLanguageName = updateLanguageName;
    updateLanguageActive = updateLanguageActive;
    updateLanguageDescription = updateLanguageDescription;
    updateLanguageRating = updateLanguageRating;
    updateLanguageOpinion = updateLanguageOpinion;

    onEducationCreate = onEducationCreate.bind(this);
    onEducationDelete = onEducationDelete.bind(this);
    onEducationItemsOrderChange = updateEducationItemsOrder.bind(this);

    onExperienceCreate = onExperienceCreate.bind(this);
    onExperienceDelete = onExperienceDelete.bind(this);
    onExperienceItemsOrderChange = updateExperienceItemsOrder.bind(this);

    onSkillCreate = onSkillCreate.bind(this);
    onSkillDelete = onSkillDelete.bind(this);
    onSkillOrderChange = updateSkillItemsOrder.bind(this);

    onLanguageCreate = onLanguageCreate.bind(this);
    onLanguageDelete = onLanguageDelete.bind(this);
    onLanguageItemsOrderChange = updateLanguageItemsOrder.bind(this);

    onNavigationItemClick = onNavigationItemClick.bind(this);
    onNavigationToggle = onNavigationToggle.bind(this);

    updateSheetConfigVisibilityChange = updateSheetConfigVisibility.bind(this);

    state = {
        navigation: {
            open: false,
            currentElement: Pages.PROFILE,
        },
        details: new Details(),
        updaters: {},
        watchers: {},
        updating: UpdateCodes.READY,
    };

    handlers = {
        onNameChange: (event: ChangeEvent<HTMLInputElement>) => this.updateName(event.target.value),
        onSurnameChange: (event: ChangeEvent<HTMLInputElement>) => this.updateSurname(event.target.value),
        onEmailChange: (event: ChangeEvent<HTMLInputElement>) => this.updateEmail(event.target.value),
        onAddressChange: (event: ChangeEvent<HTMLInputElement>) => this.updateAddress(event.target.value),
        onTitleChange: (event: ChangeEvent<HTMLInputElement>) => this.updateTitle(event.target.value),
        onPhoneChange: (event: ChangeEvent<HTMLInputElement>) => this.updatePhone(event.target.value),
        onImageChange: (validity: ValidityState, files: FileList | null) => this.updateImage(validity, files),
        onBirthdayDayChange: (event: ChangeEvent<HTMLInputElement>) => this.updateBirthdayDay(event.target.value),
        onBirthdayMonthChange: (event: ChangeEvent<HTMLInputElement>) => this.updateBirthdayMonth(event.target.value),
        onBirthdayYearChange: (event: ChangeEvent<HTMLInputElement>) => this.updateBirthdayYear(event.target.value),
        onClauseChange: this.updateClause,
        onInterestsChange: this.updateInterests,

        onEducationTitleChange: (id: number, title: string) => this.updateEducationTitle(id, title),
        onEducationDescriptionChange: (id: number, content: Content) => this.updateEducationDescription(id, content),
        onEducationDateToChange: (id: number, from: string) => this.updateEducationDateTo(id, from),
        onEducationDateFromChange: (id: number, to: string) => this.updateEducationDateFrom(id, to),
        onEducationActiveChange: (id: number, active: boolean) => this.updateEducationActive(id, active),

        onExperienceTitleChange: (id: number, title: string) => this.updateExperienceTitle(id, title),
        onExperiencePositionChange: (id: number, position: string) => this.updateExperiencePosition(id, position),
        onExperienceDescriptionChange: (id: number, content: Content) => this.updateExperienceDescription(id, content),
        onExperienceDateToChange: (id: number, from: string) => this.updateExperienceDateTo(id, from),
        onExperienceDateFromChange: (id: number, to: string) => this.updateExperienceDateFrom(id, to),
        onExperienceActiveChange: (id: number, active: boolean) => this.updateExperienceActive(id, active),

        onSkillNameChange: (id: number, name: string) => this.updateSkillName(id, name),
        onSkillDescriptionChange: (id: number, description: string) => this.updateSkillDescription(id, description),
        onSkillOpinionChange: (id: number, opinion: string) => this.updateSkillOpinion(id, opinion),
        onSkillRatingChange: (id: number, rating: string | number) => this.updateSkillRating(id, rating),
        onSkillHasRatingChange: (id: number, hasRating: boolean) => this.updateSkillHasRating(id, hasRating),
        onSkillActiveChange: (id: number, active: boolean) => this.updateSkillActive(id, active),

        onLanguageNameChange: (id: number, name: string) => this.updateLanguageName(id, name),
        onLanguageActiveChange: (id: number, active: boolean) => this.updateLanguageActive(id, active),
        onLanguageDescriptionChange: (id: number, description: string) => this.updateLanguageDescription(id, description),
        onLanguageRatingChange: (id: number, rating: number | string) => this.updateLanguageRating(id, rating),
        onLanguageOpinionChange: (id: number, opinion: string) => this.updateLanguageOpinion(id, opinion),

        onSheetConfigVisibilityChange: (name: string, value: boolean) => this.updateSheetConfigVisibilityChange(this.props.id, name, value),
    }

    constructor(props: GeneratorProps) {
        super(props);

        this.__updater = new Updater(this, 500);
    }

    componentDidMount() {
        this.setState({
            updating: UpdateCodes.PENDING
        });

        if (!this.props.details) {
            Sheets
                .find(this.props.id)
                .then(async (res: any) => {
                    const details = Object.assign(new Details(), {
                        ...res.profile,
                    }, {
                        experiences: res.experiences,
                        educations: res.educations,
                        languages: res.languages,
                        skills: res.skills,
                        config: res.config
                    }, {
                        image: await Image.get(this.props.id).catch(() => "")
                    });
                    this.setState({
                        details: details,
                        updating: UpdateCodes.READY
                    });
                })
                .catch(() => {
                    console.warn("Cannot load sheet");

                    this.setState({
                        updating: UpdateCodes.ERROR
                    });
                })
        } else {
            this.setState({
                details: this.props.details
            });

            Image.get(this.props.id)
                .then((image) => {
                    this.setState({
                        details: Object.assign({}, this.props.details, {
                            image: image
                        }),
                        updating: UpdateCodes.READY
                    })
                })
                .catch((err) => {
                    if (err instanceof ImageNotFound) {
                        this.setState({
                            updating: UpdateCodes.READY
                        })
                    } else {
                        this.setState({
                            updating: UpdateCodes.ERROR
                        })
                    }
                })
        }

    }

    getUpdater(): Updater {
        return this.__updater;
    }

    print() {
        const connection = new ConnectionBuilder().axios(AxiosConnection.PPT);
        Authenticator.setConnection(connection);
        connection.get("print?id=" + this.props.id, {
            responseType: "blob",
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');

            link.href = url;
            link.setAttribute('download', 'file.pdf');
            link.click();
        })
    }

    render() {
        const pages = {
            [Pages.PROFILE]: <Page key={Pages.PROFILE} title="Dane osobowe">
                <Profile
                    information={{details: this.state.details, handlers: this.handlers}}
                />
            </Page>,
            [Pages.EDUCATION]: <Page key={Pages.EDUCATION} title="Edukacja">
                <Education
                    information={{details: this.state.details, handlers: this.handlers}}
                    onEducationItemCreate={this.onEducationCreate}
                    onEducationItemDelete={this.onEducationDelete}
                    onEducationItemOrderChange={this.onEducationItemsOrderChange}
                />
            </Page>,
            [Pages.EXPERIENCE]: <Page key={Pages.EXPERIENCE} title="Doświadczenie">
                <Experience
                    information={{details: this.state.details, handlers: this.handlers}}
                    onExperienceItemCreate={this.onExperienceCreate}
                    onExperienceItemDelete={this.onExperienceDelete}
                    onExperienceItemOrderChange={this.onExperienceItemsOrderChange}
                />
            </Page>,
            [Pages.SKILLS]: <Page key={Pages.SKILLS} title="Umiejętności">
                <Skills
                    information={{details: this.state.details, handlers: this.handlers}}
                    onSkillItemCreate={this.onSkillCreate}
                    onSkillItemDelete={this.onSkillDelete}
                    onSkillItemOrderChange={this.onSkillOrderChange}
                />
            </Page>,
            [Pages.LANGUAGES]: <Page key={Pages.LANGUAGES} title="Języki">
                <Languages
                    information={{details: this.state.details, handlers: this.handlers}}
                    onLanguageItemCreate={this.onLanguageCreate}
                    onLanguageItemDelete={this.onLanguageDelete}
                    onLanguageItemOrderChange={this.onLanguageItemsOrderChange}
                />
            </Page>,
            [Pages.OTHERS]: <Page key={Pages.OTHERS} title="Inne">
                <Others
                    information={{details: this.state.details, handlers: this.handlers}}
                />
            </Page>,
            [Pages.CONFIG]: <Page key={Pages.OTHERS} title="Inne">
                <Configuration
                    information={{config: this.state.details.config, handlers: this.handlers}}
                />
            </Page>,
        };

        return (
            <div className={styles.container}>
                <div className={styles.builder}>
                    <Builder
                        pages={pages}
                        information={{details: this.state.details, handlers: this.handlers}}
                        navigation={this.state.navigation}
                        onNavigationItemClick={this.onNavigationItemClick}
                        onNavigationToggle={this.onNavigationToggle}
                        updating={this.state.updating}
                        onReadyClick={() => this.print()}
                    />
                </div>
                <Scroller>
                    <Positioner>
                        <Positioned>
                            <Curriculum schema={Basic} details={this.state.details}/>
                        </Positioned>
                    </Positioner>
                </Scroller>
            </div>
        );
    }
}

export default Generator;