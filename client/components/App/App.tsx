import React, {Component, Fragment} from "react";
import cn from "classnames";
import _ from "lodash";

import styles from './app.module.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faPlus,
} from "@fortawesome/free-solid-svg-icons";
import Navigation from "./Navigation/Navigation";
import Pages from "./Pages/Pages";

import {onNavigationItemClick, onNavigationToggle} from "../../services/Utils/navigation";
import NavigationConsumer from "../Composition/Navigation/Consumer";
import Sheet from "./Sheet/Sheet";
import NavigationState from "../Composition/Navigation/State";
import Sheets from "../../services/Repositories/Sheets/Sheets";
import UpdaterState from "../../services/Updaters/UpdaterState";
import UpdaterConsumer from "../../services/Updaters/UpdaterConsumer";
import Updater from "../../services/Updaters/Updater";
import Router from "next/router";

interface AppState extends NavigationState, UpdaterState {
    sheets: Array<object>,
    updater?: number,
}

interface AppProps {
    sheets?: Array<object>
}

export default class App extends Component<AppProps, AppState> implements NavigationConsumer, UpdaterConsumer {
    __updater: Updater;
    onNavigationItemClick = onNavigationItemClick.bind(this);
    onNavigationToggle = onNavigationToggle.bind(this);

    state = {
        sheets: [],
        updaters: {},
        watchers: {},
        navigation: {
            open: false,
            currentElement: Pages.DOCS
        }
    }

    constructor(props: AppProps) {
        super(props);

        this.__updater = new Updater(this, 500)
    }

    getUpdater(): Updater {
        return this.__updater;
    }

    componentDidMount() {
        if(!this.props.sheets)
            Sheets.get().then((sheets) => {
                this.setState({
                    sheets: sheets
                })
            })
        else
            this.setState({
                sheets: this.props.sheets
            })
    }

    createSheet() {
        Sheets.create(`moje-cv-${this.state.sheets.length + 1}`).then((sheet) => {
            this.setState((prev) => {
                return {
                    sheets: Array.from([...prev.sheets, sheet])
                }
            })
        })
    }

    deleteSheet(id: number) {
        Sheets.delete(id).then(() => {
            this.setState((prev) => {
                const sheets = Array.from(prev.sheets);

                sheets.splice(prev.sheets.findIndex((sheet: any) => {
                    return sheet.id === id;
                }), 1)

                return {
                    sheets: sheets
                }
            })
        })
    }

    _getSheetFromState(sheets: Array<any>, id: number) {
        const index = sheets.findIndex((sheet: any) => {
            return sheet.id === id;
        });

        return _.clone(sheets[index]);
    }

    _addSheetToState(sheets: Array<any>, sheet: any) {
        const newSheets = [...sheets];

        const index = sheets.findIndex((_sheet: any) => {
            return _sheet.id === sheet.id;
        });

        newSheets[index] = sheet;

        return newSheets;
    }

    gotoSheet(id: number) {
        return Router.push(`/app/${id}`).then();
    }

    updateSheet(id: number, name: string) {
        this.setState((prev) => {
            const sheet = this._getSheetFromState(prev.sheets, id);

            sheet.name = name;

            if (sheet.updater) {
                window.clearTimeout(sheet.updater);
            }

            return {
                sheets: this._addSheetToState(prev.sheets, sheet)
            }
        });

        this.getUpdater().update(`sheet.${id}`, () => {
            Sheets.update(id, name).then(response => {
                this.setState((prev) => {
                    const sheetToUpdate = this._getSheetFromState(prev.sheets, response.id);

                    sheetToUpdate.updated_at = response.updated_at;

                    return {
                        sheets: this._addSheetToState(prev.sheets, sheetToUpdate)
                    }
                })
            })
        });
    }

    render() {
        return (
            <Fragment>
                <div className={styles.wrapper}>
                    <div className={styles.navigation}>
                        <Navigation
                            onNavigationToggle={this.onNavigationToggle}
                            onItemClick={this.onNavigationItemClick}
                            navigationOpen={this.state.navigation.open}
                            currentItem={this.state.navigation.currentElement}
                        />
                    </div>
                    <div className={cn(styles.main)}>
                        <div className={styles.topbar}>
                            <h1 className={styles.title}>Arkusze</h1>
                        </div>
                        <div className={styles.content}>
                            <h1 className={styles.contentTitle}>
                                Wybierz arkusz
                            </h1>
                            <div className={styles.sheets}>
                                {this.state.sheets.map((element: { id: number, name: string, updated_at: string }) => {
                                    return (
                                        <Sheet key={element.id} name={element.name} updated_at={element.updated_at}
                                               onDelete={() => this.deleteSheet(element.id)}
                                               onChange={(name) => this.updateSheet(element.id, name)}
                                               onClick={() => this.gotoSheet(element.id)}
                                        />
                                    )
                                })}
                                <div className={styles.create} onClick={() => this.createSheet()}>
                                    <div className={styles.createButton}>
                                        <FontAwesomeIcon icon={faPlus}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}