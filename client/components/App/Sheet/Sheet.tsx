import React from "react";
import styles from "./sheet.module.scss";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFile, faPen, faTrash} from "@fortawesome/free-solid-svg-icons";
import cn from "classnames";

type SheetProps = {
    name: string,
    updated_at: string,
    onDelete?: () => void,
    onChange?: (name: string) => void,
    onClick?: () => void,
}

const Sheet = (props: SheetProps) => {
    return (
        <div className={styles.sheet}>
            <div className={styles.navigation}>
                <div className={styles.icon}>
                    <FontAwesomeIcon icon={faFile}/>
                </div>
                <div className={styles.footer}>
                    <div className={styles.delete} onClick={props.onDelete}>
                        <FontAwesomeIcon icon={faTrash}/>
                    </div>
                </div>
            </div>
            <div className={styles.content}>
                <div className={styles.image}>
                    <input className={styles.input} value={props.name} placeholder={"Nazwa arkusza"} onChange={(el) => {
                        props.onChange ? props.onChange(el.target.value) : "";
                    }}/>
                </div>
                <div className={styles.main}>
                    <div className={cn(styles.description)}>
                        Arkusz
                    </div>
                    <div className={cn(styles.value, styles.bold, styles.italic)}>
                        Basic
                    </div>
                    <div className={cn(styles.description)}>
                        Data edytowania
                    </div>
                    <div className={cn(styles.value, styles.bold)}>
                        {props.updated_at}
                    </div>
                </div>
            </div>
            <div className={styles.show} onClick={props.onClick}>
                <FontAwesomeIcon icon={faPen}/>
            </div>
        </div>
    );
};

export default Sheet;