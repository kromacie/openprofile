import React from 'react';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faArrowLeft, faFileAlt,
    faUserAlt
} from "@fortawesome/free-solid-svg-icons";

import Pages from "../Pages/Pages";
import Item from "../../Composition/Navigation/Item/Item";
import BaseNavigation from "../../Composition/Navigation/Navigation";


interface NavigationProps {
    onNavigationToggle: () => void,
    onItemClick: (index: number) => void,
    navigationOpen: boolean,
    currentItem: number
}

const Navigation = ({onNavigationToggle, navigationOpen, onItemClick, currentItem}: NavigationProps) => {

    const open = navigationOpen;

    return (
        <BaseNavigation navigationOpen={navigationOpen} topItems={[
            <Item
                key={Pages.PROFILE}
                icon={<FontAwesomeIcon icon={faUserAlt}/>}
                title="Profil"
                open={open}
                onClick={() => onItemClick(Pages.PROFILE)}
                clicked={currentItem == Pages.PROFILE}/>,
            <Item
                key={Pages.DOCS}
                icon={<FontAwesomeIcon icon={faFileAlt}/>}
                open={open}
                onClick={() => onItemClick(Pages.DOCS)}
                clicked={currentItem == Pages.DOCS}
                title="Dokumenty"/>,
        ]} bottomItems={[
            <Item
                key={"Zwiń"}
                icon={<FontAwesomeIcon icon={faArrowLeft}/>}
                title="Zwiń"
                open={open}
                onClick={onNavigationToggle}
                spin={true}
                bottom={true}/>
        ]}/>
    );
};

export default Navigation;