import React from 'react';

export default function (Component, props) {
    return React.cloneElement(
        Component, props
    );
}