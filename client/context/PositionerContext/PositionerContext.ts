import React from "react";

const PositionerContext = React.createContext<{width: number, height: number}>({width: 0, height: 0});

export const PositionerProvider = PositionerContext.Provider;
export const PositionerConsumer = PositionerContext.Consumer;

export default PositionerContext;


