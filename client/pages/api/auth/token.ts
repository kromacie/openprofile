import {Request, Response} from "express";
import Authenticator from "../../../services/Authenticator/Authenticator";
import Builder, {AxiosConnection} from "../../../requests/Builder";

export default function (req: Request, res: Response) {
    Authenticator
        .setConnection(new Builder().axios(AxiosConnection.API, false))
        .login(req.body.username, req.body.password)
        .then((token) => {
            if(req.session) {
                req.session.token = token;
            }

            res.status(200);
            res.send(token);
        })
        .catch((reason => {
            res.status(401);
            res.contentType('text')
            res.end();
        }))
}
