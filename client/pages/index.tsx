import React from 'react';
import App from "../components/App/App";
import {GetServerSideProps} from "next";
import Authenticator from "../services/Authenticator/Authenticator";

export default function Home(props: {token: string}) {
    Authenticator.setToken(props.token);

    return (
        <App />
    )
}

export const getServerSideProps: GetServerSideProps = async (context: any) => {
    const {req} = context;
    const {res} = context;
    const {session} = req;

    if(session.token) {
        await Authenticator.setToken(session.token.token).check()
            .catch(() => {
                res.redirect(302, '/login');
            })
    }

    return {
        props: {
            token: Authenticator.getToken(),
        }
    };
}