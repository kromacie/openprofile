import React from 'react';

import LoginSection from '../components/Login/Login'
import {GetServerSideProps} from "next";
import Authenticator from "../services/Authenticator/Authenticator";

export default function Login() {
    return (
        <div>
            <LoginSection/>
        </div>
    )
}

export const getServerSideProps: GetServerSideProps = async (context: any) => {
    const {req} = context;
    const {res} = context;
    const {session} = req;

    if(session.token) {
        await Authenticator.setToken(session.token.token).check()
            .then(() => {
                res.redirect(302, '/');
            })
    }

    return {
        props: {
            token: Authenticator.getToken()
        }
    };
}
