import React, {useEffect, useState} from 'react';
import Curriculum from "../../../components/Curriculum/Curriculum";
import Basic from "../../../components/Curriculum/Basic/Basic";
import Sheets from "../../../services/Repositories/Sheets/Sheets";
import Details from "../../../components/Curriculum/Details";
import Authenticator from "../../../services/Authenticator/Authenticator";
import Image from "../../../services/Repositories/Sheets/Profile/Image/Image";
import {GetServerSideProps} from "next";

export default function Print(props: { id: number, details: Details, token: string }) {
    const [image, setImage] = useState({url: ""});
    const [ready, setReady] = useState(false);
    Authenticator.setToken(props.token);

    useEffect(() => {
        if(props.token) {
            Authenticator.setToken(props.token);
        }

        Image.get(props.id, new Date().getTime()).then((i) => {
            setImage(i);
            setReady(true);
        }).catch(() => {
            setReady(true);
        })
    }, [])

    return (
        <Curriculum print={true} ready={ready} details={Object.assign({}, props.details, {image: image})} schema={Basic} />
    )

}

export const getServerSideProps: GetServerSideProps = async (context: any) => {

    const {id} = context.query;
    const {req} = context;
    const {session} = req;
    const {cookies} = req;

    if (session.token) {
        Authenticator.setToken(session.token.token);
    } else if(cookies.open_profile_token) {
        Authenticator.setToken(cookies.open_profile_token);
    }

    return {
        props: {
            details: await Sheets.find(id, 'desc').then(async (res: any) => {
                return Object.assign({}, new Details(), {
                    ...res.profile,
                }, {
                    experiences: res.experiences,
                    educations: res.educations,
                    languages: res.languages,
                    skills: res.skills,
                    config: res.config
                });
            }),
            id: id,
            token: Authenticator.getToken()
        }
    }

}