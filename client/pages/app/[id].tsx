import React from "react";
import Generator from "../../components/Generator/Generator";
import Details from "../../components/Curriculum/Details";
import {GetServerSideProps} from "next";
import Authenticator from "../../services/Authenticator/Authenticator";

function App(props: { id: number, token: string, details?: Details }) {
    Authenticator.setToken(props.token);

    return (
        <Generator id={props.id} details={props.details}/>
    )
}

export const getServerSideProps: GetServerSideProps = async (context: any) => {
    const {id} = context.query;
    const {req} = context;
    const {res} = context;
    const {session} = req;

    if(session.token) {
        await Authenticator.setToken(session.token.token).check()
            .catch(() => {
                res.writeHead(302, { Location: '/login' });
                res.end();
            })
    }

    return {
        props: {
            id: id,
            token: Authenticator.getToken()
        }
    }
}

export default App;