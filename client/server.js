const express = require('express');
const next = require('next');
const {createProxyMiddleware} = require('http-proxy-middleware');
const cookieParser = require('cookie-parser');
const session = require('express-session');


const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV === 'development';
const app = next({dev});
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = express();

    server.use(cookieParser());
    server.use(session({
        secret: "somerandomsecretucannotguess",
        resave: false,
        saveUninitialized: false,
    }))
    server.use(
        createProxyMiddleware(['/api/**', '!**/auth/token', '!**/auth/token/refresh'], {
            target: "http://web:80",
            pathRewrite: {
                '^/api': '/'
            },
            changeOrigin: true,
        })
    )

    server.use(
        '/ppt', createProxyMiddleware({
            target: "http://puppeteer:4201",
            changeOrigin: true,
            pathRewrite: {
                '^/ppt': '/'
            },
        })
    );

    server.all('*', (req, res) => handle(req, res));

    server.listen(port, err => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`);
    });
});