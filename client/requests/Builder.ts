import {ApolloClient, createHttpLink, InMemoryCache, NormalizedCache, NormalizedCacheObject} from "@apollo/client";
import Authenticator from "../services/Authenticator/Authenticator";
import {createUploadLink} from "apollo-upload-client";
import axios from "axios";
import {setContext} from "@apollo/client/link/context";

export enum AxiosConnection {
    API = 'api',
    PPT = 'ppt',
    CLIENT = 'client'
}

let globalMultipartApolloClient: ApolloClient<NormalizedCacheObject> | null;
let globalStandardApolloClient: ApolloClient<NormalizedCacheObject> | null;

export default class Builder {

    public graphql(multipart: boolean = false, initialData?: NormalizedCacheObject) {

        if (multipart && globalMultipartApolloClient) {
            return globalMultipartApolloClient;
        }

        if (!multipart && globalStandardApolloClient) {
            return globalStandardApolloClient;
        }

        return this.buildGraphqlConnection(multipart, initialData);
    }

    private buildGraphqlConnection(multipart: boolean, initialData?: NormalizedCacheObject) {

        const cache = new InMemoryCache();

        if (initialData) {
            cache.restore(initialData);
        }

        const authLink = setContext((_, {headers}) => {
            // get the authentication token from local storage if it exists
            const token = Authenticator.getToken();
            // return the headers to the context so httpLink can read them
            return {
                headers: {
                    ...headers,
                    authorization: token ? `Bearer ${token}` : "",
                }
            }
        });

        const uri = (typeof window === "undefined" ? 'http://client:3000' : '') + '/api/graphql/default';

        if (multipart) {
            return new ApolloClient({
                cache: cache,
                // @ts-ignore
                link: authLink.concat(createUploadLink({
                    uri: uri,
                }))
            })
        }

        return new ApolloClient({
            cache: cache,
            link: authLink.concat(createHttpLink({
                uri: uri,
            })),
        });
    }

    public axios(connection: AxiosConnection, proxy = true) {
        let path = '';
        let origin = ''

        switch (connection) {
            default:
            case AxiosConnection.API:
                path = proxy ? '/api' : '';
                origin = proxy ? 'http://client:3000' : 'http://web:80'
                break;
            case AxiosConnection.PPT:
                path = proxy ? '/ppt' : '';
                origin = 'http://ppt:3000'
                break;
            case AxiosConnection.CLIENT:
                path = '/api';
                origin = '';
                break;
        }

        return axios.create({
            baseURL: (typeof window === "undefined" ? origin : '') + path,
            timeout: 10000,
        });
    }
}