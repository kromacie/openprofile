import axios from 'axios';

const instance = axios.create({
    baseURL: (typeof window === "undefined" ? "http://client:3000" : '') + '/api',
    timeout: 10000,
});

export default instance;