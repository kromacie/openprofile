import {gql} from '@apollo/client';

const remove = gql`
    mutation deleteSheet($id: Int!) {
        deleteSheet(input: {
            id: $id
        })
    }
`

export default remove;