import {gql} from "@apollo/client";

const update = gql`
    mutation updateSheetConfigVisibility($id: Int!, $value: Boolean!, $key: String!) {
        updateSheetConfigVisibility(input: {
            id: $id,
            key: $key,
            value: $value
        })
    }
`;

export default update;