import {gql} from '@apollo/client';

const update = gql`
    mutation updateSheet($id: Int!, $name: String!) {
        updateSheet(input: {
            id: $id
            name: $name
        }) {
            id,
            name,
            updated_at
        }
    }
`

export default update;