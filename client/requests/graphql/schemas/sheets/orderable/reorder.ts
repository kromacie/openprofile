import {gql} from "@apollo/client";

const reorder = gql`
    mutation reorder($id: Int!, $to: Int!, $entity: OrderableEnum!) {
        reorderItems(input: {
            id: $id,
            to: $to,
            entity: $entity
        })
    }
`;

export default reorder;