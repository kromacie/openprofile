import {gql} from '@apollo/client';

const all = gql`
    query {
        user {
            sheets {
                id
                name
                updated_at
            }
        }
    }
`

export default all;