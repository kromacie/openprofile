import {gql} from '@apollo/client';

const create = gql`
    mutation createSheet($name: String!) {
        createSheet(input: {
            name: $name
        }) {
            id, 
            name,
            updated_at
        }
    }
`

export default create;