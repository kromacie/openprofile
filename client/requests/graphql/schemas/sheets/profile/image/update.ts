import {gql} from '@apollo/client';

const update = gql`
    mutation updateSheetProfileImage($id: Int!, $picture: FileUpload!) {
        updateSheetProfileImage(input: {
            id: $id,
            picture: $picture
        }) {
            url
        }
    }
`

export default update;