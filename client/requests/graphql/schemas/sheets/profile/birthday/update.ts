import {gql} from '@apollo/client';

const update = gql`
    mutation updateSheetProfileBirthday($id: Int!, $key: String!, $value: Any!) {
        updateSheetProfileBirthday(input: {
            id: $id
            key: $key
            value: $value
        })
    }
`

export default update;