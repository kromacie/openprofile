import {gql} from '@apollo/client';

const update = gql`
    mutation updateSheetProfile($id: Int!, $key: String!, $value: Any!) {
        updateSheetProfile(input: {
            id: $id
            key: $key
            value: $value
        })
    }
`

export default update;