import {gql} from "@apollo/client";

const update = gql`
mutation updateExperience($id: Int!, $value: Any!, $key: String!) {
    updateExperience(input: {
        id: $id,
        key: $key,
        value: $value
    })
}
`;

export default update;