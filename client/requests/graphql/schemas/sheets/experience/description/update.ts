import {gql} from "@apollo/client";

const update = gql`
mutation updateExperienceDescription($id: Int!, $value: EditorDescription!) {
    updateExperienceDescription(input: {
        id: $id,
        description: $value
    })
}
`;

export default update;