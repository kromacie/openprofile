import {gql} from "@apollo/client";

const update = gql`
mutation updateExperienceDate($id: Int!, $value: DateRange!) {
    updateExperienceDate(input: {
        id: $id,
        date: $value
    })
}
`;

export default update;