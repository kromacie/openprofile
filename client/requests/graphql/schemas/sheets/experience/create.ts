import {gql} from "@apollo/client";

const create = gql`
    mutation createExperience ($id: Int!) {
        createExperience(input: {
            id: $id
        }) {
            id
        }
    }
`;

export default create;