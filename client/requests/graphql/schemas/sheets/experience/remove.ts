import {gql} from "@apollo/client";

const remove = gql`
    mutation deleteExperience ($id: Int!) {
        deleteExperience(input: {
            id: $id
        })
    }
`;

export default remove;