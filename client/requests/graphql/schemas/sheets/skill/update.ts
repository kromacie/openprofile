import {gql} from '@apollo/client';

const update = gql`
    mutation updateSkill($id: Int!, $key: String!, $value: Any!) {
        updateSkill(input: {id: $id, key: $key, value: $value})
    }
`;

export default update;