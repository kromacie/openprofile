import {gql} from '@apollo/client';

const remove = gql`
    mutation deleteSkill($id: Int!) {
        deleteSkill(input: {id: $id})
    }
`;

export default remove;