import {gql} from '@apollo/client';

const create = gql`
    mutation createSkill($id: Int!) {
        createSkill(input: {id: $id}) {
            id
        }
    }
`;

export default create;