import {gql} from '@apollo/client';

const find = gql`
    query findSheet($id: Int!, $filter: String!) {
        user {
            sheet(input: {
                id: $id
            }) {
                id
                profile {
                    name
                    surname
                    email
                    telephone
                    birthday {
                        year
                        month
                        day
                    }
                    address
                    title
                    clause
                    image {
                        url
                    }
                }
                experiences(input: {
                    filter: $filter
                }) {
                    id
                    title
                    order
                    position
                    date
                    description
                    active
                }
                educations(input: {
                    filter: $filter
                }) {
                    id
                    title
                    order
                    date
                    description
                    active
                }
                skills(input: {
                    filter: $filter
                }) {
                    id
                    name
                    rating
                    hasRating
                    opinion
                    description
                }
                languages(input: {
                    filter: $filter
                }) {
                    id
                    name
                    rating
                    hasRating
                    opinion
                    description
                }
                config {
                    isEducationVisible
                    isExperienceVisible
                    isLanguageVisible
                    isSkillsVisible
                    isInterestsVisible
                }
            }
        }
    }
`

export default find;