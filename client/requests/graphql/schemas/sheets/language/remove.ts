import {gql} from '@apollo/client';

const remove = gql`
    mutation deleteLanguage($id: Int!) {
        deleteLanguage(input: {id: $id})
    }
`;

export default remove;