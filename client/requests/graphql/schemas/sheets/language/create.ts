import {gql} from '@apollo/client';

const create = gql`
mutation createLanguage($id: Int!) {
    createLanguage(input: {id: $id}) {
        id
    }
}
`;

export default create;