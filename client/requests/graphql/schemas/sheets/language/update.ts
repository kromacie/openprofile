import {gql} from '@apollo/client';

const update = gql`
    mutation updateLanguage($id: Int!, $key: String!, $value: Any!) {
        updateLanguage(input: {id: $id, key: $key, value: $value})
    }
`;

export default update;