import {gql} from "@apollo/client";

const update = gql`
mutation updateEducationDate($id: Int!, $value: DateRange!) {
    updateEducationDate(input: {
        id: $id,
        date: $value
    })
}
`;

export default update;