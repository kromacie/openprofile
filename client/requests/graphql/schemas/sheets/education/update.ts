import {gql} from "@apollo/client";

const update = gql`
mutation updateEducation($id: Int!, $value: Any!, $key: String!) {
    updateEducation(input: {
        id: $id,
        key: $key,
        value: $value
    })
}
`;

export default update;