import {gql} from "@apollo/client";

const update = gql`
mutation updateEducationDescription($id: Int!, $value: EditorDescription!) {
    updateEducationDescription(input: {
        id: $id,
        description: $value
    })
}
`;

export default update;