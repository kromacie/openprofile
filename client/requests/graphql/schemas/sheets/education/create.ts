import {gql} from '@apollo/client';

const create = gql`
    mutation createEducation($id: Int!) {
        createEducation(input: {
            id: $id
        }) {
            id
        }
    }
`

export default create;