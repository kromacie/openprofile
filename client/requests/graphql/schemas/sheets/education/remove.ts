import {gql} from "@apollo/client";

const remove = gql`
    mutation deleteEducation ($id: Int!) {
        deleteEducation(input: {
            id: $id
        })
    }
`;

export default remove;