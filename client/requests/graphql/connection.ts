import {ApolloClient, InMemoryCache} from '@apollo/client';
import Authenticator from "../../services/Authenticator/Authenticator";
import {createUploadLink} from "apollo-upload-client";

const connection = new ApolloClient({
    uri: (process.env.HOST ? process.env.HOST : '') + '/api/graphql/default',
    cache: new InMemoryCache(),
    headers: {
        "Authorization": Authenticator.hasToken() ?
            "Bearer " + Authenticator.getToken() :
            ""
    }
});

export const multipart = new ApolloClient({
    cache: new InMemoryCache(),
    // @ts-ignore
    link: createUploadLink({
        uri: (process.env.HOST ? process.env.HOST : '') + '/api/graphql/default',
        headers: {
            "Authorization": Authenticator.hasToken() ?
                "Bearer " + Authenticator.getToken() :
                ""
        }
    })
})

export default connection;