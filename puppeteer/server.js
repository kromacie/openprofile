const express = require('express');
const puppeteer = require('puppeteer');

const server = express();

const app = async () => {
    const browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            // This will write shared memory files into /tmp instead of /dev/shm,
            // because Docker’s default for /dev/shm is 64MB
            '--disable-dev-shm-usage'
        ],
        executablePath: 'google-chrome-unstable'
    });

    server.listen(4201, () => {
        console.log(`> Ready on http://localhost:4200`);
    });

    server.get('/print', async (req, res) => {
        const token = req.headers['authorization'];

        if (!token) {
            res.status(401);
            return res.send();
        }

        if (!req.query.id) {
            res.status(404);
            return res.send();
        }

        let page = await browser.newPage();

        const time = new Date();
        time.setFullYear(time.getFullYear() + 1);

        console.log(token.replace(/^Bearer\s*/, ''));
        console.log(time.getTime());

        await page.setCookie({
            "domain": "client:3000",
            "expirationDate": 1597288045,
            "hostOnly": false,
            "httpOnly": false,
            "name": "open_profile_token",
            "path": "/",
            "sameSite": "no_restriction",
            "secure": false,
            "session": false,
            "storeId": "0",
            "value": token.replace(/^Bearer\s*/, ''),
            "id": 1
        });

        try {
            await page.goto("http://client:3000/app/print/" + req.query.id);

            await page.waitForSelector('.curriculum-ready');

            const height = await getFooterHeight(page);
            await hideFooter(page);
            let pdf = await page.pdf({
                path: 'index.pdf',
                displayHeaderFooter: true,
                printBackground: true,
                format: 'A4',
                footerTemplate: `
              <div style="margin-bottom: 0; color: lightgray; font-weight: 300; font-size: 7px; margin-left: 20px; margin-right: 20px">
                <span>${await getFooterText(page)}</span></span>
              </div>
            `,
                headerTemplate: '<div></div>',
                margin: {
                    top: 0,
                    right: 0,
                    bottom: height + 10,
                    left: 0,
                },
            });

            res.contentType("application/pdf");
            res.send(pdf);

            page.close();
        } catch (e) {
            res.status(500);
            res.send();
            
            page.close();
        }
    });
}

const getFooterText = async (page) => {
    const element = await page.$("#cv-footer");
    return await page.evaluate(element => element.textContent, element);
};

const hideFooter = async (page) => {
    page.evaluate(() => {
        document.querySelector('#cv-footer').style.display = 'none';
    });
}

const getFooterHeight = async (page) => {
    const element = await page.$("#cv-footer");
    return await page.evaluate(element => element.offsetHeight, element);
}

app().then(() => console.log("Application started!"));

